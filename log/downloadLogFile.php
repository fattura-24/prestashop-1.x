<?php
/**
 * 2007-2019 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    Fattura24 <fattura24.com>
 * @copyright Copyright (c) Fattura24
 * @license   Commercial license
 */
$data = $_POST['info'];
$today = date('d_m_Y');
$logFileName = dirname(__FILE__) . '/f24_trace_' . $today . '.log';
$response = ['fileExists' => file_exists($logFileName)];
echo json_encode($response);
$fp = fopen($logFileName, 'a+');
fprintf($fp, '\n%s\n', $data);
fclose($fp);
