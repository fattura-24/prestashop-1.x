# Changelog

# 2.8.10
###### _Dec 06, 2024_
- fixed bug in COA option list

# 2.8.9
###### _Giu 06, 2024_
- added hook to display Fattura24 PDF in frontend

# 2.8.8
###### _Feb 20, 2024_
- changed label in module settings

# 2.8.7
###### _Feb 12, 2024_
- fixed bug in retrieving module options

# 2.8.6
###### _Feb 12, 2024_
- fixed bug in admin customers form 

# 2.8.5
###### _Nov 28, 2023_
- fixed bug in controller override

# 2.8.4
###### _Nov 20, 2023_
- fixed bug in creating docs when order total is zero


# 2.8.3
###### _Oct 09, 2023_
- Fixed bug in fetching templates, numerators, coa lists

# 2.8.2
###### _Oct 02, 2023_
- Improved showMessage method

# 2.8.1
###### _Sep 18, 2023_
- Added message to check for module updates

# 2.8.0
###### _Sep 06, 2023_
- General code refactoring and bug fixing

# 2.7.0
###### _May 22, 2023_
- Fixed bug in coa listing

# 2.6.9
###### _Apr 18, 2023_
- Added debug mode


# 2.6.8
###### _Apr 12, 2023_
- Bug fixing
- Improving sendTicket method

# 2.6.7
###### _Feb 20, 2023_
- added options to download PDF file from Fattura24
- added method to get shipping price tax excluded

# 2.6.6
###### _Jan 16, 2023_
- fixed bug in displaying order list

# 2.6.5
###### _Jan 09, 2023_
- added custom order controller decorator
- added Video guides tab

# 2.6.2
###### _Dec 14, 2022_
- Changed pdf filename methods

# 2.6.0
###### _Oct 11, 2022_
- Refactored module settings page design and added Support tab
- base code review (added css and js, added methods folder)

# 2.5.4
###### _Aug 29, 2022_
- Replaced links to Fattura24 docs
  
# 2.5.3
###### _Lug 04, 2022_
- Removed methods to check module version and get update message

# 2.5.2
###### _Lug 04, 2022_
- Added currency for all document types except FE
- improved API connection error message handling

# 2.5.1
###### _Mag 16, 2022_
- Added idRequest parameter to prevent duplicates document creation

# 2.5.0
###### _Mag 04, 2022_
- Fixed bug in display invoice button when invoice creation in Fattura24 is disabled

# 2.4.9
###### _Mar 29, 2022_
- Replaced PaymentMethodDescription with $payment_method_needle for e-payments

# 2.4.8
###### _Mar 23, 2022_
 - Fixed bug in productArray loop when a gift product is in an order

## 2.4.7
###### _Mar 22, 2022_
 - Fixed bug for natura in non electronic documents

## 2.4.6
###### _Mar 07, 2022_
- Added link to video guides
- Improved order paid handling

## 2.4.5
###### _Mar 01, 2022_
- Added option to choose order number in Fattura24

## 2.4.4
###### _Feb 21, 2022_
- Improved coupon handling for percent discounts applied to product
- added check for taxRate by means of taxId
- added method to disable Fattura24 PDF linked to disable overrides option


## 2.4.3
###### _Feb 07, 2022_
- Fixed bug in $docIdFattura24 check

## 2.4.2
###### _Feb 02, 2022_
- added settings fields to enable zero total order and invoice creation
- set default payment method to MP08

## 2.4.1
###### _Dec 02, 2021_
- fixed bug in creating xml rows for gift products

## 2.4.0
###### _Dec 02, 2021_
- code review
- changed products and coupons handling
- added prestashop checkout to electronic payments

## 2.3.5
###### _Sep 21, 2021_
- fixed bug in customername

## 2.3.4
###### _Sep 15, 2021_
- added placeholder to PEC and SDI input fields
- added electronic payment method

## 2.3.3
###### _Ago 24, 2021_
- Extended log files lifecycle

## 2.3.2
###### _Giu 30, 2021_
- Improved $VatDescription handling in products
- Improved $VatDescription handling in shipping


## 2.3.1
###### _Giu 21, 2021_
- removed override AdminOrdersController

## 2.3.0
###### _Mag 25, 2021_
- added Controller to display F24 PDF file in ps 1.7.7 versions
- improved displayAdminOrder hook for ps 1.7.7 versions
  
## 2.2.6
###### _Mag 18, 2021_
- fixed bug in displayAdminOrder for cancelled orders

## 2.2.5
###### _Mag 10, 2021_
- fixed bug in method upgrade_module_2_2_4
- improved order_status handling

## 2.2.4
###### _Apr 08, 2021_
- fixed bug in Pdf download for older ps versions
- general code improvement


## 2.2.3
###### _Mar 22, 2021_
- Improved discounts handling for PS 1.6.x
- Minor bug fixed

## 2.2.2
###### _Feb 19, 2021_
- Fixed bug in customername handling
- Fixed bug in IdNumerator

## 2.2.1
###### _Feb 18, 2021_
- Changed customername field handling


## 2.2.0
###### _Feb 04, 2021_
- Changed discounts and cart rules handling
- Fixed bug in Natura list
- General code improvement

## 2.1.8
###### _Feb 04, 2021_
- Added gift product handling in discounts
- added payment method description to documents

## 2.1.7
###### _Jan 18, 2021_
- Fixed bug in discount handling and splitting for cart rules


## 2.1.6
###### _Jan 11, 2021_
- Updated Natura list
- Added error message for old Natura
- Updated pdf fileName method

## 2.1.5
###### _Dec 09, 2020_
- Updated templates
- Fixed bug in frontend order completion
- Minor bug fixes

## 2.1.2
###### _Oct 26, 2020_
- fixed bug in hook display customer admin

## 2.1.1
###### _Oct 15, 2020_
- Fixed bug in discount handling
- Updated Natura list and methods
- Fixed behaviour for ps 1.7.7

## 2.1.0
###### _Oct 06, 2020_
- Fixed bug in free shipping method

## 2.0.9
###### _Oct 05, 2020_
- Fixed bug in delOldLogFiles method
- improved VatDescription in shipping

## 2.0.8
###### _Sep 24, 2020_
- Fixed bug in API verify and Default Object buttons

## 2.0.7
###### _Sep 23, 2020_
- Changed log file name and handling

## 2.0.6
###### _Sep 07, 2020_
- Added source info for beter Api call handling
- Added expiry message to api check message

## 2.0.5
###### _Aug 24, 2020_
- changed methods to add fattura24 columns in PS orders table
- fixed bugs in Fattura24 settings page whe Api Key input field is empty
- added error handling if apiResponseOrder and apiResponseInvoice columns don't exist

## 2.0.4
###### _Aug 03, 2020_
- improved Api response time methods
## 2.0.3
###### _Lug 30, 2020_
- added Api response time methods in log file
- improved baseUrl variable for Api calls handling
## 2.0.2
###### _Lug 23, 2020_
- minor bug fixed
## 2.0.1
###### _Lug 22, 2020_
- improved CustomerVatCode logic, now it's linked to correct DocumentType
## 2.0.0
###### _Lug 09, 2020_
- fixed bugs in bankwire payment handling
- added templates for different PS versions
## 1.8.9
###### _Mag 29, 2020_
- first release in Prestashop addons