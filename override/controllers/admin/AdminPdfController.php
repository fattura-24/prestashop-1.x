<?php
/**
 * 2007-2019 PrestaShop.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    Fattura24 <fattura24.com>
 * @copyright Copyright (c) Fattura24
 * @license   Commercial license
 */

/**
 * Override del controller PDF Admin, per chi ha versioni vecchie di PS.
 */
class AdminPdfController extends AdminPdfControllerCore
{
    public function initProcess()
    {
        parent::initProcess();
    }

    public function generateInvoicePDFByIdOrder($orderId)
    {
        /*if (!Module::isEnabled('fattura24') || Configuration::get('PS_CREAZIONE_FATTURA') == 0) {
            return parent::generateInvoicePDFByIdOrder((int)$orderId);
        }*/

        $order = new Order((int) $orderId);
        if (!Validate::isLoadedObject($order)) {
            exit(Tools::displayError('The order cannot be found within your database.'));
        }

        $fileName = Module::getInstanceByName('fattura24')->constructFileName($order->id, $order->reference);
        $filePath = _PS_DOWNLOAD_DIR_ . 'Fattura24/' . $fileName;

        if (!file_exists($filePath) || filesize($filePath) < 200) {
            $query = 'SELECT docIdFattura24 FROM ' . _DB_PREFIX_ . 'orders WHERE id_order=\'' . (int) $orderId . '\';';
            $docIdFattura24 = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($query)['docIdFattura24'];

            if (!empty($docIdFattura24)) {
                Module::getInstanceByName('fattura24')->downloadDocument($docIdFattura24, $orderId, true);
            }
        }

        if (file_exists($filePath) && filesize($filePath) > 200) {
            header('Content-type: application/pdf');
            header('Content-Disposition: attachment; filename="' . $fileName . '"');
            header('Content-Transfer-Encoding: binary');
            readfile($filePath);
        } else {
            return parent::generateInvoicePDFByIdOrder((int) $orderId);
        }
    }

    public function generateInvoicePDFByIdOrderInvoice($id_order_invoice)
    {
        if (!Module::isEnabled('fattura24') || 0 == Configuration::get('PS_CREAZIONE_FATTURA')) {
            return parent::generateInvoicePDFByIdOrderInvoice((int) $id_order_invoice);
        }

        $order_invoice = new OrderInvoice((int) $id_order_invoice);
        if (!Validate::isLoadedObject($order_invoice)) {
            exit(Tools::displayError('The order invoice cannot be found within your database.'));
        }

        $orderId = (int) $order_invoice->id_order;
        $order = new Order((int) $orderId);

        $fileName = Module::getInstanceByName('fattura24')->constructFileName($order->id, $order->reference);
        $filePath = _PS_DOWNLOAD_DIR_ . 'Fattura24/' . $fileName;

        if (!file_exists($filePath) || filesize($filePath) < 200) {
            $query = 'SELECT docIdFattura24 FROM ' . _DB_PREFIX_ . 'orders WHERE id_order=\'' . $orderId . '\';';
            $docIdFattura24 = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($query)['docIdFattura24'];

            if (!empty($docIdFattura24)) {
                Module::getInstanceByName('fattura24')->downloadDocument($docIdFattura24, $orderId, true);
            }
        }

        if (file_exists($filePath) && filesize($filePath) > 200) {
            header('Content-type: application/pdf');
            header('Content-Disposition: attachment; filename="' . $fileName . '"');
            header('Content-Transfer-Encoding: binary');
            readfile($filePath);
        } else {
            return parent::generateInvoicePDFByIdOrderInvoice($id_order_invoice);
        }
    }
}
