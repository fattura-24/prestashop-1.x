<?php
/**
 * 2007-2019 PrestaShop.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    Fattura24 <fattura24.com>
 * @copyright Copyright (c) Fattura24
 * @license   Commercial license
 */

// use Order;
/**
 * Override del controller PDF front end, per chi ha versioni vecchie di PS.
 */

class PdfInvoiceController extends PdfInvoiceControllerCore
{
    public function display()
    {
        $orderId = Tools::getValue('order_id');
        $order = new Order((int) $orderId);
        $fatt24 = Module::getInstanceByName('fattura24');
        $fileName = $fatt24->constructFileName($order->id, $order->reference);
        $filePath = _PS_DOWNLOAD_DIR_ . 'Fattura24/' . $fileName;

        if (!file_exists($filePath) || filesize($filePath) < 200) {
            // $orderId = (int) $this->order->id;
            $query = 'SELECT docIdFattura24 FROM ' . _DB_PREFIX_ . 'orders WHERE id_order=\'' . (int) $orderId . '\';';
            $docIdFattura24 = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($query)['docIdFattura24'];

            if (!empty($docIdFattura24)) {
                Module::getInstanceByName('fattura24')->downloadDocument($docIdFattura24, $orderId, true);
            }
        }

        if (file_exists($filePath) && filesize($filePath) > 200) {
            header('Content-type: application/pdf');
            header('Content-Disposition: attachment; filename="' . $fileName . '"');
            header('Content-Transfer-Encoding: binary');
            readfile($filePath);

            return;
        } else {
            return parent::display();
        }
    }
}
