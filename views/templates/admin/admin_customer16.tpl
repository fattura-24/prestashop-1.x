{*
 * 2007-2019 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    Fattura24 <fattura24.com>
 * @copyright Copyright (c) Fattura24
 * @license   Commercial license
 *}

<!-- Template riquadro campi codice destinatario e pec nei dati del cliente per PS 1.6 -->
<!-- Verificare se si può dismettere -->

<div class="panel">
    <div class="panel-heading"><i class="icon-user"></i> {$section['label']|escape:'htmlall':'UTF-8'}{l s='Dati Fattura Elettronica' mod='fattura24'}</div>
    <form id="customer_note" class="form-horizontal" action="{$linkController|escape:'htmlall':'UTF-8'}" method="post" >
        <div class="row">
            <div class="form-wrapper">
                <input type="hidden" name="id_fattura24" value="{$id_fattura24|escape:'htmlall':'UTF-8'}"></input>
                <span>{l s='Codice Destinatario' mod='fattura24'}<input type="text" name="fattura24_codice_destinatario" value="{$fattura24_codice_destinatario|escape:'htmlall':'UTF-8'}" readonly></span>
                <span>{l s='PEC' mod='fattura24'}<input type="text" name="fattura24_pec" value="{$fattura24_pec|escape:'htmlall':'UTF-8'}" readonly></span>
            
                <button type="button" onclick="input_readOnly(this)">{l s='Modify' mod='fattura24'}</button>                     
            </div>
            <div class="col-lg-12">
                <button type="submit" id="submitPecSdicode" class="btn btn-default pull-right" disabled="disabled">
                    <i class="icon-save"></i>
                    {l s='Save' mod='fattura24'}
                </button>
            </div>
        </div>
    </form>
</div>                
   
<script>
    function input_readOnly(obj){
        var PecSdicodeForm = obj.form;
        PecSdicodeForm['fattura24_codice_destinatario'].readOnly = false;
        PecSdicodeForm['fattura24_pec'].readOnly = false;
        document.getElementById("submitPecSdicode").disabled = false;  
    }
</script>