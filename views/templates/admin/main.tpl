{**
 * 2007-2022 Stripe
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    202-ecommerce <tech@202-ecommerce.com>
 * @copyright Copyright (c) Stripe
 * @license   Academic Free License (AFL 3.0)
 *}

 <!-- Wrapper delle sezioni impostazioni del modulo Fattura24 -->

<div class="tabs">
    <div class="sidebar navigation col-md-2">
     {if isset($logo)}
        <img style="margin-bottom: 10px;" width="90%" class="tabs-logo" src="{$logo|escape:'htmlall':'UTF-8'}"/>
      {/if}
        <nav class="list-group categorieList">
            <a id="fattura24_settings" class="list-group-item migration-tab" href="#">
                   <i class="icon-code"></i>
                   {l s='Settings' mod='fattura24'}
                   <span class="badge-module-tabs pull-right"></span>
             </a>
             <a id="fattura24_support" class="list-group-item migration-tab" href="#">
                   <i class="icon-headphones"></i>
                   {l s='Support' mod='fattura24'}
                   <span class="badge-module-tabs pull-right"></span>
             </a>
             <a id="fattura24_video" class="list-group-item migration-tab" href="#">
                    <i class="icon-play"></i>
                   {l s='Video guides' mod='fattura24'}
                   <span class="badge-module-tabs pull-right"></span>
             </a>
         </nav>
    </div>
 
     <div class="col-md-10">
         <div id="fattura24_wrapper">
             <section id="show_settings" class="content-wrap panel">
                 {include file="./_partials/form.tpl"}
            </section>
            <section id="show_support" class="content-wrap panel">
                 {include file="./_partials/support.tpl"}
             </section>
             <section id="show_video" class="content-wrap panel">
                {include file="./_partials/video.tpl"}
             </section>
         </div>
     </div>
  </div>