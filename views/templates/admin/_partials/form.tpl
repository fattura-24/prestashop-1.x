{**
 * 2007-2019 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    Fattura24 <fattura24.com>
 * @copyright Copyright (c) Fattura24
 * @license   Commercial license
 *}

 <!-- Template pagina impostazioni generali modulo Fattura24 -->
 <form id="module_form1" class="defaultForm form-horizontal"
 action="#fattura24_settings"
 method="post" enctype="multipart/form-data" novalidate>
 <input type="hidden" name="submitModule" value="1"/>
 <div class="panel-heading">
 {l s='GENERAL SETTINGS' mod='fattura24'}
 </div>
 <div class="panel" id="fieldset_0">
          <div class="form-wrapper">
             <div class="form-group col-lg-12" style="margin-top:20px">
                 <button type="submit" value="1" id="module_form_submit_btn_header" name="submitModule"
                         class="btn btn-primary pull-right">
                         {l s='Save' mod='fattura24'}
                 </button>
             </div>
         </div>

         <div class="form-group">
             <label class="fattura24-label col-lg-2">{l s='Module version' mod='fattura24'}</label>
                 
             <div class="col-lg-1">
                 <div class="fattura24-field">
                     <h4 id='version'>{$version|escape:'htmlall':'UTF-8'}</h4>
                 </div>
             </div>
             <!-- Questo elemento nascosto mi serve per fare il test della chiave API con il nuovo parametro source -->
             <div class="col-lg-1" hidden>
                 <div class="fattura24-field">
                     <h4 id='source'>{$source|escape:'htmlall':'UTF-8'}</h4>
                 </div>
             </div>
         </div>

         <div class="form-group">
             <label class="fattura24-label col-lg-2">
                 {l s='Api Key' mod='fattura24'}
             </label>

             <div class="fattura24-field col-lg-3">
                 <input type="text"
                        name="PS_FATTURA24_API"
                        id="PS_FATTURA24_API"
                        value="{$apiKey|escape:'htmlall':'UTF-8'}"
                 />
             </div>

             <div>  
                <button type="submit" name='testApiKey' class="btn btn-default" id="verify" value="3">{l s='API Key check' mod='fattura24'}</button>&nbsp;
                     <!--<span id="result">
                     </span>-->
             </div>                 
         </div>
         <br/>

         <div class="form-group col-lg-12">
             <h2>{l s='Address book' mod='fattura24'}</h2>
             <hr>
         </div>
                     
         <div class="form-group">
             <label class="fattura24-label col-lg-2">
                    {l s='Save customer' mod='fattura24'}
                    <span class="help">
                         <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                            title="Spunta questo campo per salvare i dati del cliente nella rubrica di Fattura24 
                            quando viene creato un ordine in Prestashop o quando crei un documento (ordine o fattura) in Fattura24">?</a>
                    </span>
             </label>
      
             <div class="col-lg-1">
                  <div class="checkbox">
                       <label for="PS_SALVA_CLIENTE">
                             <input type="checkbox" name="PS_SALVA_CLIENTE"
                                    id="PS_SALVA_CLIENTE" class="" value="1"
                                    {if $salvaCliente eq 1}
                                         checked="checked"
                                    {/if}
                             />
                        </label>
                   </div>
             </div>
         </div>
         <br/>
         
         <div class="form-group col-lg-12">
              <h2>{l s='Orders' mod='fattura24'}</h2>
              <hr>
         </div>

         <div class="form-group">
             <label class="fattura24-label col-lg-2">
                 {l s='Create order' mod='fattura24'}&nbsp;
                     <span class="help">
                         <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                            title="Spunta questo campo se vuoi attivare la creazione dell'ordine in Fattura24">?</a>
                     </span>
             </label>
             <div class="col-lg-1">
                 <div class="checkbox">
                     <label for="PS_CREA_ORDINE">
                         <input type="checkbox" name="PS_CREA_ORDINE"
                             id="PS_CREA_ORDINE" class="" value="1"
                             {if $creazioneOrdine eq 1}
                                 checked="checked"
                             {/if}
                         />
                     </label>
                 </div>
             </div>
        </div>

        <div class="form-group">
             <label class="fattura24-label col-lg-2">
                 {l s='Enable order creation when order total is zero' mod='fattura24'}&nbsp;
                     <span class="help">
                         <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                            title="Spunta questo campo per abilitare la creazione di ordini con totale zero">?</a>
                     </span>
             </label>
             <div class="col-lg-1">
                 <div class="checkbox">
                     <label for="PS_ABILITA_ORDINE_ZERO">
                         <input type="checkbox" name="PS_ABILITA_ORDINE_ZERO"
                             id="PS_ABILITA_ORDINE_ZERO" class="" value="1"
                             {if $abilitaOrdineZero eq 1}
                                 checked="checked"
                            {/if}
                         />
                     </label>
                 </div>
             </div>
        </div>


        <div class="form-group">
        <label class="fattura24-label col-lg-2">
            {l s='Order number' mod='fattura24'}&nbsp;
                <span class="help">
                    <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                       title="Scegli quale dato usare come numero di ordine in F24">?</a>
                </span>
        </label>
             <div class="col-lg-4">
                 <div class="fattura24-field">
                     <select name="PS_NUMERO_ORDINE" id="PS_NUMERO_ORDINE">
                         {html_options options=$numOrdine selected=$numOrdineSelected}
                     </select>
                 </div>    
             </div>
        </div>

        <div class="form-group">
             <label class="fattura24-label col-lg-2">
                 {l s='Download PDF' mod='fattura24'}&nbsp;
                 <span class="help">
                     <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                         title="Spunta questo campo se vuoi che venga scaricato automaticamente in locale il PDF dell'ordine">?</a>
                 </span>
             </label>
             <div class="col-lg-1">
                 <div class="checkbox">
                     <label for="PS_PDF_ORDINE">
                         <input type="checkbox" name="PS_PDF_ORDINE"
                                id="PS_PDF_ORDINE"
                                value="1"
                                {if $pdfOrdine eq 1}
                                     checked="checked"
                                {/if}
                         />
                     </label>
                 </div>
             </div>
         </div>

         <div class="form-group">
             <label class="fattura24-label col-lg-2">
                 {l s='Send email' mod='fattura24'} &nbsp;
                 <span class="help">
                     <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                         title="Spunta questo campo se vuoi che il PDF dell'ordine venga spedito automaticamente al cliente via email">?</a>
                 </span>
             </label>
             <div class="col-lg-1">
                 <div class="checkbox">
                      <label for="PS_MAIL_ORDINE">
                         <input type="checkbox" name="PS_MAIL_ORDINE"
                                id="PS_MAIL_ORDINE"
                                value="1"
                                {if $emailOrdine eq 1}
                                     checked="checked"
                                {/if}
                         />
                      </label>
                 </div>
             </div>
         </div>

        

         <div class="form-group">
             <label class="fattura24-label col-lg-2">
                 {l s='Order template' mod='fattura24'}&nbsp;
                 <span class="help">
                     <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                        title="Seleziona il modello da usare per la creazione del PDF dell'ordine tra i tuoi modelli ordine in Fattura24. 
                        Questo modello verrà usato se nell'ordine non è presente una destinazione.
                        Se è selezionato 'Predefinito', sarà usato il modello impostato come modello predefinito in Fattura24. 
                        Per visualizzare la lista, devi prima salvare la tua Api Key di Fattura24 in Prestashop">?</a>
                 </span>
             </label>
             <div class="col-lg-4">
                 <div class="fattura24-field">
                     <select name="PS_F24_MODELLO_ORDINE" id="PS_F24_MODELLO_ORDINE">
                         {html_options options=$listaModelliOrdine selected=$modelloOrdine}
                     </select>
                 </div>
             </div>
         </div>

         <div class="form-group">
             <label class="fattura24-label col-lg-2">
                 {l s='Order template with destination' mod='fattura24'}&nbsp;
                 <span class="help">
                     <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                        title="Seleziona il modello da usare per la creazione del PDF dell'ordine tra i tuoi modelli ordine in Fattura24. 
                        Questo modello verrà usato se nell'ordine è presente una destinazione.
                        Se è selezionato 'Predefinito', sarà usato il modello impostato come modello predefinito in Fattura24. 
                        Per visualizzare la lista, devi prima salvare la tua Api Key di Fattura24 in Prestashop">?</a>
                 </span>
             </label>
             <div class="col-lg-4">
                 <div class="fattura24-field">
                     <select name="PS_F24_MODELLO_ORDINE_DEST" id="PS_F24_MODELLO_ORDINE_DEST">
                         {html_options options=$listaModelliOrdine selected=$modelloOrdineDest}
                     </select>
                 </div>
             </div>
         </div>
         <br/>

         <div class="form-group col-lg-12">
             <h2>{l s='Invoices' mod='fattura24'}</h2>
             <hr>
         </div>

         <div class="form-group">
             <label class="fattura24-label col-lg-2">
                 {l s='Create invoice' mod='fattura24'}&nbsp;
                 <span class="help">
                     <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                        title="Spunta questo campo se vuoi che venga creata la fattura tramite Fattura24 e che sia scaricata sul tuo Prestashop quando lo stato 
                        dell'ordine viene impostato come 'pagato">?</a>
                 </span>
             </label>
             
             <div class="col-lg-3">
                 <div class="fattura24-field">
                     <select name="PS_CREAZIONE_FATTURA" id="PS_CREAZIONE_FATTURA">
                         {html_options options=$opzioniCreaFattura selected=$creazioneFattura}
                     </select>
                 </div>
             </div>
         </div>

         <div class="form-group">
             <label class="fattura24-label col-lg-2">
                 {l s='Enable invoice creation when order total is zero' mod='fattura24'}&nbsp;
                     <span class="help">
                         <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                            title="Spunta questo campo per abilitare la creazione di fatture con totale zero">?</a>
                     </span>
             </label>
             <div class="col-lg-1">
                 <div class="checkbox">
                     <label for="PS_ABILITA_FATTURA_ZERO">
                         <input type="checkbox" name="PS_ABILITA_FATTURA_ZERO"
                             id="PS_ABILITA_FATTURA_ZERO" class="" value="1"
                             {if $abilitaFatturaZero eq 1}
                                 checked="checked"
                            {/if}
                         />
                     </label>
                 </div>
             </div>
        </div>
        <div class="form-group">
        <label class="fattura24-label col-lg-2">
            {l s='Download PDF' mod='fattura24'}&nbsp;
            <span class="help">
                <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                    title="Spunta questo campo se vuoi che venga scaricato automaticamente in locale il PDF dell'ordine">?</a>
            </span>
        </label>
        <div class="col-lg-1">
            <div class="checkbox">
                <label for="PS_PDF_FATTURA">
                    <input type="checkbox" name="PS_PDF_FATTURA"
                           id="PS_PDF_FATTURA"
                           value="1"
                           {if $pdfFattura eq 1}
                                checked="checked"
                           {/if}
                    />
                </label>
            </div>
        </div>
    </div>
                     
         <div class="form-group">
             <label class="fattura24-label col-lg-2">
                {l s='Purpose of payment' mod='fattura24'}&nbsp; 
                <span class="help">
                     <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                        title="Inserisci (N) per riportare in causale il numero d'ordine, (R) per il riferimento Prestashop, o una combinazione di entrambi">?</a>
                </span>
             </label>
             
             <div class="fattura24-field col-lg-3">
                 <input type="text"
                        name="PS_INV_OBJECT"
                        id="PS_INV_OBJECT"
                        value="{$invObject|escape:'htmlall':'UTF-8'}"
                 />
             </div>

             <div class="col-lg-2">
                 <div>
                     <button type="button" class="btn btn-default" id="F24Object"> Predefinito </button>&nbsp; <!-- pulsante aggiornato in data 24.10.2019 -->
                 </div>
             </div>
         </div>
                     
         <div class="form-group">
             <label class="fattura24-label col-lg-2">
                 Natura IVA&nbsp; 
                 <span class="help">
                     <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                        title="Inserisci">?</a>
                 </span>
             </label>
                
             <div class="col-lg-2">
                 <div class="fattura24-field">
                     <input type="text"
                            name="PS_CONF_NATURA_IVA"
                            id="PS_CONF_NATURA_IVA"
                            value="{$confNaturaIva|escape:'htmlall':'UTF-8'}"
                     />
                 </div>
             </div>
         </div>
                 
         <div class="form-group">
             <label class="fattura24-label col-lg-2">
                 Natura IVA Spedizione&nbsp; 
                 <span class="help">
                     <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                        title="Scegli">?</a>
                 </span>
             </label>

             <div class="col-lg-2">
                 <div class="fattura24-field">
                     <select name="PS_CONF_NATURA_SPEDIZIONE" id="PS_CONF_NATURA_SPEDIZIONE">
                         {html_options options=$tipiAliquoteIva selected=$naturaSpedizione}
                     </select>
                 </div>
             </div>
         </div>
             
         <div class="form-group">
             <label class="fattura24-label col-lg-2">
                 {l s='Send email' mod='fattura24'}&nbsp;
                 <span class="help">
                     <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                         title="Spunta questo campo se vuoi che il PDF della fattura venga spedito automaticamente al cliente via email (solo documenti NON elettronici)">?</a>
                 </span>
             </label>
             <div class="col-lg-1">
                 <div class="checkbox">
                     <label for="PS_EMAIL_FATTURA">
                         <input type="checkbox" name="PS_EMAIL_FATTURA"
                                id="PS_EMAIL_FATTURA"
                                value="1"
                                {if $emailFattura eq 1}
                                     checked="checked"
                                {/if}
                         />
                     </label>
                 </div>
             </div>
         </div>



         <div class="form-group">
            <label class="fattura24-label col-lg-2">
                 {l s='Paid status' mod='fattura24'}&nbsp;
                 <span class="help">
                     <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                        title="Scegli la condizione per creare il documento nello stato 'Pagato'">?</a>
                 </span>
            </label>
            <div class="col-lg-3">
                 <div class = "fattura24-field">
                     <select name="PS_STATO_PAGATO" id="PS_STATO_PAGATO">
                         {html_options options=$opzioniStatoPagato selected=$statoPagato}
                     </select>
                 </div>
            </div>
         </div>

         <div class="form-group">
            <label class="fattura24-label col-lg-2">
                {l s='Disable receipts' mod='fattura24'}&nbsp;
                <span class="help">
                      <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                         title="Spunta questo campo se vuoi che la fattura sia creata anche in assenza della Partita IVA del cliente">?</a>
                </span>
            </label>
            <div class="col-lg-1">
                 <div class="checkbox">
                    <label for="PS_DISABILITA_RICEVUTE">
                        <input type="checkbox" name="PS_DISABILITA_RICEVUTE"
                               id="PS_DISABILITA_RICEVUTE"
                               value="1"
                               {if $disabilitaRicevute eq 1}
                                    checked="checked"
                               {/if}
                        />
                    </label>
                 </div>
             </div>
         </div>

             <div class="form-group">
                 <label class="fattura24-label col-lg-2">
                     {l s='Invoice template' mod='fattura24'}&nbsp;
                     <span class="help">
                         <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                         title="Seleziona il modello da usare per la creazione del PDF della fattura tra i tuoi modelli fattura in Fattura24. 
                         Questo modello verrà usato se nell'ordine non è presente una destinazione.
                         Se è selezionato 'Predefinito', sarà usato il modello impostato come modello predefinito in Fattura24. 
                         Per visualizzare la lista, devi prima salvare la tua Api Key di Fattura24 in Prestashop">?</a>
                     </span>
                 </label>

                 <div class="col-lg-4">
                     <div class="fattura24-field">
                         <select name="PS_F24_MODELLO_FATTURA" id="PS_F24_MODELLO_FATTURA">
                             {html_options options=$listaModelliFattura selected=$modelloFattura}
                         </select>
                     </div>
                 </div>
             </div>

             <div class="form-group">
                 <label class="fattura24-label  col-lg-2">
                     {l s='Invoice template with destination' mod='fattura24'}&nbsp;
                     <span class="help">
                         <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                         title="Seleziona il modello da usare per la creazione del PDF della fattura tra i tuoi modelli fattura in Fattura24. 
                         Questo modello verrà usato se nell'ordine è presente una destinazione.
                         Se è selezionato 'Predefinito', sarà usato il modello impostato come modello predefinito in Fattura24. 
                         Per visualizzare la lista, devi prima salvare la tua Api Key di Fattura24 in Prestashop">?</a>
                     </span>
                 </label>
                 <div class="col-lg-4">
                     <div class="fattura24-field">
                         <select name="PS_F24_MODELLO_FATTURA_DEST" id="PS_F24_MODELLO_FATTURA_DEST">
                             {html_options options=$listaModelliFattura selected=$modelloFatturaDest}
                         </select>
                     </div>
                 </div>
             </div>

             <div class="form-group">
                 <label class="fattura24-label col-lg-2">
                     {l s='Account planning' mod='fattura24'}&nbsp;
                     <span class="help">
                         <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                            title="Puoi selezionare il conto da associare alle prestazioni/prodotti dei documenti tra i tuoi conti in Fattura24. 
                            Per visualizzare la lista, devi prima salvare la tua Api Key di Fattura24 in Prestashop">?</a>
                     </span>
                 </label>
                         
                 <div class="col-lg-4">
                     <div class="fattura24-field">
                         <select name="PS_F24_PDC" id="PS_F24_PDC">
                             {html_options options=$listaConti selected=$conto}
                         </select>
                     </div>
                 </div>
             </div>

             <div class="form-group">
                 <label class="fattura24-label col-lg-2">
                     {l s='Receipts issue number' mod='fattura24'}&nbsp;
                     <span class="help">
                         <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                         title="Puoi selezionare il sezionale da usare per la numerazione delle ricevute tra i sezionali attivi per le ricevute in Fattura24.
                         Se è selezionato 'Predefinito', sarà usato il sezionale che hai impostato come predefinito in Fattura24.
                         Per visualizzare la lista, devi prima salvare la tua Api Key di Fattura24 in Prestashop">?</a>
                     </span>
                 </label>
                 <div class="col-lg-2">
                     <div class="fattura24-field">
                         <select name="PS_F24_SEZIONALE_RICEVUTA" id="PS_F24_SEZIONALE_RICEVUTA">
                             {html_options options=$listaSezionaliRicevuta selected=$sezionaleRicevuta}
                         </select>
                     </div>
                 </div>
             </div>
             
             <div class="form-group">
                 <label class="fattura24-label col-lg-2">
                  {l s='Invoices issue number' mod='fattura24'}&nbsp;
                     <span class="help">
                         <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                            title="Seleziona il sezionale da usare per la numerazione delle fatture tra i sezionali attivi per le fatture in Fattura24.
                            Se è selezionato 'Predefinito', sarà usato il sezionale che hai impostato come predefinito in Fattura24.
                            Per visualizzare la lista, devi prima salvare la tua Api Key di Fattura24 in Prestashop">?</a>
                     </span>
                 </label>
                 <div class="col-lg-2">
                     <div class="fattura24-field">
                         <select name="PS_F24_SEZIONALE_FATTURA" id="PS_F24_SEZIONALE_FATTURA">
                            {if $creazioneFattura neq 1}
                                 {html_options options=$listaSezionaliFattura selected=$sezionaleFattura}
                            {elseif $creazioneFattura eq 1}
                                 {html_options options=$listaSezionaliFatturaElettronica selected=$sezionaleFattura}
                            {/if}
                         </select>
                     </div>
                 </div>
             </div>

              <div class="form-group">
                 <label class="fattura24-label col-lg-2">
                     {l s='Electronic invoices virtual stamp' mod='fattura24'}&nbsp;
                     <span class="help">
                         <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                         title="Pagherò il bollo virtuale per conto del cliente">?</a>
                     </span>
                 </label>
                 <div class="col-lg-4">
                     <div class="fattura24-field">
                         <select name="PS_F24_BOLLO_VIRTUALE_FE" id="PS_F24_BOLLO_VIRTUALE_FE">
                             {html_options options=$bolloVirtualeFe selected=$bollo}
                         </select>
                     </div>
                 </div>
             </div>
             <br/>
                     
             <div class="form-group col-lg-12">
                 <h2>{l s='Advanced' mod='fattura24'}</h2>
                 <hr>
             </div>
             
             <div class="form-group">
             <label class="fattura24-label col-lg-2">
                    {l s='Enable debug' mod='fattura24'}
                    <span class="help">
                         <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                            title="Spunta questo campo per salvare i dati del cliente nella rubrica di Fattura24 
                            quando viene creato un ordine in Prestashop o quando crei un documento (ordine o fattura) in Fattura24">?</a>
                    </span>
             </label>
      
             <div class="col-lg-1">
                  <div class="checkbox">
                       <label for="PS_F24_DEBUG_MODE">
                             <input type="checkbox" name="PS_F24_DEBUG_MODE"
                                    id="PS_F24_DEBUG_MODE" style="margin-top: -10px;" value="1"
                                    {if $debugEnable eq 1}
                                         checked="checked"
                                    {/if}
                             />
                        </label>
                   </div>
             </div>
         
             </div>       
             <div id="button-wrapper" class="form-group"
                 {if $debugEnable eq 0}
                     hidden=true
                 {/if}
             >
                 <div class="col-lg-2">                        
                     <input type="button" name="PS_F24_LOGFILE" class="btn btn-default" 
                            id="download_log_file" value="{l s='Download log file' mod='fattura24'}"
                            
                     />&nbsp;&nbsp;&nbsp;
                         <span class="help">
                             <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                             title="Scarica e invia questo file a info@fattura24.com quando riscontri problemi con il modulo di Fattura24">?</a>
                         </span>
                 </div>
             </div>
             <br/>

             <div class="form-group">
                 <h2>{l s='DOCUMENTATION' mod='fattura24'}</h2>
                 <hr>
                 <button type="submit" value="1" id="module_form_submit_btn_footer" name="submitModule"
                         class="btn btn-primary pull-right">
                         {l s='Save' mod='fattura24'}
                 </button>

                 <a class="btn btn-default" href="https://www.app.fattura24.com/v3/" target="_blank">{l s='Log in Fattura24' mod='fattura24'} </a><br />
                 <br /><br />
                 <br /><br />
                 L'uso del modulo e del gestionale Fattura24 sono regolati dalle Condizioni di contratto, dall'Informativa Privacy e dal Regolamento integrativo del modulo (v. sotto i rispettivi link).
                 Fattura24 non rende il servizio di conservazione legale dei documenti fiscali e delle fatture elettroniche, per le quali il singolo utente dovrà provvedere autonomamente con i suoi mezzi, ricorrendo eventualmente anche, per le fatture elettroniche trasmesse tramite Sdi, al servizio gratuito di conservazione dell'Agenzia delle Entrate
                 <br /><br />
                             
                 <a href= "https://www.fattura24.com/prestashop/introduzione" target="_blank"> Supporto </a><br />
                 <a href="https://www.youtube.com/watch?v=exrmoG-n-OA&list=PLCvEiE9DaQUKogQjbMgkEIzFlueV-clBI" target="_blank"> Video guide </a><br />
                 <a href= "https://www.fattura24.com/documentazione-legale/condizioni-di-contratto" target="_blank"> Condizioni di contratto e termini di utilizzo di Fattura24</a><br />
                 <a href= "https://www.fattura24.com/regolamento-ecommerce/" target="_blank"> Regolamento F24 integrativo (delle Condizioni di Contratto) per modulo Prestashop</a><br />
                 <a href= "https://www.fattura24.com/documentazione-legale/informativa-privacy" target="_blank"> Privacy di Fattura24</a><br />
                 <a style ="color: darkred;" href= "https://www.fattura24.com/circolare-14e-ade-del-17-giugno-2019/" target="_blank"> Circolare Agenzia Entrate sul campo della Data-Fattura</a><br />
                                     
             </div>
         </div>
     </div><!-- /.form-wrapper -->
 </div>
</form>
<script type="text/javascript">
 function downloadFile(dataurl, filename)
 {
     var a = document.createElement("a");
     a.href = dataurl;
     console.log(dataurl);
     a.setAttribute("download", filename);
     var b = document.createEvent("MouseEvents");
     b.initEvent("click", false, true);
     a.dispatchEvent(b);
     return false;
 }

 document.addEventListener("DOMContentLoaded", function (e)
 {
     var downloadLogFile = document.getElementById("download_log_file");
     var localFileNamewithPath = '{$logFileName|escape:'htmlall':'UTF-8'}';
     var needleIndex = localFileNamewithPath.lastIndexOf('/');
     var modulePath = localFileNamewithPath.slice(0, needleIndex);
     var localFileName = localFileNamewithPath.split('/');
     var lastIndex = localFileName.length - 1;
     downloadLogFile.addEventListener('click', function (e)
     {
         $.ajax({
             method: 'POST',
             data: 'info={$info|escape:'htmlall':'UTF-8'}',
             url: '{$moduleUrl|escape:'htmlall':'UTF-8'}' + 'log/downloadLogFile.php',
             dataType: 'json',
            
             /*error: function ()
             {
                alert('Errore durante il download.');
             },*/
             complete: function(response)
             {   
                 if (response.responseJSON.fileExists === true) {
                     var today = new Date();
                     var dateformat = today.getDate() + '-' + (today.getMonth()+1) + '-' + today.getFullYear() + '_' + today.getHours() + '_' + today.getMinutes();
                     var new_filename = 'f24_trace_' + dateformat + '.log';
                     downloadFile('{$moduleUrl|escape:'htmlall':'UTF-8'}' + 'log/' + localFileName[lastIndex], new_filename);
                 } else {
                     alert('Errore durante il download.');
                 }
             }
         });
     });
});
</script>