{*
 * 2007-2019 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    Fattura24 <fattura24.com>
 * @copyright Copyright (c) Fattura24
 * @license   Commercial license
 *}

 <!-- Template pagina videolezioni modulo Fattura24 -->
 <div class="defaultForm form-horizontal fattura24">
    <div class="panel-heading">
            {l s='VIDEO GUIDES' mod='fattura24'}
    </div>    
    <div class="panel" id="fieldset_2">
        <div class="form-wrapper" style="font-size: 14px;">
            <table border="0px" width="800">
               <tbody>
               <!-- cfr.: https://www.smarty.net/docsv2/it/language.function.foreach.tpl -->
               {foreach $lezioni as $lezione}
                    <tr>
                        <td style="padding-bottom: 10px;">
                            <a href={$lezione.link|escape:'htmlall':'UTF-8'} target="_blank">
                                <img src='{$lezione.img|escape:'htmlall':'UTF-8'}' width="480" style="border-radius: 7px;" target='_blank' alt={$lezione.description|escape:'htmlall':'UTF-8'} />
                            </a>
                        </td>
                        <td style="padding: 10px;">&nbsp;</td>
                        <td style="padding-bottom: 10px;">
                            {l s='%s' sprintf=[$lezione.description] mod='fattura24'}             
                        </td>
                    </tr>
                {/foreach}
                </tbody>
            </table>
        </div>
     </div>
</div>
