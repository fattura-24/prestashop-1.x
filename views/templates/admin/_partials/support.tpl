{*
 * 2007-2019 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    Fattura24 <fattura24.com>
 * @copyright Copyright (c) Fattura24
 * @license   Commercial license
 *}

<!-- Template pagina supporto modulo Fattura24 -->
<form id="module_form2" class="defaultForm form-horizontal fattura24" action="#" method="post" enctype="multipart/form-data" novalidate="">
    <div class="panel-heading" style="margin: initial;">
            {l s='SUPPORT' mod='fattura24'}
    </div>    
    <div class="panel" id="fieldset_1">
        <div class="module_warning alert alert-warning">
            <h3 style="background-color: #fffbd3; text-align: center; text-transform: uppercase">{l s='Notice' mod='fattura24'}</h3>
            {l s='Fill in the form fields to send a support request to Fattura24 tech service.' mod='fattura24'}<br />
            {l s='If you need so, you may provide the id or the reference' mod='fattura24'} 
            {l s=' of an order in which you found an error' mod='fattura24'}<br />
            {l s='This form will create a text file and send it to Fattura24 as attachment' mod='fattura24'}<br />
            {l s='In this file we will collect: an email address to send you an answer,' mod='fattura24'}
            {l s=' module settings and order data (if field is not empty)' mod='fattura24'}<br />
            {l s='No data will be sent if text area is empty; the text file will be deleted' mod='fattura24'}
            {l s=' from server after you send the message' mod='fattura24'}<br />
            {l s='By clicking on send ticket you accept to share these data with Fattura24' mod='fattura24'}
            {l s='for all the time needed to answer your request' mod='fattura24'}
        </div>    
        <div class="form-wrapper">
            <div class="form-group">
                <label class="control-label col-lg-3">
                    {l s='Subject' mod='fattura24'}
                </label>
                <div class="col-lg-9">
                    <input type="text" name="subject" id="subject" value="">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3">
                    {l s='Reply to' mod='fattura24'}
                </label>
                <div class="col-lg-9">
                    <input type="text" name="email" id="email" value="{$adminEmail|escape:'htmlall':'UTF-8'}" readonly="true">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3">
                {l s='Order id or reference' mod='fattura24'}
                </label>
                    <div class="col-lg-9">
                        <input type="text" name="order_id_reference" id="order_id_reference" value="" placeholder="{l s='Put here only one id or reference' mod='fattura24'}">
                    </div>
                   
                  
                </div>
            
            <div class="form-group">
                <label class="control-label col-lg-3">
                    {l s='Message' mod='fattura24'}
                </label>
                <div class="col-lg-9">
                    <textarea 
                          id="message" 
                          name="text_message" 
                          rows="8"
                          cols="60"
                          >
                    </textarea>
                </div>
            </div>
        <div class="panel-footer">
            <button type="submit" value="1" id="module_form_submit_btn" name="submitSupport" class="btn btn-primary pull-right">
                {l s='Send ticket' mod='fattura24'}
            </button>
        </div>
    </div>
</form>