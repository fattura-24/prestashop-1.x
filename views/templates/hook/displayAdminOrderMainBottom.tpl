{*
 * 2007-2019 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    Fattura24 <fattura24.com>
 * @copyright Copyright (c) Fattura24
 * @license   Commercial license
 *}

  <!-- template elemento HTML nel dettaglio dell'ordine -->

 <div style="position: relative; top: -100px; display: inline-flex;" class="card col-md-3" id="Fattura24">
 <div class="card-header">
     <h3 class="card-header-title"><i class="material-icons">monetization_on</i> {l s='Fattura24' mod='fattura24'}
({$order_documents|escape:'htmlall':'UTF-8'})</span></h3>
 </div>
 <div class="info-block">
     <div id="config_CreaFattura" hidden>{$config_CreaFattura|escape:'htmlall':'UTF-8'}</div>
     <div id="config_Overrides" hidden>{$config_Overrides|escape:'htmlall':'UTF-8'}</div>
     <div id="file_exists" hidden>{$file_exists|escape:'htmlall':'UTF-8'}</div>
     <div class="row d-print-block">
         <div class="info-block-col col-xl-6">
             <strong>{l s='Order status' mod='fattura24'}   </strong>&nbsp;
             <div id="f24_order">{$fattura24_order|escape:'htmlall':'UTF-8'}</div>&nbsp;
         </div>
             <div class="info-block-col col-md-6">
                 <strong>{l s='Invoice status' mod='fattura24'}   </strong>&nbsp;
                 <div id="f24_invoice">{$fattura24_invoice|escape:'htmlall':'UTF-8'}</div>&nbsp;
             </div>    
         </div>
     </div>    
 </div>
</div>        
