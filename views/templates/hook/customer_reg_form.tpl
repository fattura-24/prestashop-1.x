{*
 * 2007-2019 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    Fattura24 <fattura24.com>
 * @copyright Copyright (c) Fattura24
 * @license   Commercial license
 *}
 
<!-- template form di registrazione cliente al negozio --> 
<input type="hidden" name="id_fattura24" value="{if isset($id_fattura24)}{$id_fattura24|escape:'htmlall':'UTF-8'}{else}{/if}">
<input type="hidden" name="fattura24_id_customer" value="{$fattura24_id_customer|escape:'htmlall':'UTF-8'}">
<div class="form-group row">
    <label class="col-md-3 form-control-label">{l s='PEC Email address' mod='fattura24'}</label>
    <div class="col-md-6">
        <input type="email" name="fattura24_pec" value="{if isset($smarty.post.fattura24_pec)}{$smarty.post.fattura24_pec|escape:'htmlall':'UTF-8'}{else}{$fattura24_pec|escape:'htmlall':'UTF-8'}{/if}" placeholder="{l s='Optional' mod='fattura24'}"/>
    </div>
</div>
<div class="form-group row">

    <label class="col-md-3 form-control-label">{l s='SDI code' mod='fattura24'}</label>
    <div class="col-md-6">
        <input type="text" maxlength="7" name="fattura24_codice_destinatario" value="{if isset($smarty.post.fattura24_codice_destinatario)}{$smarty.post.fattura24_codice_destinatario|escape:'htmlall':'UTF-8'}{else}{$fattura24_codice_destinatario|escape:'htmlall':'UTF-8'}{/if}" placeholder="{l s='Optional' mod='fattura24'}"/>
    </div>
</div>
