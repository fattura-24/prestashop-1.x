{*
 * 2007-2019 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    Fattura24 <fattura24.com>
 * @copyright Copyright (c) Fattura24
 * @license   Commercial license
 *}

 <!-- template elemento HTML nel dettaglio dell'ordine -->
<div class="panel" id="Fattura24">
    <div class="panel-heading">
        <i class="icon-money"></i>{l s='Fattura24' mod='fattura24'}
        <span class="badge">{$order_documents|escape:'htmlall':'UTF-8'}</span>
    </div>
        <table>
            <tbody>
                <thead>
                    <tr>
                        <td><strong>{l s='Order status' mod='fattura24'}   </strong></td>
                        <td style="padding-left: 20px;"><strong>{l s='Invoice status' mod='fattura24'}  </strong></td>
                    </tr>
                </thead>  

                <tr>
                    <td id="f24_order">{$fattura24_order|escape:'htmlall':'UTF-8'}</td>
                    <td id="f24_invoice" style="padding-left: 20px;">{$fattura24_invoice|escape:'htmlall':'UTF-8'}</td>
                </tr>
                
                <!--<tr>
                    <td>
                        {if $order_status_name neq '' && $fattura24_order_docId == '' && $config_CreaOrdine eq 1}  
                           <button type="submit" name="submitOrder" class="btn btn-primary">{l s='Save order' mod='fattura24'}</button>
                        {/if}
                    </td>
                    
                    <td style="padding-left: 20px;">
                        {if $order_status_name neq '' && $fattura24_invoice_docId == '' && $config_CreaFattura neq 0 } 
                            <button type="submit" name="submit_invoice" id="submit_invoice" class="btn btn-primary">{l s='Save invoice' mod='fattura24'}</button>
                        {/if}
                    </td>
                </tr>-->
            </tbody>
        </table>
    </div>
</div>    
