/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License version 3.0
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License version 3.0
 */ 

/**
 * Funzioni JS legate al comportamento dei campi nella schermata si amministrazione
 * abilita / disablita modifica del campo e simili
 */

function toggle(element, status){
    if (status === 'disabled') {
        $(element).prop('checked', false);
        $(element).prop('disabled', true);
    } else if (status === 'readonly') {
        $(element).prop('disabled', true);
    } else {
        $(element).prop('disabled', false);
    }
    var panels = document.getElementsByClassName('panel');
    var panel = panels[0];
}    

window.addEventListener('load', () => {

        // default Object button
        let Objectbtn = document.getElementById("F24Object");

        Objectbtn.addEventListener('click', function (e) {
            // set input field default value
            document.getElementById("PS_INV_OBJECT").defaultValue = "Ordine (N) Shop on-line";
        });  

        // Rubrica
        const PS_SALVA_CLIENTE = document.getElementById('PS_SALVA_CLIENTE');

        // Ordini
        const PS_CREA_ORDINE = document.getElementById('PS_CREA_ORDINE');
        const PS_ABILITA_ORDINE_ZERO = document.getElementById('PS_ABILITA_ORDINE_ZERO');
        const PS_PDF_ORDINE = document.getElementById('PS_PDF_ORDINE');
        const PS_MAIL_ORDINE = document.getElementById('PS_MAIL_ORDINE');
        const PS_F24_MODELLO_ORDINE = document.getElementById('PS_F24_MODELLO_ORDINE');
        const PS_F24_MODELLO_ORDINE_DEST = document.getElementById('PS_F24_MODELLO_ORDINE_DEST');
        

        if (!PS_CREA_ORDINE.checked){
            toggle(PS_ABILITA_ORDINE_ZERO, 'disabled');
            toggle(PS_PDF_ORDINE, 'disabled');
            toggle(PS_MAIL_ORDINE, 'disabled');
            toggle(PS_F24_MODELLO_ORDINE, 'disabled');
            toggle(PS_F24_MODELLO_ORDINE_DEST, 'disabled');
        }
        if (PS_CREA_ORDINE.checked){
            PS_SALVA_CLIENTE.checked = true;
            PS_SALVA_CLIENTE.disabled = true;
        }

          // Listener per creazione ordine
        PS_CREA_ORDINE.addEventListener('change', function (e){
            if (PS_CREA_ORDINE.checked){
                toggle(PS_ABILITA_ORDINE_ZERO);
                toggle(PS_PDF_ORDINE);
                toggle(PS_MAIL_ORDINE);
                toggle(PS_F24_MODELLO_ORDINE);
                toggle(PS_F24_MODELLO_ORDINE_DEST);
                PS_SALVA_CLIENTE.checked = true;
                PS_SALVA_CLIENTE.disabled = true;
            }
            else{
                toggle(PS_ABILITA_ORDINE_ZERO, 'disabled');
                toggle(PS_PDF_ORDINE, 'disabled');
                toggle(PS_MAIL_ORDINE, 'disabled');
                toggle(PS_F24_MODELLO_ORDINE, 'disabled');
                toggle(PS_F24_MODELLO_ORDINE_DEST, 'disabled');
                if(!PS_CREAZIONE_FATTURA.checked)
                    PS_SALVA_CLIENTE.disabled = false;
            }          
        });

        const PS_CREAZIONE_FATTURA = document.getElementById('PS_CREAZIONE_FATTURA');
        const PS_ABILITA_FATTURA_ZERO = document.getElementById('PS_ABILITA_FATTURA_ZERO');
        const PS_DISABILITA_RICEVUTE = document.getElementById('PS_DISABILITA_RICEVUTE');
        const PS_EMAIL_FATTURA = document.getElementById('PS_EMAIL_FATTURA');
        const PS_STATO_PAGATO = document.getElementById('PS_STATO_PAGATO');

        const PS_INV_OBJECT = document.getElementById('PS_INV_OBJECT');       
        const PS_CONF_NATURA_IVA = document.getElementById('PS_CONF_NATURA_IVA');
        const PS_CONF_NATURA_SPEDIZIONE = document.getElementById('PS_CONF_NATURA_SPEDIZIONE');
        const PS_F24_MODELLO_FATTURA = document.getElementById('PS_F24_MODELLO_FATTURA_');
        
        const PS_F24_MODELLO_FATTURA_DEST = document.getElementById('PS_F24_MODELLO_FATTURA_DEST');
        const PS_F24_PDC = document.getElementById('PS_F24_PDC');
        
        const PS_F24_SEZIONALE_RICEVUTA = document.getElementById('PS_F24_SEZIONALE_RICEVUTA');
        const PS_F24_SEZIONALE_FATTURA = document.getElementById('PS_F24_SEZIONALE_FATTURA');
        const PS_F24_BOLLO_VIRTUALE = document.getElementById('PS_F24_BOLLO_VIRTUALE_FE');

        const PS_F24_DEBUG_MODE = document.getElementById('PS_F24_DEBUG_MODE');

        
        // Listener per creazione fatture
        PS_CREAZIONE_FATTURA.addEventListener('change', function (e){
            let valueSelected = PS_CREAZIONE_FATTURA.options[PS_CREAZIONE_FATTURA.selectedIndex].text;
            //console.log("SELECTED OPTION FATTURA = "+ valueSelected);
            
            switch (valueSelected) {
                case 'Disabilitato':
                    toggle(PS_DISABILITA_RICEVUTE, 'disabled');
                    toggle(PS_ABILITA_FATTURA_ZERO, 'disabled');
                    toggle(PS_EMAIL_FATTURA, 'disabled');
                    toggle(PS_STATO_PAGATO, 'disabled');
                    toggle(PS_F24_MODELLO_FATTURA, 'disabled');
                    toggle(PS_F24_MODELLO_FATTURA_DEST, 'disabled');
                    toggle(PS_F24_PDC, 'disabled');
                    toggle(PS_F24_SEZIONALE_RICEVUTA, 'disabled');
                    toggle(PS_F24_SEZIONALE_FATTURA, 'disabled');
                    toggle(PS_INV_OBJECT, 'disabled');
                    toggle(PS_CONF_NATURA_IVA, 'readonly');
                    toggle(PS_CONF_NATURA_SPEDIZIONE, 'readonly');
                    toggle(PS_F24_BOLLO_VIRTUALE, 'readonly');
                    break;
                case 'Fattura NON elettronica':
                case 'Ricevuta NON fiscale':
                    PS_SALVA_CLIENTE.checked = true;
                    PS_SALVA_CLIENTE.disabled = true;
                    toggle(PS_ABILITA_FATTURA_ZERO, 'enabled');
                    toggle(PS_F24_PDC, 'enabled');
                    toggle(PS_DISABILITA_RICEVUTE, 'enabled');
                    toggle(PS_EMAIL_FATTURA, 'enabled');
                    toggle(PS_INV_OBJECT, 'enabled');
                    toggle(PS_STATO_PAGATO, 'enabled');
                    toggle(PS_CONF_NATURA_IVA, 'readonly');
                    toggle(PS_CONF_NATURA_SPEDIZIONE, 'readonly');
                    toggle(PS_F24_BOLLO_VIRTUALE, 'readonly');
                    toggle(PS_F24_MODELLO_FATTURA, 'enabled');
                    toggle(PS_F24_MODELLO_FATTURA_DEST, 'enabled');
                    toggle(PS_F24_SEZIONALE_RICEVUTA, 'enabled');
                    toggle(PS_F24_SEZIONALE_FATTURA, 'enabled');
                    break;
                case 'Fattura elettronica':
                    toggle(PS_ABILITA_FATTURA_ZERO, 'enabled');
                    toggle(PS_F24_PDC, 'enabled');
                    toggle(PS_DISABILITA_RICEVUTE, 'enabled');
                    toggle(PS_EMAIL_FATTURA, 'enabled');
                    toggle(PS_INV_OBJECT, 'enabled');
                    toggle(PS_STATO_PAGATO, 'enabled');
                    toggle(PS_CONF_NATURA_IVA, 'enabled');
                    toggle(PS_CONF_NATURA_SPEDIZIONE, 'enabled');
                    toggle(PS_F24_BOLLO_VIRTUALE, 'enabled');
                    toggle(PS_F24_SEZIONALE_RICEVUTA, 'enabled');
                    toggle(PS_F24_SEZIONALE_FATTURA, 'enabled');
                    toggle(PS_F24_MODELLO_FATTURA, 'enabled');
                    toggle(PS_F24_MODELLO_FATTURA_DEST, 'enabled');
                    break;
                default:
                    toggle(PS_ABILITA_FATTURA_ZERO, 'enabled');
                    toggle(PS_INV_OBJECT, 'enabled');
                    toggle(PS_STATO_PAGATO, 'enabled');
                    toggle(PS_CONF_NATURA_IVA, 'readonly');
                    toggle(PS_CONF_NATURA_SPEDIZIONE, 'readonly');
                    toggle(PS_F24_SEZIONALE_RICEVUTA, 'enabled');
                    toggle(PS_F24_SEZIONALE_FATTURA, 'enabled');
                    toggle(PS_F24_BOLLO_VIRTUALE, 'readonly');
                    break;                
            }
        });

        PS_F24_DEBUG_MODE.addEventListener('change', function (e){
            let checked =  e.target.checked;
            if (!checked) {
                document.getElementById('button-wrapper').setAttribute('hidden', 'true');
            } else {
                document.getElementById('button-wrapper').removeAttribute('hidden');
            }
        })
});