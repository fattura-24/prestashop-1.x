/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License version 3.0
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License version 3.0
 */ 

/**
 * Funzioni javascript legate al pannello di amministrazione.
 * Logica mostra/nascondi sezioni delle impostazioni
 */
window.addEventListener('load', () => {
    let wrapper = document.getElementById('fattura24_wrapper');
    let settings = document.getElementById('fattura24_settings');
    let support = document.getElementById('fattura24_support');
    let video = document.getElementById('fattura24_video');
    let show_support = document.getElementById('show_support');
    let show_video = document.getElementById('show_video');
    
    settings.classList.add('active'); // default active section

    settings.addEventListener('click', () => {
        wrapper.style.display = 'inline-block';
        settings.classList.add('active');
        support.classList.remove('active');
        video.classList.remove('active');
        show_support.style.display = 'none';
        show_video.style.display = 'none';
    });

    support.addEventListener('click', (e) => {
        wrapper.style.display = 'none';
        settings.classList.remove('active');
        support.classList.add('active');
        show_support.style.display = 'inline-block';
        show_video.style.display = 'none';
        video.classList.remove('active');
    });

    video.addEventListener('click', (e) => {
        wrapper.style.display = 'none';
        settings.classList.remove('active');
        support.classList.remove('active');
        show_support.style.display = 'none';
        show_video.style.display = 'inline-block';
        video.classList.add('active');
    });
})
