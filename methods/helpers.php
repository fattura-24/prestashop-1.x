<?php
/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License version 3.0
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License version 3.0
 */

 /**
  * Funzioni di supporto utilizzate in fattura24.php
  */

// descrizione di default aliquota IVA
  function getDefaultVatDescription($rate)
{
    return 'IVA ' . $rate . '%';
}

/**
 * cfr.: https://stackoverflow.com/questions/34058962/multidimensional-array-to-string-conversion
 * @param array $param
 * @return string
 */
function array2string($param)
{
    $result = '';
    array_walk_recursive(
        $param,
        function ($v, $k) use (&$result) {
            $result .= '[' . $k . '] => ' . $v . PHP_EOL;
        }
    );

    return $result;
}

/**
 * Verifica disponibilità aggiornamento
 */
function isUpdateAvailable($current_version)
{
    $url = 'https://www.fattura24.com/download/prestashop/';
    $dom = new DOMDocument();
    $dom->loadHTMLFile($url);
    $links = $dom->getElementsByTagName('a');
    $fileNames = [];

    foreach ($links as $link) {
        if ($link->nodeValue == '../') {
            continue;
        }
        $fileNames[] = $link->nodeValue;
    }

    $latest_filename = array_reduce($fileNames, function ($latest, $current) use ($url) {
        preg_match('/v(\d+\.\d+\.\d+)/', $current, $current_matches);
        preg_match('/v(\d+\.\d+\.\d+)/', $latest, $latest_matches);

        $current_version = isset($current_matches[1]) ? $current_matches[1] : '0.0.0';
        $latest_version = isset($latest_matches[1]) ? $latest_matches[1] : '0.0.0';

        return version_compare($current_version, $latest_version, '>') ? $url . $current : $latest;
    }, '');

    preg_match('/v(\d+\.\d+\.\d+)/', $latest_filename, $matches);
    $latest_version = $matches[1];

    $is_update_available = version_compare($latest_version, $current_version, '>');
    $updates = [
        'update_available' => $is_update_available ? 'true' : 'false',
        'filename' => $latest_filename,
    ];

    return $updates;
}
