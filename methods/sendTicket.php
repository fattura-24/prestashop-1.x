<?php
/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License version 3.0
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License version 3.0
 */

/**
 * Funzioni per inviare un ticket a Desktale dal modulo:
 * ottieni informazioni sull'account
 */
function getF24AccountInfo($response)
{
    $apiKey = Configuration::get('PS_FATTURA24_API');
    $accountId = '';
    $defaultEmailOwner = 'nessuna_email@trovata.it';


    if (empty($apiKey)) {
        $accountId = 'chiave_api_non_inserita';
    } elseif (isset($response['error'])) {
        $accountId = (int) $response['error']['code'] !== 200 ?
            'server_api_non_raggiungibile' : 'errore_generico';
    } else {
        $result = simplexml_load_string($response['output']);
        if (is_object($result)) {
            $accountId = (int) $result->subscription->accountId;
            $emailOwner = $result->subscription->emailOwner ? $result->subscription->emailOwner : $defaultEmailOwner ;
        }
    }

    return [
        'accountId' => $accountId,
        'emailOwner' => $emailOwner,
    ];
}

// chiamata API Desktale
function sendTicket($data)
{
    $endpoint = 'https://www.desktale.com/api/server/tickets/sendTicket?api_key=';
    // $endpoint = 'http://192.168.178.30:4000/server/tickets/sendTicket?api_key=';
    $api_key = 'fis4xr6FH4Gfhk7rTDFsd2wq3ZdfD3cgjB7ds3sDMnp5';
    $url = $endpoint . $api_key;
    $headers = ['Content-Type:multipart/form-data'];
    $file = getTicketFileName($data);
    $fileName = basename($file);

    /*
     * cfr: https://www.php.net/manual/en/class.curlfile.php
     * in questo modo ottengo il solo nome del file scartando tutto il
     * resto del percorso e lo passo come terzo parametro a CURLFile
     */

    // https://stackoverflow.com/questions/2628798/print-array-to-a-file
    file_put_contents($file, array2string($data));
    $identify = [
        'widget_id' => 'P-' . $data['account_id'],
        'id_account' => 'P-' . $data['account_id'],
        'name' => $data['name'],
        'email' => $data['email'],
        'emailCc' => $data['email_owner'],
    ];

    $formData = [
        'identify' => json_encode($identify),
        'widget_id' => 'P-' . $data['account_id'],
        'name' => $data['name'],
        'email' => $data['email'],
        'emailCc' => $data['email_owner'],
        'subject' => $data['subject'],
        'text' => htmlentities($data['text_message'], ENT_HTML401, ''),
        'file' => new CURLFile($file, 'text/plain', $fileName),
    ];

    $ch = curl_init();
    $data['useragent'] = 'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:17.0) Gecko/20100101 Firefox/17.0';
    $options = [
        CURLOPT_VERBOSE => true,
        CURLOPT_URL => $url,
        CURLOPT_USERAGENT => $data['useragent'],
        CURLOPT_HTTPHEADER => $headers,
        CURLOPT_POSTFIELDS => $formData,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HEADER => 0,
        CURLOPT_TIMEOUT => 60,
    ];

    curl_setopt_array($ch, $options);
    $output = curl_exec($ch);
    $result = $output ? [
        'response' => 'ok',
        'message' => $output,
    ] :
        [
            'response' => 'ko',
            'message' => json_encode([curl_getinfo($ch), curl_error($ch), curl_errno($ch)]),
        ];
    curl_close($ch);
    unlink($file);

    return $result;
}

// costruisco il nome del file di testo allegato al ticket
function getTicketFileName($data)
{
    $path = _PS_MODULE_DIR_ . 'fattura24/log/';
    $today = date('d_m_Y');
    $source = Tools::strtolower($data['source']);
    $accountId = $data['account_id'];
    $fileName = $path . 'att_' . $source . '_' . $accountId . '_' . $today . '.txt';

    return $fileName;
}
