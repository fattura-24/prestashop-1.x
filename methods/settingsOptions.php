<?php
/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License version 3.0
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License version 3.0
 */

/**
 * Funzioni di servizio legate alle impostazioni del modulo Fattura24
 */
function f24Options()
{
    $f24_fields = [
        'PS_FATTURA24_API' => '',
        'PS_SALVA_CLIENTE' => '1',
        'PS_CREA_ORDINE' => '1',
        'PS_ABILITA_ORDINE_ZERO' => '0',
        'PS_PDF_ORDINE' => '0',
        'PS_MAIL_ORDINE' => '0',
        'PS_NUMERO_ORDINE' => '0',
        'PS_F24_MODELLO_ORDINE' => 'Predefinito',
        'PS_F24_MODELLO_ORDINE_DEST' => 'Predefinito',
        'PS_CREAZIONE_FATTURA' => 'Disattivata',
        'PS_ABILITA_FATTURA_ZERO' => '0',
        'PS_PDF_FATTURA' => '1',
        'PS_CONF_NATURA_IVA' => '',
        'PS_CONF_NATURA_SPEDIZIONE' => 'Nessuna',
        'PS_INV_OBJECT' => 'Ordine (N) Shop on-line',
        'PS_EMAIL_FATTURA' => '0',
        'PS_STATO_PAGATO' => '0',
        'PS_DISABILITA_RICEVUTE' => '0',
        'PS_F24_MODELLO_FATTURA' => 'Predefinito',
        'PS_F24_MODELLO_FATTURA_DEST' => 'Predefinito',
        'PS_F24_PDC' => 'Nessun Pdc',
        'PS_F24_SEZIONALE_RICEVUTA' => 'Predefinito',
        'PS_F24_SEZIONALE_FATTURA' => 'Predefinito',
        'PS_F24_BOLLO_VIRTUALE_FE' => 'Mai',
        'PS_F24_TEST_KEY' => '0 | 0 | 1970-01-01 00:00:00',
        'PS_F24_DEBUG_MODE' => '0',
    ];

    return $f24_fields;
}

function f24Value($param)
{
    return [$param => Tools::getValue($param), Configuration::get($param)];
}

function getConfigFieldsValues()
{
    $optionKeys = [
        'PS_FATTURA24_API',
        'PS_SALVA_CLIENTE',
        'PS_CREA_ORDINE',
        'PS_ABILITA_ORDINE_ZERO',
        'PS_PDF_ORDINE',
        'PS_MAIL_ORDINE',
        'PS_NUMERO_ORDINE',
        'PS_F24_MODELLO_ORDINE',
        'PS_F24_MODELLO_ORDINE_DEST',
        'PS_CREAZIONE_FATTURA',
        'PS_ABILITA_FATTURA_ZERO',
        'PS_PDF_FATTURA',
        'PS_CONF_NATURA_IVA',
        'PS_CONF_NATURA_SPEDIZIONE',
        'PS_INV_OBJECT',
        'PS_EMAIL_FATTURA',
        'PS_STATO_PAGATO',
        'PS_DISABILITA_RICEVUTE',
        'PS_F24_MODELLO_FATTURA',
        'PS_F24_MODELLO_FATTURA_DEST',
        'PS_F24_PDC',
        'PS_F24_SEZIONALE_FATTURA',
        'PS_F24_SEZIONALE_RICEVUTA',
        'PS_F24_BOLLO_VIRTUALE_FE',
        'PS_F24_DEBUG_MODE',
        'PS_F24_TEST_KEY',
    ];
    $result = [];
    foreach ($optionKeys as $key) {
        $result[$key] = Configuration::get($key);
    }

    return $result;
}

// dropdown lista codici natura
function getListaNatureNew()
{
    $tipiAliquoteIva = [
        'N0' => 'Nessuna',
        'N1' => ['N1' => 'N1 - Escluse ex art. 15'],
        'N2' => ['N2.1' => 'N2.1 - Non soggette ad IVA ai sensi degli artt. da 7 a 7-septies del DPR 633/72',
                 'N2.2' => 'N2.2 - Altri casi', ],
        'N3' => ['N3.1' => 'N3.1 - Esportazioni',
                 'N3.2' => 'N3.2 - Cessioni intracomunitarie',
                 'N3.3' => 'N3.3 - Cessioni verso San Marino',
                 'N3.4' => 'N3.4 - Operazioni assimilate alle cessioni all\'esportazione',
                 'N3.5' => 'N3.5 - A seguito di dichiarazioni d\'intento',
                 'N3.6' => 'N3.6 - Altre operazioni che non concorrono alla formazione del plafond', ],
        'N4' => ['N4' => 'N4 - Esenti'],
        'N5' => ['N5' => 'N5 - Regime del margine / IVA non esposta in fattura'],
        'N6' => ['N6.1' => 'N6.1 - Cessione di rottami e altri materiali di recupero',
                 'N6.2' => 'N6.2 - Cessione di oro e argento puro',
                 'N6.3' => 'N6.3 - Subappalto nel settore edile',
                 'N6.4' => 'N6.4 - Cessione di fabbricati',
                 'N6.5' => 'N6.5 - Cessione di telefoni cellulari',
                 'N6.6' => 'N6.6 - Cessione di prodotti elettronici',
                 'N6.7' => 'N6.7 - Prestazioni comparto edile e settori connessi',
                 'N6.8' => 'N6.8 - Operazioni settore energetico',
                 'N6.9' => 'N6.9 - Altri casi', ],
        'N7' => ['N7' => 'N7 - IVA assolta in altro stato UE'],
        ];

    return $tipiAliquoteIva;
}

// aggiorna valori configurazione
function updateConfig()
{
    Configuration::updateValue('PS_FATTURA24_API', $_REQUEST['PS_FATTURA24_API']);

    if (isset($_REQUEST['PS_CONF_NATURA_IVA'])) {
        Configuration::updateValue('PS_CONF_NATURA_IVA', $_REQUEST['PS_CONF_NATURA_IVA']);
    }
    if (isset($_REQUEST['PS_CONF_NATURA_SPEDIZIONE'])) {
        Configuration::updateValue('PS_CONF_NATURA_SPEDIZIONE', $_REQUEST['PS_CONF_NATURA_SPEDIZIONE']);
    }
    if (isset($_REQUEST['PS_INV_OBJECT'])) {
        Configuration::updateValue('PS_INV_OBJECT', $_REQUEST['PS_INV_OBJECT']); // oggetto del documento
    }
    // se le checkbox sono settate imposto il valore della relativa configurazione a 1, altrimenti a 0
    if (isset($_REQUEST['PS_SALVA_CLIENTE'])) {
        Configuration::updateValue('PS_SALVA_CLIENTE', $_REQUEST['PS_SALVA_CLIENTE']);
    } else {
        Configuration::updateValue('PS_SALVA_CLIENTE', '0');
    }

    if (isset($_REQUEST['PS_CREA_ORDINE'])) {
        Configuration::updateValue('PS_CREA_ORDINE', $_REQUEST['PS_CREA_ORDINE']);
    } else {
        Configuration::updateValue('PS_CREA_ORDINE', '0');
    }

    if (isset($_REQUEST['PS_ABILITA_ORDINE_ZERO'])) {
        Configuration::updateValue('PS_ABILITA_ORDINE_ZERO', $_REQUEST['PS_ABILITA_ORDINE_ZERO']);
    } else {
        Configuration::updateValue('PS_ABILITA_ORDINE_ZERO', '0');
    }

    if (isset($_REQUEST['PS_NUMERO_ORDINE'])) {
        Configuration::updateValue('PS_NUMERO_ORDINE', $_REQUEST['PS_NUMERO_ORDINE']);
    } else {
        Configuration::updateValue('PS_NUMERO_ORDINE', '0');
    }

    if (isset($_REQUEST['PS_PDF_ORDINE'])) {
        Configuration::updateValue('PS_PDF_ORDINE', $_REQUEST['PS_PDF_ORDINE']);
    } else {
        Configuration::updateValue('PS_PDF_ORDINE', '0');
    }

    if (isset($_REQUEST['PS_MAIL_ORDINE'])) {
        Configuration::updateValue('PS_MAIL_ORDINE', $_REQUEST['PS_MAIL_ORDINE']);
    } else {
        Configuration::updateValue('PS_MAIL_ORDINE', '0');
    }

    if (isset($_REQUEST['PS_F24_MODELLO_ORDINE'])) {
        Configuration::updateValue('PS_F24_MODELLO_ORDINE', $_REQUEST['PS_F24_MODELLO_ORDINE']);
    } else {
        Configuration::updateValue('PS_F24_MODELLO_ORDINE', 'Default');
    }

    if (isset($_REQUEST['PS_F24_MODELLO_ORDINE_DEST'])) {
        Configuration::updateValue('PS_F24_MODELLO_ORDINE_DEST', $_REQUEST['PS_F24_MODELLO_ORDINE_DEST']);
    } else {
        Configuration::updateValue('PS_F24_MODELLO_ORDINE_DEST', 'Default');
    }

    if (isset($_REQUEST['PS_CREAZIONE_FATTURA'])) {
        Configuration::updateValue('PS_CREAZIONE_FATTURA', $_REQUEST['PS_CREAZIONE_FATTURA']);
    } else {
        Configuration::updateValue('PS_CREAZIONE_FATTURA', '0');
    }

    if (isset($_REQUEST['PS_ABILITA_FATTURA_ZERO'])) {
        Configuration::updateValue('PS_ABILITA_FATTURA_ZERO', $_REQUEST['PS_ABILITA_FATTURA_ZERO']);
    } else {
        Configuration::updateValue('PS_ABILITA_FATTURA_ZERO', '0');
    }

    if (isset($_REQUEST['PS_PDF_FATTURA'])) {
        Configuration::updateValue('PS_PDF_FATTURA', $_REQUEST['PS_PDF_FATTURA']);
    } else {
        Configuration::updateValue('PS_PDF_FATTURA', '0');
    }

    if (isset($_REQUEST['PS_EMAIL_FATTURA'])) {
        Configuration::updateValue('PS_EMAIL_FATTURA', $_REQUEST['PS_EMAIL_FATTURA']);
    } else {
        Configuration::updateValue('PS_EMAIL_FATTURA', '0');
    }

    if (isset($_REQUEST['PS_STATO_PAGATO'])) {
        Configuration::updateValue('PS_STATO_PAGATO', $_REQUEST['PS_STATO_PAGATO']);
    } else {
        Configuration::updateValue('PS_STATO_PAGATO', '0');
    }

    if (isset($_REQUEST['PS_DISABILITA_RICEVUTE'])) {
        Configuration::updateValue('PS_DISABILITA_RICEVUTE', $_REQUEST['PS_DISABILITA_RICEVUTE']);
    } else {
        Configuration::updateValue('PS_DISABILITA_RICEVUTE', '0');
    }

    if (isset($_REQUEST['PS_F24_MODELLO_FATTURA'])) {
        Configuration::updateValue('PS_F24_MODELLO_FATTURA', $_REQUEST['PS_F24_MODELLO_FATTURA']);
    } else {
        Configuration::updateValue('PS_F24_MODELLO_FATTURA', 'Default');
    }

    if (isset($_REQUEST['PS_F24_MODELLO_FATTURA_DEST'])) {
        Configuration::updateValue('PS_F24_MODELLO_FATTURA_DEST', $_REQUEST['PS_F24_MODELLO_FATTURA_DEST']);
    } else {
        Configuration::updateValue('PS_F24_MODELLO_FATTURA_DEST', 'Default');
    }

    if (isset($_REQUEST['PS_F24_PDC'])) {
        Configuration::updateValue('PS_F24_PDC', $_REQUEST['PS_F24_PDC']);
    } else {
        Configuration::updateValue('PS_F24_PDC', 'Default');
    }

    if (isset($_REQUEST['PS_F24_SEZIONALE_RICEVUTA'])) {
        Configuration::updateValue('PS_F24_SEZIONALE_RICEVUTA', $_REQUEST['PS_F24_SEZIONALE_RICEVUTA']);
    } else {
        Configuration::updateValue('PS_F24_SEZIONALE_RICEVUTA', 'Default');
    }

    if (isset($_REQUEST['PS_F24_SEZIONALE_FATTURA'])) {
        Configuration::updateValue('PS_F24_SEZIONALE_FATTURA', $_REQUEST['PS_F24_SEZIONALE_FATTURA']);
    } else {
        Configuration::updateValue('PS_F24_SEZIONALE_FATTURA', 'Default');
    }
    if (isset($_REQUEST['PS_F24_BOLLO_VIRTUALE_FE'])) {
        Configuration::updateValue('PS_F24_BOLLO_VIRTUALE_FE', $_REQUEST['PS_F24_BOLLO_VIRTUALE_FE']);
    }

    if (isset($_REQUEST['PS_F24_DEBUG_MODE'])) {
        Configuration::updateValue('PS_F24_DEBUG_MODE', $_REQUEST['PS_F24_DEBUG_MODE']);
    } else {
        Configuration::updateValue('PS_F24_DEBUG_MODE', '0');
    }
}

// videolezioni
function getLessons()
{
    $path = __PS_BASE_URI__ . 'modules/fattura24/views/img/lezioni/';
    $lessons = [];
    $filenum = 5;

    for ($i = 1; $i <= $filenum; ++$i) {
        $lessons[$i]['img'] = $path . 'lezione' . $i . '-prestashop.jpg';
        switch ($i) {
            case 1:
                $lessons[$i]['link'] = 'https://youtu.be/exrmoG-n-OA';
                $lessons[$i]['description'] =
                    'Vediamo come installare e attivare il modulo al marketplace di Prestashop (addons). Prima panoramica della configurazione.';
                break;
            case 2:
                $lessons[$i]['link'] = 'https://youtu.be/huQMhQu5gfw';
                $lessons[$i]['description'] =
                    'Vediamo come configurare le aliquote in Fattura24 su Prestashop con un focus sull\'aliquota 0% e come configurare la Natura nel modulo Fattura24.';
                break;
            case 3:
                $lessons[$i]['link'] = 'https://youtu.be/jUHuyx-37BA';
                $lessons[$i]['description'] =
                    'Vediamo come creare e controllare un ordine per il checkout e il documento per poi convertirlo in ricevuta e in fattura elettronica.';
                break;
            case 4:
                $lessons[$i]['link'] = 'https://youtu.be/cnlaQGEEdm0';
                $lessons[$i]['description'] =
                    'Vediamo un approfondimento sull\'attributo \'pagato\' dello stato dell\'ordine e sulla visualizzazione dei prezzi (IVA inclusa vs IVA esclusa) nel negozio.';
                break;
            case 5:
                $lessons[$i]['link'] = 'https://youtu.be/XsiRj7oLiI8';
                $lessons[$i]['description'] =
                    'Vediamo come configurare il negozio per vendere lo stesso prodotto in diversi paesi dell\'Europa, e come associare a quel prodotto l\'aliquota in base al paese di fatturazione del cliente.';
                break;
            default:
                break;
        }
    }

    return $lessons;
}

// risultato della chiamata API TestKey
function getTestRes($result)
{
    $values = explode(' | ', $result);
    $keys = ['exitCode', 'httpCode', 'lastUpdate'];
    if (count($values) == 4) {
        $keys[] = 'expireDate';
    }

    $resArray = [];
    foreach ($values as $key => $val) {
        $resArray[$keys[$key]] = $val;
    }

    return $resArray;
}
