#!/bin/bash

# Per evitare di inserire pi� volte la pw di accesso al server � necessario
# creare una sshpass con i seguenti comandi:
#
# shell> ssh-keygen -b 2048 -t rsa
# shell> ssh-copy-id root@80.211.30.113
#
# Il primo comando non va eseguito nel caso sia stato precedentemente utilizzato 
# sulla macchina di sviluppo per accedere ad altri server.

SERVER_IP="80.211.30.113"
LOCAL_FOLDER="/Applications/MAMP/htdocs/ps17/modules/fattura24"
TARGET_FOLDER="/var/www/html/prestashop17/modules/fattura24"

echo "**** Prestashop Collaudo - inizio aggiornamento ****"


CMD1="rm -R -f /var/www/html/prestashop17/modules/fattura24;"
CMD2="mkdir /var/www/html/prestashop17/modules/fattura24;" 
CMD3="chmod 777 /var/www/html/prestashop17/modules/fattura24"
											
ssh root@${SERVER_IP} ${CMD1}${CMD2}

rsync -rva --delete ${LOCAL_FOLDER}/* root@${SERVER_IP}:${TARGET_FOLDER}

ssh root@${SERVER_IP} "chown -R apache:apache ${TARGET_FOLDER}"

echo "**** Prestashop Collaudo - fine aggiornamento ****"


