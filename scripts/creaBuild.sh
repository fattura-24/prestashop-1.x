#!/bin/bash

#
# Con questo script intendo automatizzare la creazione del file compresso
# contenente il modulo fattura24 per Prestashop
#

local_folder="/Users/davide/Desktop/Ecommerce/prestashop-addons"
prefix="fattura24_pre_v"
version=$(xmllint -xpath "module/version/text()" /Users/davide/Desktop/Ecommerce/prestashop-addons/fattura24/config.xml --nocdata)

echo "**** - creazione file zip pulito ****"
echo
cd ${local_folder}
echo "*** Preparo la creazione del file per la versione ${version} ****"
echo

zip -r ${prefix}${version}.zip -x"*.DS_Store" -x="__MACOSX" fattura24

echo "**** - creazione file zip terminata ****"
echo
