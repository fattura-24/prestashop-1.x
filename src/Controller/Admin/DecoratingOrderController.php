<?php
/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License version 3.0
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License version 3.0
 */

 /**
  * Versione personalizzata del controller degli ordini Symfony
  */

declare(strict_types=1);

namespace Fattura24\Controller\Admin;

use Configuration;
use Db;
use Fattura24;
// use Order;
use PrestaShop\PrestaShop\Core\Search\Filters\OrderFilters;
use PrestaShopBundle\Controller\Admin\FrameworkBundleAdminController;
use PrestaShopBundle\Controller\Admin\Sell\Order\OrderController;
use PrestaShopBundle\Security\Annotation\AdminSecurity;
use PrestaShopBundle\Security\Annotation\DemoRestricted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DecoratingOrderController extends FrameworkBundleAdminController
{
    /**
     * @var OrderController
     */
    private $orderController;
    private $fattura24;

    public function __construct(OrderController $orderController)
    {
        $this->orderController = $orderController;
        $this->fattura24 = new \Fattura24();
        parent::__construct();
    }

    /**
     * Shows list of orders
     *
     * @AdminSecurity("is_granted('read', request.get('_legacy_controller'))")
     *
     * @param Request $request
     * @param OrderFilters $filters
     */
    public function indexAction(Request $request, OrderFilters $filters)
    {
        return $this->orderController->indexAction($request, $filters);
    }

    /**
     * Places an order from BO
     *
     * @AdminSecurity("is_granted('create', request.get('_legacy_controller'))")
     *
     * @param Request $request
     */
    public function placeAction(Request $request)
    {
        return $this->orderController->placeAction($request);
    }

    /**
     * Renders create order page.
     * Whole page dynamics are on javascript side.
     * To load specific cart pass cartId to url query params (handled by javascript)
     *
     * @AdminSecurity("is_granted('create', request.get('_legacy_controller'))")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function createAction(Request $request)
    {
        return $this->orderController->createAction($request);
    }

    /**
     * @AdminSecurity("is_granted('read', request.get('_legacy_controller'))", redirectRoute="admin_orders_index")
     *
     * @param Request $request
     */
    public function searchAction(Request $request)
    {
        return $this->orderController->searchAction($request);
    }

    /**
     * Generates invoice PDF for given order
     *
     * @AdminSecurity("is_granted('read', request.get('_legacy_controller'))", redirectRoute="admin_orders_index")
     *
     * @param int $orderId
     *
     * metodo modificato: se ho il docId in Fattura24 e il file non esiste
     * lo scarico e poi lo mostro a video, se già esiste lo mostro; in tutti gli altri casi
     * genero il PDF di Prestashop (funzione nativa). Funziona sia nell'elenco degli ordini sia nel
     * dettaglio dell'ordine.
     * In Prestashop 1.6 vengono utilizzati i vecchi metodi con l'override
     * Davide Iandoli 23.12.2022
     */
    public function generateInvoicePdfAction($orderId)
    {
        $downloadPdfInvoice = '1' === \Configuration::get('PS_PDF_FATTURA');

        if ($downloadPdfInvoice) {
            $this->displayF24Pdf($orderId);
        } else {
            $this->orderController->generateInvoicePdfAction($orderId);
        }
    }

    /**
     * Metodo aggiunto da Fattura24. Nel caso peggiore visualizza il PDF generato
     * da PrestaShop. Gli altri metodi sono quelli originari del controller Symfony
     */
    private function displayF24Pdf($orderId)
    {
        $order = new \Order((int) $orderId);

        $fileName = $this->fattura24->
            constructFileName($order->id, $order->reference);
        $filePath = _PS_DOWNLOAD_DIR_ . 'Fattura24/' . $fileName;
        if (!file_exists($filePath) || filesize($filePath) < 200) {
            $query = 'SELECT docIdFattura24 FROM ' . _DB_PREFIX_ . 'orders WHERE id_order=\'' . (int) $orderId . '\';';
            $docIdFattura24 = \Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($query)['docIdFattura24'];
            if (!empty($docIdFattura24)) {
                $this->fattura24->downloadDocument($docIdFattura24, $orderId, true);
            }
        }
        if (file_exists($filePath) && filesize($filePath) > 200) {
            header('Content-type: application/pdf');
            header('Content-Disposition: attachment; filename="' . $fileName . '"');
            header('Content-Transfer-Encoding: binary');
            readfile($filePath);
        } else {
            $this->orderController->generateInvoicePdfAction($orderId);
        }
    }

    /**
     * Generates delivery slip PDF for given order
     *
     * @AdminSecurity("is_granted('read', request.get('_legacy_controller'))", redirectRoute="admin_orders_index")
     *
     * @param int $orderId
     */
    public function generateDeliverySlipPdfAction($orderId)
    {
        $this->orderController->generateDeliverySlipPdfAction($orderId);
    }

    /**
     * @param Request $request
     *
     * @AdminSecurity("is_granted('update', request.get('_legacy_controller'))", redirectRoute="admin_orders_index")
     */
    public function changeOrdersStatusAction(Request $request)
    {
        return $this->orderController->changeOrdersStatusAction($request);
    }

    /**
     * @AdminSecurity("is_granted('read', request.get('_legacy_controller'))", redirectRoute="admin_orders_index")
     *
     * @param OrderFilters $filters
     */
    public function exportAction(OrderFilters $filters)
    {
        return $this->orderController->exportAction($filters);
    }

    /**
     * @AdminSecurity("is_granted('read', request.get('_legacy_controller'))")
     *
     * @param int $orderId
     * @param Request $request
     */
    public function viewAction(int $orderId, Request $request)
    {
        return $this->orderController->viewAction($orderId, $request);
    }

    /**
     * @AdminSecurity(
     *     "is_granted(['update', 'delete'], request.get('_legacy_controller'))",
     *     redirectRoute="admin_orders_view",
     *     redirectQueryParamsToKeep={"orderId"},
     *     message="You do not have permission to edit this."
     * )
     *
     * @param int $orderId
     * @param Request $request
     */
    public function partialRefundAction(int $orderId, Request $request)
    {
        return $this->orderController->partialRefundAction($orderId, $request);
    }

    /**
     * @AdminSecurity("is_granted(['update', 'delete'], request.get('_legacy_controller'))")
     *
     * @param int $orderId
     * @param Request $request
     */
    public function standardRefundAction(int $orderId, Request $request)
    {
        return $this->orderController->standardRefundAction($orderId, $request);
    }

    /**
     * @AdminSecurity("is_granted(['update', 'delete'], request.get('_legacy_controller'))")
     *
     * @param int $orderId
     * @param Request $request
     */
    public function returnProductAction(int $orderId, Request $request)
    {
        return $this->orderController->returnProductAction($orderId, $request);
    }

    /**
     * @AdminSecurity("is_granted(['create'], request.get('_legacy_controller'))", redirectRoute="admin_orders_index")
     *
     * @param int $orderId
     * @param Request $request
     */
    public function addProductAction(int $orderId, Request $request)
    {
        return $this->orderController->addProductAction($orderId, $request);
    }

    /**
     * @AdminSecurity("is_granted(['read'], request.get('_legacy_controller'))", redirectRoute="admin_orders_index")
     *
     * @param int $orderId
     */
    public function getProductPricesAction(int $orderId)
    {
        return $this->orderController->getProductPricesAction($orderId);
    }

    /**
     * @AdminSecurity("is_granted(['read'], request.get('_legacy_controller'))", redirectRoute="admin_orders_index")
     *
     * @param int $orderId
     */
    public function getInvoicesAction(int $orderId)
    {
        return $this->orderController->getInvoicesAction($orderId);
    }

    /**
     * @AdminSecurity("is_granted(['read'], request.get('_legacy_controller'))", redirectRoute="admin_orders_index")
     *
     * @param int $orderId
     */
    public function getDocumentsAction(int $orderId)
    {
        return $this->orderController->getDocumentsAction($orderId);
    }

    /**
     * @AdminSecurity("is_granted(['read'], request.get('_legacy_controller'))", redirectRoute="admin_orders_index")
     *
     * @param int $orderId
     */
    public function getShippingAction(int $orderId)
    {
        return $this->orderController->getShippingAction($orderId);
    }

    /**
     * @AdminSecurity(
     *     "is_granted('update', request.get('_legacy_controller'))",
     *     redirectRoute="admin_orders_view",
     *     redirectQueryParamsToKeep={"orderId"},
     *     message="You do not have permission to edit this."
     * )
     *
     * @param int $orderId
     * @param Request $request
     */
    public function updateShippingAction(int $orderId, Request $request)
    {
        return $this->orderController->updateShippingAction($orderId, $request);
    }

    /**
     * @AdminSecurity(
     *     "is_granted('update', 'AdminOrders')",
     *     redirectRoute="admin_orders_view",
     *     redirectQueryParamsToKeep={"orderId"},
     *     message="You do not have permission to edit this."
     * )
     *
     * @param int $orderId
     * @param int $orderCartRuleId
     */
    public function removeCartRuleAction(int $orderId, int $orderCartRuleId)
    {
        $this->orderController->removeCartRuleAction($orderId, $orderCartRuleId);
    }

    /**
     * @AdminSecurity(
     *     "is_granted('update', 'AdminOrders')",
     *     redirectRoute="admin_orders_view",
     *     redirectQueryParamsToKeep={"orderId"},
     *     message="You do not have permission to edit this."
     * )
     *
     * @param int $orderId
     * @param int $orderInvoiceId
     * @param Request $request
     */
    public function updateInvoiceNoteAction(int $orderId, int $orderInvoiceId, Request $request)
    {
        return $this->orderController->updateInvoiceNoteAction($orderId, $orderInvoiceId, $request);
    }

    /**
     * @AdminSecurity("is_granted('update', request.get('_legacy_controller'))", redirectRoute="admin_orders_index")
     *
     * @param int $orderId
     * @param int $orderDetailId
     * @param Request $request
     */
    public function updateProductAction(int $orderId, int $orderDetailId, Request $request)
    {
        return $this->orderController->updateProductAction($orderId, $orderDetailId, $request);
    }

    /**
     * @AdminSecurity(
     *     "is_granted('update', 'AdminOrders')",
     *     redirectRoute="admin_orders_view",
     *     redirectQueryParamsToKeep={"orderId"},
     *     message="You do not have permission to edit this."
     * )
     *
     * @param int $orderId
     * @param Request $request
     */
    public function addCartRuleAction(int $orderId, Request $request)
    {
        return $this->orderController->addCartRuleAction($orderId, $request);
    }

    /**
     * @param int $orderId
     * @param Request $request
     *
     * @AdminSecurity("is_granted('update', request.get('_legacy_controller'))", redirectRoute="admin_orders_index")
     */
    public function updateStatusAction(int $orderId, Request $request)
    {
        return $this->orderController->updateStatusAction($orderId, $request);
    }

    /**
     * Updates order status directly from list page.
     *
     * @param int $orderId
     * @param Request $request
     *
     * @AdminSecurity("is_granted('update', request.get('_legacy_controller'))", redirectRoute="admin_orders_index")
     */
    public function updateStatusFromListAction(int $orderId, Request $request)
    {
        return $this->orderController->updateStatusFromListAction($orderId, $request);
    }

    /**
     * @AdminSecurity(
     *     "is_granted('update', 'AdminOrders')",
     *     redirectRoute="admin_orders_view",
     *     redirectQueryParamsToKeep={"orderId"},
     *     message="You do not have permission to edit this."
     * )
     *
     * @param int $orderId
     * @param Request $request
     */
    public function addPaymentAction(int $orderId, Request $request)
    {
        return $this->orderController->addPaymentAction($orderId, $request);
    }

    /**
     * @AdminSecurity("is_granted('read', request.get('_legacy_controller'))")
     *
     * @param int $orderId
     */
    public function previewAction(int $orderId)
    {
        return $this->orderController->previewAction($orderId);
    }

    /**
     * Duplicates cart from specified order
     *
     * @AdminSecurity("is_granted('update', request.get('_legacy_controller')) || is_granted('create', 'AdminOrders')")
     *
     * @param int $orderId
     */
    public function duplicateOrderCartAction(int $orderId)
    {
        return $this->orderController->duplicateOrderCartAction($orderId);
    }

    /**
     * @AdminSecurity(
     *     "is_granted('update', request.get('_legacy_controller'))",
     *     redirectRoute="admin_orders_view",
     *     redirectQueryParamsToKeep={"orderId"},
     *     message="You do not have permission to edit this."
     * )
     *
     * @DemoRestricted(
     *     redirectRoute="admin_orders_view",
     *     redirectQueryParamsToKeep={"orderId"}
     * )
     *
     * @param Request $request
     * @param int $orderId
     */
    public function sendMessageAction(Request $request, int $orderId)
    {
        return $this->orderController->sendMessageAction($request, $orderId);
    }

    /**
     * @AdminSecurity(
     *     "is_granted('update', 'AdminOrders')",
     *     redirectRoute="admin_orders_view",
     *     redirectQueryParamsToKeep={"orderId"},
     *     message="You do not have permission to edit this."
     * )
     *
     * @param Request $request
     */
    public function changeCustomerAddressAction(Request $request)
    {
        return $this->orderController->changeCustomerAddressAction($request);
    }

    /**
     * @AdminSecurity(
     *     "is_granted('update', request.get('_legacy_controller'))",
     *     redirectRoute="admin_orders_view",
     *     redirectQueryParamsToKeep={"orderId"},
     *     message="You do not have permission to edit this."
     * )
     *
     * @param int $orderId
     * @param Request $request
     */
    public function changeCurrencyAction(int $orderId, Request $request)
    {
        return $this->orderController->changeCurrencyAction($orderId, $request);
    }

    /**
     * @AdminSecurity(
     *     "is_granted('update', 'AdminOrders')",
     *     redirectRoute="admin_orders_view",
     *     redirectQueryParamsToKeep={"orderId"},
     *     message="You do not have permission to edit this."
     * )
     *
     * @param int $orderId
     * @param int $orderStatusId
     * @param int $orderHistoryId
     */
    public function resendEmailAction(int $orderId, int $orderStatusId, int $orderHistoryId)
    {
        return $this->orderController->resendEmailAction($orderId, $orderStatusId, $orderHistoryId);
    }

    /**
     * @AdminSecurity("is_granted('delete', request.get('_legacy_controller'))", redirectRoute="admin_orders_index")
     *
     * @param int $orderId
     * @param int $orderDetailId
     */
    public function deleteProductAction(int $orderId, int $orderDetailId)
    {
        return $this->orderController->deleteProductAction($orderId, $orderDetailId);
    }

    /**
     * @AdminSecurity("is_granted('read', request.get('_legacy_controller'))", redirectRoute="admin_orders_index")
     *
     * @param int $orderId
     */
    public function getDiscountsAction(int $orderId)
    {
        return $this->orderController->getDiscountsAction($orderId);
    }

    /**
     * @AdminSecurity("is_granted('read', request.get('_legacy_controller'))", redirectRoute="admin_orders_index")
     *
     * @param int $orderId
     */
    public function getPricesAction(int $orderId)
    {
        return $this->orderController->getPricesAction($orderId);
    }

    /**
     * @AdminSecurity("is_granted('read', request.get('_legacy_controller'))", redirectRoute="admin_orders_index")
     *
     * @param int $orderId
     */
    public function getPaymentsAction(int $orderId)
    {
        return $this->orderController->getPaymentsAction($orderId);
    }

    /**
     * @AdminSecurity("is_granted('read', request.get('_legacy_controller'))", redirectRoute="admin_orders_index")
     *
     * @param int $orderId
     */
    public function getProductsListAction(int $orderId)
    {
        return $this->orderController->getProductsListAction($orderId);
    }

    /**
     * @AdminSecurity(
     *     "is_granted('update', request.get('_legacy_controller'))",
     *     message="You do not have permission to generate this."
     * )
     *
     * Generates invoice for given order
     *
     * @param int $orderId
     */
    public function generateInvoiceAction(int $orderId)
    {
        return $this->orderController->generateInvoiceAction($orderId);
    }

    /**
     * Sends email with process order link to customer
     *
     * @AdminSecurity("is_granted('update', request.get('_legacy_controller')) || is_granted('create', 'AdminOrders')")
     *
     * @param Request $request
     */
    public function sendProcessOrderEmailAction(Request $request)
    {
        return $this->orderController->sendProcessOrderEmailAction($request);
    }

    /**
     * @AdminSecurity(
     *     "is_granted(['update', 'delete'], request.get('_legacy_controller'))",
     *     redirectRoute="admin_orders_view",
     *     redirectQueryParamsToKeep={"orderId"},
     *     message="You do not have permission to edit this."
     * )
     *
     * @param int $orderId
     * @param Request $request
     */
    public function cancellationAction(int $orderId, Request $request)
    {
        return $this->orderController->cancellationAction($orderId, $request);
    }

    /**
     * @AdminSecurity("is_granted('update', request.get('_legacy_controller'))")
     *
     * @param Request $request
     */
    public function configureProductPaginationAction(Request $request)
    {
        return $this->orderController->configureProductPaginationAction($request);
    }

    /**
     * Method for downloading customization picture
     *
     * @AdminSecurity("is_granted('read', request.get('_legacy_controller'))")
     *
     * @param int $orderId
     * @param string $value
     */
    public function displayCustomizationImageAction(int $orderId, string $value)
    {
        return $this->displayCustomizationImageAction($orderId, $value);
    }

    /**
     * Set order internal note.
     *
     * @AdminSecurity(
     *     "is_granted(['update', 'create'], request.get('_legacy_controller'))",
     *     redirectRoute="admin_orders_index"
     * )
     *
     * @param mixed $orderId
     * @param Request $request
     */
    public function setInternalNoteAction($orderId, Request $request)
    {
        return $this->setInternalNoteAction($orderId, $request);
    }

    /**
     * @AdminSecurity(
     *     "is_granted(['create', 'update'], request.get('_legacy_controller'))",
     *     message="You do not have permission to perform this search."
     * )
     *
     * @param Request $request
     */
    public function searchProductsAction(Request $request)
    {
        return $this->orderController->searchProductsAction($request);
    }
}
