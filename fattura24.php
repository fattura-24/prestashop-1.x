<?php
/**
 * 2007-2019 PrestaShop.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    Fattura24 <fattura24.com>
 * @copyright Copyright (c) Fattura24
 * @license   Commercial license
 */

/**
 * File principale del modulo
 */ 

declare(strict_types=1);

require_once __DIR__ . '/vendor/autoload.php';

if (!defined('_PS_VERSION_')) {
    exit;
}

/**
 * File di servizio esterni per funzioni come invia ticket,
 * memorizza e vedi impostazioni e simili
 */
$filesToInclude = [
    'sendTicket.php',
    'settingsOptions.php',
    'helpers.php',
];

foreach ($filesToInclude as $file) {
    require_once 'methods/' . $file;
}

class Fattura24 extends Module
{
    private $phpversion;
    private $baseUrl;
    private $moduleDate;
    private $sourceInfo;

    /**
     * Costruttore del modulo. La prop version va aggiornata insieme alla sezione version del
     * file config.xml
     */
    public function __construct()
    {
        // $this->baseUrl = "https://www.app.fattura24.com/api/v0.4/";
        $this->name = 'fattura24';
        $this->tab = 'administration';
        $this->version = '2.9.0'; // modificare insieme al file config.xml
        $this->author = 'Fattura24.com';
        $this->module_key = '00cd0339f7de777834493205747ac875';
        $this->need_instance = 0;
        $this->bootstrap = true;
        $this->phpversion = phpversion();
        $this->baseUrl = 'https://www.app.fattura24.com/api/v0.3/';
        $this->moduleDate = '2023/01/16 15:00';
        $this->sourceInfo = 'F24-Pre ' . $this->version;
        $this->displayName = $this->l('Fattura24.com');
        $this->description = $this->l('Create your invoices by Fattura24.com');
        parent::__construct();
    }

    /**
     * Azioni svolte in fase di installazione del modulo
     * - Creazione tabella personalizzata campi pec e codice destinatario;
     * - Aggiunta colonne personalizzate alla tabella ordini;
     * - Registrazione degli hooks utilizzati dal modulo;
     * - Creazione della cartella dove salvare i file PDF
     */
    public function install()
    {
        $f24_fields = f24Options();

        foreach ($f24_fields as $field => $value) {
            if (0 == Tools::strlen(Configuration::get($field))) {
                Configuration::updateValue($field, $value);
            }
        }

        Configuration::updateValue('PS_F24_LOGFILE', $this->getLogFileName());

        $dim = 'update `' . _DB_PREFIX_ . 'order_state` set pdf_invoice= \'';
        $dim .= pSQL('0') . '\' where id_order_state=\'2\'';
        Db::getInstance()->Execute($dim);

        /**
         * Gestione campi aggiuntivi con tabella.
         */
        $sql = [];

        $sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'fattura24` (
            `id_fattura24` int(11) NOT NULL AUTO_INCREMENT,
            `id_customer` int(11),
            `fattura24_codice_destinatario` varchar(13),
            `fattura24_pec` varchar(60),
            PRIMARY KEY  (`id_fattura24`)
        ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;';

        foreach ($sql as $query) {
            if (false == Db::getInstance()->execute($query)) {
                return false;
            }
        }

        if (!parent::install()) {
            return false;
        }

        /**
         * Nuovo metodo per aggiungere le nuove colonne alla tabella ordini di Prestashop
         * Le aggiungo solo se non esistono già. In caso di azione fallita scrivo nel log l'errore
         * Davide Iandoli 19.08.2020.
         */
        $sql_orders = 'DESCRIBE `' . _DB_PREFIX_ . 'orders`';
        $fields = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS($sql_orders);

        $new_fields = [
            'docIdFattura24',
            'docIdOrderFattura24',
            'apiResponseOrder',
            'apiResponseInvoice',
        ];

        foreach ($new_fields as $field) {
            if (!array_search($field, array_column($fields, 'Field'))) {
                try {
                    $query = 'ALTER TABLE `' . _DB_PREFIX_ . 'orders` ADD `' . $field .
                        '` varchar(500) DEFAULT NULL';
                    Db::getInstance()->execute($query);
                } catch (PrestaShopDatabaseException $e) {
                    // $this->trace(' qualcosa è andato storto :', $e);
                }
            }
        }

        // registro gli hooks che utilizzo
        $fatt24Hooks = [
            'displayAdminCustomers',
            'displayAdminOrder',
            'displayCustomerAccountForm',
            'displayPDFInvoice',
            'actionCustomerAccountAdd',
            'actionCustomerAccountUpdate',
            'actionValidateOrder',
            'actionOrderStatusUpdate',
            'actionOrderStatusPostUpdate',
        ];

        foreach ($fatt24Hooks as $hook) {
            if (!$this->registerHook($hook)) {
                $this->registerHook($hook);
            }
        }

        // crea cartella per il PDF
        $documentsFolder = _PS_DOWNLOAD_DIR_ . 'Fattura24/';
        if (!file_exists($documentsFolder)) {
            mkdir($documentsFolder, 0777, true);
        }

        if (!file_exists($documentsFolder . 'index.php')) {
            file_put_contents($documentsFolder . 'index.php', '<?php');
        }

        $this->trace('Modulo installato correttamente!');

        return true;
    }

    /**
     * Rimozione tabella personalizzata
     * Eseguita in fase di rimozione modulo
     */
    private function uninstallDB()
    {
        return Db::getInstance()->execute('
       DROP TABLE IF EXISTS ' . _DB_PREFIX_ . 'fattura24;');
    }

    /**
     * Azioni eseguite in fase di rimozione modulo
     */
    public function uninstall()
    {
        if (!parent::uninstall()) {
            Configuration::deleteByName('PS_FATTURA24_API');
        }
        Configuration::deleteByName('PS_SALVA_CLIENTE');

        $this->uninstallDB();
        parent::uninstall();

        return true;
    }

    /**
     * Legge il contenuto delle impostazioni del modulo
     * e lo stampa a video
     */
    public function getContent()
    {
        $employee = new Employee($this->context->employee->id);

        /**
         * Api calls to get lists. By this method I call API only three times
         * and pass the result as a parameter to my method.
         */
        $test_url = $this->baseUrl . 'TestKey';
        $f24_template_url = $this->baseUrl . 'GetTemplate';
        $f24_pdc_url = $this->baseUrl . 'GetPdc';
        $f24_sezionali_url = $this->baseUrl . 'GetNumerator';
        $send_data = [];
        $send_data['apiKey'] = Configuration::get('PS_FATTURA24_API');
        $send_data['source'] = $this->sourceInfo;
        $testKey = $this->curlDownload($test_url, http_build_query($send_data));
        $F24accountInfo = getF24AccountInfo($testKey);
        $ps_test_key = Configuration::get('PS_F24_TEST_KEY'); // ultimo test API
        $test_res = getTestRes($ps_test_key);
        $resultArray = $this->getTestMsg($test_res);
        $showMessage = $this->showMessage($resultArray);

        // fix introdotto per gestire i casi in cui il campo input API Key è vuoto
        if (empty($send_data['apiKey'])) {
            $templates = '';
            $f24_pdc = '';
            $f24_sezionali = '';
        } else {
            $message_displayed = isset($testKey['displayed']) && $testKey['displayed'] == true;
            $templates = $message_displayed ? '' : $this->curlDownload(
                $f24_template_url,
                http_build_query($send_data)
            )['output'];
            // se è fallita la prima chiamata non faccio le altre due
            $f24_pdc = $message_displayed ? '' : $this->curlDownload(
                $f24_pdc_url,
                http_build_query($send_data)
            )['output'];
            $f24_sezionali = $message_displayed ? '' : $this->curlDownload(
                $f24_sezionali_url,
                http_build_query($send_data)
            )['output'];
        }

        /**
         * aggiunge Javascript e stili
         */
        $jsFiles = ['admin.js', 'settings.js'];
        foreach ($jsFiles as $file) {
            $this->context->controller->addJs($this->_path . '/views/js/' . $file);
        }
        $this->context->controller->addCSS($this->_path . '/views/css/admin.css');
        $output = $this->getNaturaError() . $this->getUpdateMessage()
            . $this->getApiError($resultArray, $showMessage) . $this->getConfigError();

        // Al click su 'Salva' aggiorna le impostazioni e visualizza messaggi eventuali    
        if (Tools::isSubmit('submitModule')) {
            updateConfig();
            $output = $this->getUpdateMessage() . $this->getNaturaError() . $this->getApiError($resultArray, true) . $this->getConfigError();
        }

        /*
         * Qui rifaccio la chiamata utilizzando il dato immesso nel campo
         * altrimenti dopo aver fatto click sul pulsante il test potrebbe fornirmi
         * un messaggio fuorviante. Attenzione: essendo il pulsante 'submit'
         * il click innesca l'aggiornamento della pagina delle opzioni
         */
        if (Tools::isSubmit('testApiKey')) {
            $now = $this->now();
            $send_data['apiKey'] = Tools::getValue('PS_FATTURA24_API');
            $testKey = $this->curlDownload($test_url, http_build_query($send_data));
            Configuration::updateValue('PS_F24_TEST_KEY', $this->setF24ApiResult($testKey, $now));
            $output = $this->getUpdateMessage() . $this->getNaturaError() . $this->getApiError($resultArray, true) . $this->getConfigError();
        }

        /**
         * Azione collegata al pulsante invia nella sezione 'Supporto'
         */
        if (Tools::isSubmit('submitSupport')) {
            $values = [
                'name' => $employee->firstname . ' ' . $employee->lastname,
                'source' => $this->sourceInfo,
                'account_id' => $F24accountInfo['accountId'],
                'email_owner' => $F24accountInfo['emailOwner'],
                'test_key' => $testKey,
                'module_config' => getConfigFieldsValues(),
            ];
            $fieldNames = ['subject', 'email', 'order_id_reference', 'text_message'];
            foreach ($fieldNames as $val) {
                $values[$val] = trim(Tools::getValue((string) $val));
            }

            $idReference = $values['order_id_reference'];
            $order = '';
            if (!empty($idReference)) {
                if (is_numeric($idReference)) {
                    $order = new Order((int) $idReference);
                } else {
                    $order = new Order((int) $this->getOrderIdByReference($idReference));
                }
                $values['order_data'] = [
                    'general' => var_export($order, true),
                    'products' => var_export($order->getProducts(), true),
                    'coupons' => var_export($order->getCartRules(), true),
                ];
                $order_id = $order->id;
                $values['xml'] = $this->createInvoiceXml($order_id, true);
            }

            $values['info'] = $this->getInfo();
            // $response = '';
            $message['response'] = '';

            if (empty($values['text_message'])) {
                $output .= $this->displayError($this->l('You cannot send a ticket with an empty message.'));
            } else {
                $message = sendTicket($values);
                if ('ok' == $message['response']) {
                    $output .= $this->displayConfirmation($this->l('Message sent successfully'));
                } else {
                    $err_Msg = $this->l('Unable to send message! Please email to assistenza@fattura24.com');
                    $output .= $this->displayError($err_Msg);
                    $this->trace('Impossibile inviare il messaggio! ', $message['message']);
                }
            }
        }
        // fine codice legato alla sezione 'Supporto'

        if (Tools::usingSecureMode()) {
            $domain = Tools::getShopDomainSsl(true, true);
        } else {
            $domain = Tools::getShopDomain(true, true);
        }
        $lezioni = getLessons();

        /**
         * Assegna i valori delle opzioni a Smarty
         */
        $this->context->smarty->assign(
            [
                'logo' => $domain . __PS_BASE_URI__
                        . basename(_PS_MODULE_DIR_)
                        . '/' . $this->name . '/views/img/logo_orange.png',
                'lezioni' => $lezioni,
                'token' => Tools::getValue('token'),
                'adminEmail' => $employee->email,
                'version' => $this->version,
                'source' => $this->sourceInfo,
                'apiKey' => Configuration::get('PS_FATTURA24_API'),
                'listaModelliOrdine' => $this->getTemplate($templates, true),
                'numOrdine' => $this->f24OrderNumber(),
                'numOrdineSelected' => Configuration::get('PS_NUMERO_ORDINE'),
                'modelloOrdine' => Configuration::get('PS_F24_MODELLO_ORDINE'),
                'modelloOrdineDest' => Configuration::get('PS_F24_MODELLO_ORDINE_DEST'),
                'listaModelliFattura' => $this->getTemplate($templates, false),
                'opzioniCreaFattura' => $this->getInvoiceOptions(),
                'tipiAliquoteIva' => getListaNatureNew(),
                'opzioniStatoPagato' => $this->f24PaidStatus(),
                'modelloFattura' => Configuration::get('PS_F24_MODELLO_FATTURA'),
                'modelloFatturaDest' => Configuration::get('PS_F24_MODELLO_FATTURA_DEST'),
                'listaConti' => $this->getPdc($f24_pdc),
                'conto' => Configuration::get('PS_F24_PDC'),
                'salvaCliente' => Configuration::get('PS_SALVA_CLIENTE'),
                'creazioneOrdine' => Configuration::get('PS_CREA_ORDINE'),
                'abilitaOrdineZero' => Configuration::get('PS_ABILITA_ORDINE_ZERO'),
                'pdfOrdine' => Configuration::get('PS_PDF_ORDINE'),
                'pdfFattura' => Configuration::get('PS_PDF_FATTURA'),
                'emailOrdine' => Configuration::get('PS_MAIL_ORDINE'),
                'creazioneFattura' => Configuration::get('PS_CREAZIONE_FATTURA'),
                'abilitaFatturaZero' => Configuration::get('PS_ABILITA_FATTURA_ZERO'),
                'confNaturaIva' => Configuration::get('PS_CONF_NATURA_IVA'),
                'naturaSpedizione' => Configuration::get('PS_CONF_NATURA_SPEDIZIONE'),
                'invObject' => Configuration::get('PS_INV_OBJECT'),
                // oggetto del documento
                'emailFattura' => Configuration::get('PS_EMAIL_FATTURA'),
                'statoPagato' => Configuration::get('PS_STATO_PAGATO'),
                'disabilitaRicevute' => Configuration::get('PS_DISABILITA_RICEVUTE'),
                'listaSezionaliFattura' => $this->getNumerator($f24_sezionali, 1),
                'listaSezionaliFatturaElettronica' => $this->getNumerator($f24_sezionali, 11),
                'listaSezionaliRicevuta' => $this->getNumerator($f24_sezionali, 3),
                'sezionaleRicevuta' => Configuration::get('PS_F24_SEZIONALE_RICEVUTA'),
                'sezionaleFattura' => Configuration::get('PS_F24_SEZIONALE_FATTURA'),
                'bolloVirtualeFe' => $this->f24StampOptions(),
                'bollo' => Configuration::get('PS_F24_BOLLO_VIRTUALE_FE'),
                'moduleUrl' => _MODULE_DIR_ . 'fattura24/',
                'debugEnable' => Configuration::get('PS_F24_DEBUG_MODE'),
                'logFileName' => $this->getLogFileName(),
                'info' => $this->getInfo(),
            ]
        );

        // e li passa al template perché vengano stampati a video in HTML
        $output .= $this->display(__FILE__, 'views/templates/admin/main.tpl'); // tutti i dati passano al file form.tpl

        return $output;
    }

    /**
     * Aggancio la funzione di visualizzazione dei clienti lato admin
     * per aggiungere i campi pec e codice destinatario
     */
    public function hookDisplayAdminCustomers($params)
    {
        // template per versioni PS > 1.7
        if (Tools::substr(_PS_VERSION_, 0, 3) >= 1.7) {
            $idc = $params['id_customer'];
            $template_path = 'views/templates/admin/admin_customer.tpl'; // template per ps 1.7
        // template per versioni PS < 1.7
        } else {
            $idc = pSQL(Tools::getValue('id_customer'));
            $template_path = 'views/templates/admin/admin_customer16.tpl'; // template per ps 1.6
        }

        $sdi_code = Tools::getValue('fattura24_codice_destinatario');
        if (!$sdi_code or Tools::strlen($sdi_code) <= 1) {
            $sdi_code = '0000000';
        }

        if (Tools::getValue('id_fattura24') > 0) {
            $sql = 'update `' . _DB_PREFIX_ . "fattura24` set fattura24_codice_destinatario = '";
            $sql .= pSQL((string) $sdi_code) . "',fattura24_pec='";
            $sql .= pSQL(Tools::getValue('fattura24_pec')) . "' where id_fattura24 = ";
            $sql .= (int) Tools::getValue('id_fattura24') . ' ';
            Db::getInstance()->Execute($sql);
        } else {
            $sql = 'INSERT INTO `' . _DB_PREFIX_ . 'fattura24`(id_customer,fattura24_codice_destinatario,fattura24_pec) 
                    VALUES (' . (int) $idc . ",'" . pSQL((string) $sdi_code) . "','"
                . pSQL(Tools::getValue('fattura24_pec')) . "')";
            Db::getInstance()->Execute($sql);
        }

        $sql = 'select id_fattura24,fattura24_codice_destinatario,fattura24_pec from `';
        $sql .= _DB_PREFIX_ . 'fattura24` where id_customer = ' . (int) $idc . '';
        $result = Db::getInstance()->getRow($sql);

        // link controller Admin per PS > 1.7
        if (Tools::substr(_PS_VERSION_, 0, 5) >= 1.7) {
            $linkController = Context::getContext()->link->getAdminLink('AdminCustomers', true, [], [
                'id_customer' => $idc,
                'viewcustomer' => 1,
            ]);
        // link controller Admin per PS < 1.6    
        } else {
            $linkController = $this->context->link->getAdminLink('AdminCustomers', true);
            $linkController .= '&id_customer=' . $idc . '&viewcustomer';
        }

        // assegno i valori a Smarty
        $this->context->smarty->assign('linkController', $linkController);
        $this->context->smarty->assign('id_fattura24', $result['id_fattura24']);
        $this->context->smarty->assign('fattura24_codice_destinatario', $result['fattura24_codice_destinatario']);
        $this->context->smarty->assign('fattura24_pec', $result['fattura24_pec']);

        // perché li stampi a video in HTML
        $output = $this->context->smarty->fetch($this->local_path . $template_path);

        return $output;
    }

    /**
     * Hook con cui aggiungo all'ordine PS le informazioni relative alla presenza di ordini
     * o ricevute create in Fattura24 
     */ 
    public function hookDisplayAdminOrder(array $params)
    {
        $orderId = (int) $params['id_order'];
        $order = new Order($orderId);
        $order_status = $order->getCurrentOrderState();
        if (is_object($order_status)) {
            $status_name = current($order_status->name);
            if ($status_name == $this->l('Cancelled')) {
                $order_status_name = '';
            } else {
                $order_status_name = $status_name;
            }
        } else {
            $order_status_name = $order_status;
        }
        $query = 'SELECT docIdFattura24, docIdOrderFattura24 FROM ' . _DB_PREFIX_;
        $query .= 'orders WHERE id_order=\'' . (int) $orderId . '\';';
        $docIdFattura24 = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($query)['docIdFattura24'];
        $docIdOrderFattura24 = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($query)['docIdOrderFattura24'];
        $new_query = '';
        $existsInF24 = false;
        $downloadInvoicePdf = 1 == (int) Configuration::get('PS_PDF_FATTURA');

        if (!empty($docIdFattura24) && $downloadInvoicePdf) {
            // dovrebbe scaricare di nuovo il PDF se ho il docId e il file non esiste
            $existsInF24 = !simplexml_load_string((string) $this->downloadDocument($docIdFattura24, $orderId, true));
        }

        try {
            $new_query = 'SELECT apiResponseOrder, apiResponseInvoice from ' . _DB_PREFIX_ .
                $new_query .= 'orders WHERE id_order=\'' . (int) $orderId . '\';';
            $apiResponseOrder = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($new_query)['apiResponseOrder'];
            $apiResponseInvoice = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($new_query)['apiResponseInvoice'];
            $obj_apiResponseOrder = isset($apiResponseOrder) ? json_decode($apiResponseOrder) : '';
            $obj_apiResponseInvoice = isset($apiResponseInvoice) ? json_decode($apiResponseInvoice) : '';
            if (is_object($obj_apiResponseOrder)) {
                $orderResponseDescription = $obj_apiResponseOrder->description;
            } else {
                $orderResponseDescription = '';
            }

            if (is_object($obj_apiResponseInvoice)) {
                $invoiceResponseDescription = $obj_apiResponseInvoice->description;
            } else {
                $invoiceResponseDescription = '';
            }
        } catch (PrestaShopDatabaseException $e) {
            // se le colonne non esistono
            // $this->trace(' errore nel db : ', $e);
            $orderResponseDescription = '';
            $invoiceResponseDescription = '';
        }

        $config_CreaOrdine = Configuration::get('PS_CREA_ORDINE');
        $config_CreaFattura = Configuration::get('PS_CREAZIONE_FATTURA');
        $config_Overrides = Configuration::get('PS_DISABLE_OVERRIDES');

        $fileName = _PS_DOWNLOAD_DIR_ . 'Fattura24/' . $this->constructFileName($order->id, $order->reference);
        $fileExists = $existsInF24 && file_exists($fileName);

        $order_documents = 0;
        if (!empty($docIdOrderFattura24 || false !== strpos($orderResponseDescription, 'already exists'))) {
            ++$order_documents;
        }

        if (!empty($docIdFattura24 || false !== strpos($invoiceResponseDescription, 'already exists'))) {
            ++$order_documents;
        }

        if (!$config_CreaOrdine) {
            $message_order = $this->l('invoice creation disabled');
        } elseif (empty($order_status_name)) {
            $message_order = $this->l('order cancelled in Prestashop');
        } else {
            if (!empty($docIdOrderFattura24 || false !== strpos($orderResponseDescription, 'already exists'))) {
                $message_order = $this->l('document saved in Fattura24');
            } elseif (false !== strpos($orderResponseDescription, 'Invalid ID Request')) {
                $message_order = $this->l('processing data in Fattura24, retry later');
            } else {
                $message_order = $this->l('document NOT saved in Fattura24');
            }
        }

        if (!$config_CreaFattura) {
            $message_invoice = $this->l('document creation disabled');
        } elseif (empty($order_status_name)) {
            $message_invoice = $this->l('order cancelled in Prestashop');
        } else {
            if (!empty($docIdFattura24 || false !== strpos($invoiceResponseDescription, 'already exists'))) {
                if (isset($obj_apiResponseInvoice->docNumber)) {
                    try {
                        $message_invoice = $this->l('doc n. ')
                            . $obj_apiResponseInvoice->docNumber
                            . $this->l(' saved in Fattura24');
                    } catch (Exception $e) {
                        $message_invoice = $this->l('document saved in Fattura24');
                    }
                } else {
                    $message_invoice = $this->l('document saved in Fattura24');
                }
            } elseif (false !== strpos($invoiceResponseDescription, 'Invalid ID Request')) {
                $message_invoice = $this->l('processing data in Fattura24, retry later');
            } else {
                $message_invoice = $this->l('document NOT saved in Fattura24');
            }
        }

        // uso order_documents per incrementare il contatore

        $this->context->smarty->assign('id_order', $orderId);
        $this->context->smarty->assign('order_status_name', $order_status_name);
        $this->context->smarty->assign('config_CreaOrdine', $config_CreaOrdine);
        $this->context->smarty->assign('config_CreaFattura', $config_CreaFattura);
        $this->context->smarty->assign('config_Overrides', $config_Overrides);
        $this->context->smarty->assign('file_exists', $fileExists);
        $this->context->smarty->assign('order_documents', $order_documents); // contatore documenti salvati
        $this->context->smarty->assign('fattura24_invoice_docId', $docIdFattura24); // docId per tasto fattura
        $this->context->smarty->assign('fattura24_invoice', $message_invoice);
        $this->context->smarty->assign('fattura24_order_docId', $docIdOrderFattura24); // docId per tasto ordine
        $this->context->smarty->assign('fattura24_order', $message_order);
        if (Tools::substr(_PS_VERSION_, 0, 5) >= '1.7.7') {
            return $this->display(__FILE__, 'views/templates/hook/displayAdminOrderMainBottom.tpl');
        } else {
            return $this->display(__FILE__, 'views/templates/hook/displayAdminOrderLeft.tpl');
        }
    }

    /**
     * Hook per la gestione dei campi SDI e PEC
     * in fase di acquisto da parte del cliente
     */
    public function hookDisplayCustomerAccountForm($params)
    {
        if ($params['cookie']->id_customer) {
            $sql = 'select id_fattura24,fattura24_codice_destinatario,fattura24_pec from `' . _DB_PREFIX_;
            $sql .= 'fattura24` where id_customer = ' . (int) $params['cookie']->id_customer . '';
            $result = Db::getInstance()->getRow($sql);
            if ($result) {
                $this->context->smarty->assign('id_fattura24', $result['id_fattura24']);
                $this->context->smarty->assign('fattura24_id_customer', (int) $params['cookie']->id_customer);
                $this->context->smarty->assign(
                    'fattura24_codice_destinatario',
                    $result['fattura24_codice_destinatario']
                );
                $this->context->smarty->assign('fattura24_pec', $result['fattura24_pec']);
            } else {
                $this->context->smarty->assign('id_fattura24', '');
                $this->context->smarty->assign('fattura24_id_customer', 0);
                $this->context->smarty->assign('fattura24_codice_destinatario', '');
                $this->context->smarty->assign('fattura24_pec', '');
            }

            return $this->display(__FILE__, 'views/templates/hook/customer_reg_form.tpl');

            /* gestione account in caso di checkout ospite corretto il 21.03.2019 - fix per ps 1.7 - */
        } else {
            $this->context->smarty->assign('id_fattura24', '');
            $this->context->smarty->assign('fattura24_id_customer', (int) $params['cookie']->id_customer);
            $this->context->smarty->assign('fattura24_codice_destinatario', '');
            $this->context->smarty->assign('fattura24_pec', '');

            return $this->display(__FILE__, 'views/templates/hook/customer_reg_form.tpl');
        }
    }

     /**
     * Hook per la gestione dei campi SDI e PEC
     * in registrazione nuovo cliente lato negozio
     */
    public function hookDisplayCustomerIdentityForm($params)
    {
        $sdi_code = Tools::getValue('fattura24_codice_destinatario');
        $result = '';
        $customer = '';

        if (!$sdi_code or Tools::strlen($sdi_code) <= 1) {
            $sdi_code = '0000000';
        }

        if (Tools::getValue('id_fattura24') > 0) {
            $sql = 'update `' . _DB_PREFIX_ . "fattura24` set fattura24_codice_destinatario = '";
            $sql .= pSQL((string) $sdi_code) . "',fattura24_pec='" . pSQL((int) Tools::getValue('fattura24_pec'));
            $sql .= "' where id_fattura24 = " . (int) Tools::getValue('id_fattura24') . ' ';
            Db::getInstance()->Execute($sql);
        } elseif (Tools::getValue('fattura24_id_customer')) {
            $sql = 'INSERT INTO `' . _DB_PREFIX_ . 'fattura24`(id_customer,fattura24_codice_destinatario,fattura24_pec) 
                    VALUES (' . pSQL((int) Tools::getValue('fattura24_id_customer')) . ",'"
                . pSQL((string) $sdi_code) . "','" . pSQL((string) Tools::getValue('fattura24_pec')) . "')";
            Db::getInstance()->Execute($sql);
        }

        if ($params['cookie']->id_customer) {
            $sql = 'select id_fattura24,fattura24_codice_destinatario,fattura24_pec from `' . _DB_PREFIX_;
            $sql .= 'fattura24` where id_customer = ' . (int) $params['cookie']->id_customer . '';
            $result = Db::getInstance()->getRow($sql);
            $customer = $result['id_customer'];

            /* gestione account ospite fix per ps 1.7 */
        } elseif ($params['cookie']->id_guest) {
            $sql = 'select id_fattura24,fattura24_codice_destinatario,fattura24_pec from `' . _DB_PREFIX_;
            $sql .= 'fattura24` where id_customer = ' . (int) $params['cookie']->id_guest . '';
            $result = Db::getInstance()->getRow($sql);
            $customer = $result['id_guest'];
        }

        $this->context->smarty->assign('id_fattura24', $result['id_fattura24']);
        $this->context->smarty->assign('fattura24_id_customer', $customer);
        $this->context->smarty->assign('fattura24_codice_destinatario', $result['fattura24_codice_destinatario']);
        $this->context->smarty->assign('fattura24_pec', $result['fattura24_pec']);

        return $this->display(__FILE__, 'views/templates/hook/customer_reg_form.tpl');
    }

    /**
     * Aggiunta campi PEC e SDI al modulo di
     * aggiunta cliente lato admin 
     */
    public function hookActionCustomerAccountAdd($params)
    {
        $newCustomer = $params['newCustomer'];
        $sql = 'INSERT INTO `' . _DB_PREFIX_ . 'fattura24`(id_customer,fattura24_codice_destinatario,fattura24_pec)
                  VALUES (' . (int) $newCustomer->id . ",'" . pSQL((string)
                  Tools::getValue('fattura24_codice_destinatario'))
            . "','" . pSQL((string) Tools::getValue('fattura24_pec')) . "')";
        Db::getInstance()->Execute($sql);

        if (0) {
            $handle = fopen(dirname(__FILE__) . '/notify.html', 'a');
            fwrite($handle, "*********new*********<br>\n");
            fwrite($handle, 'start: ' . " \n<br>\n [" . date('Y-m-d h-m-s') . "]<br><br>\n");

            fwrite($handle, 'pec:' . Tools::getValue('fattura24_pec'));
            fwrite($handle, 'codice_destinatario:' . Tools::getValue('fattura24_codice_destinatario'));
            fwrite($handle, 'customer:' . $newCustomer->id);
            fwrite($handle, 'sql:' . $sql);

            fwrite($handle, 'end: ' . " \n<br>\n [" . date('Y-m-d h-m-s') . "]<br><br>\n");
            fclose($handle);
        }
    }

    // aggiorna i campi fiscali nell'account utente
    public function hookActionCustomerAccountUpdate($params)
    {
        $sdi_code = Tools::getValue('fattura24_codice_destinatario');
        if (!$sdi_code or Tools::strlen($sdi_code) <= 1) {
            $sdi_code = '0000000';
        }

        if (Tools::getValue('id_fattura24') > 0) {
            $sql = 'update `' . _DB_PREFIX_ . "fattura24` set fattura24_codice_destinatario = '";
            $sql .= pSQL((string) $sdi_code) . "',fattura24_pec='" . pSQL((string) Tools::getValue('fattura24_pec'));
            $sql .= "' where id_fattura24 = " . (int) Tools::getValue('id_fattura24') . ' ';
            Db::getInstance()->Execute($sql);
        } elseif (Tools::getValue('fattura24_id_customer')) {
            $sql = 'INSERT INTO `' . _DB_PREFIX_ . 'fattura24`(id_customer,fattura24_codice_destinatario,fattura24_pec) 
                    VALUES (' . pSQL((int) Tools::getValue('fattura24_id_customer')) . ",'"
                . pSQL((string) $sdi_code) . "','" . pSQL((string) Tools::getValue('fattura24_pec')) . "')";
            Db::getInstance()->Execute($sql);
        }
    }

    /* fine hook aggiuntivi */

    /**
     * Hook di convalida ordine: in base ala configurazione del modulo
     * si decide se creare un ordine in Fattura24 oppure innescare la chiamata
     * SaveCustomer 
     */
    public function hookActionValidateOrder($order)
    {
        if (!$this->active) {
            return;
        }

        $check_order = isset($order['order']) ? $order['order'] : $order['objOrder'];
        $sendOrder = 1 == (int) Configuration::get('PS_CREA_ORDINE');
        $zeroTotal = 0.00 == (float) $check_order->total_paid;
        $zeroOrderEnabled = 1 == (int) Configuration::get('PS_ABILITA_ORDINE_ZERO');
        $sendZeroOrder = $zeroTotal && $zeroOrderEnabled;

        if ($sendOrder && !$zeroTotal) {
            $this->afterHook($check_order, false);
        } elseif ($sendZeroOrder) {
            $this->afterHook($check_order, false);
        } else {
            $this->saveCustomer($check_order);
        }
    }

    private function generatePrestashopPDF($orderId) {
        $order_invoice = new OrderInvoice($orderId);
        if (Validate::isLoadedObject($order_invoice)) {
            $pdf = new PDF($order_invoice, PDF::TEMPLATE_INVOICE, $this->context->smarty);
            $pdf->render();
        } else {
            $this->trace('Errore nel caricamento dell\'ordine per generare il PDF di PrestaShop');
        }
    }

    /**
     * 
     * visualizza/scarica il PDF di Fattura24 nell'elenco degli ordini lato admin
     * e nella sezione 'Storico degli ordini' quando il cliente accede lato negozio
     * @return void
     */
    public function hookDisplayPDFInvoice($order) {
        // Inizia il buffer di output
        ob_start();
    
        // Ottieni i dettagli dell'ordine
        $ps_order = $order['object']->getOrder(); // qui stiamo in OrderInvoice
        $orderId = $ps_order->id;
        $orderReference = $ps_order->reference;

        if (1 !== (int)Configuration::get('PS_PDF_FATTURA')) {
            ob_end_clean();
            return $this->generatePrestashopPDF($orderId);
        }
    
        // Costruisci il nome e il percorso del file PDF
        $fileName = $this->constructFileName($orderId, $orderReference);
        $filePath = _PS_DOWNLOAD_DIR_ . 'Fattura24/' . $fileName;
    
        // Controlla se il file esiste e ha una dimensione adeguata
        if (!file_exists($filePath) || filesize($filePath) < 200) {
            $this->trace('File non esiste o è troppo piccolo, tento il download da Fattura24');
    
            // Recupera l'ID del documento Fattura24
            $query = 'SELECT docIdFattura24 FROM ' . _DB_PREFIX_ . 'orders WHERE id_order=\'' . (int)$orderId . '\';';
            $docIdFattura24 = \Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($query)['docIdFattura24'];
    
            // Scarica il documento se l'ID esiste
            if (!empty($docIdFattura24)) {
                $this->downloadDocument($docIdFattura24, $orderId, true);
            }
        }
    
        // Verifica ancora se il file esiste e ha una dimensione adeguata dopo il download
        if (file_exists($filePath)) {
            $this->trace('File esiste dopo il download, dimensione file: ' . filesize($filePath));
    
            if (filesize($filePath) > 200) {
                $this->trace('Il file ha una dimensione adeguata, procedo con il download');
    
                // Svuota il buffer di output prima di impostare le intestazioni
                ob_end_clean();
    
                // Imposta le intestazioni per il download del file PDF
                header('Content-type: application/pdf');
                header('Content-Disposition: attachment; filename="' . $fileName . '"');
                header('Content-Transfer-Encoding: binary');
    
                // Leggi e invia il file al browser
                readfile($filePath);
                die('File pdf di Fattura24 scaricato e inviato al browser'); // Termina l'esecuzione dello script dopo aver inviato il file
            } else {
                $this->trace('Il file è troppo piccolo dopo il download, generazione del PDF di PrestaShop');
            }
        } else {
            $this->trace('File non esiste dopo il download, generazione del PDF di PrestaShop');
        }
    
        // Svuota il buffer di output prima di generare il PDF di PrestaShop
        ob_end_clean();
        return $this->generatePrestashopPDF($orderId);
    }
    

    /**
     * Innesca la creazione della ricevuta/fattura al cambio di status dell'ordine PS
     * @param mixed $order
     * @return void
     */
    public function hookActionOrderStatusUpdate($order)
    {
        $orderPaid = 1 == (int) $order['newOrderStatus']->paid;
        // $this->trace('order :', $order);
        // $this->trace('order paid 1 :', $orderPaid);
        if (!$this->active || !$orderPaid || 0 == (int) Configuration::get('PS_CREAZIONE_FATTURA')) {
            return;
        }
        $orderId = $order['id_order'];
        $check_order = new Order($orderId);
        $zeroTotal = 0.00 == (float) $check_order->total_paid;
        $zeroInvoiceEnabled = 1 == (int) Configuration::get('PS_ABILITA_FATTURA_ZERO');

        $query = 'SELECT docIdFattura24 FROM ' . _DB_PREFIX_ . 'orders WHERE id_order=\'' . (int) $orderId . '\';';
        $docIdFattura24 = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($query)['docIdFattura24'];
        $sendZeroInvoice = $zeroTotal && $zeroInvoiceEnabled;

        if ($sendZeroInvoice && $zeroTotal && !$docIdFattura24) {
            $this->afterHook($check_order, true);
        } elseif (!$docIdFattura24 && !$zeroTotal) {
            $this->afterHook($check_order, true);
        }
    }

    /**
     * Azioni successive all'aggiornamento di status dell'ordine PS:
     * download e archiviazione del file PDF scaricato da Fattura24
     * @param mixed $order
     * @return void
     */
    public function hookActionOrderStatusPostUpdate($order)
    {
        $orderPaid = 1 == (int) $order['newOrderStatus']->paid;
        // $this->trace('order 2 :', $order);
        // $this->trace('order paid 2 :', $orderPaid);
        if (!$this->active || !$orderPaid || 0 == (int) Configuration::get('PS_CREAZIONE_FATTURA')) {
            return;
        }
        $orderId = $order['id_order'];
        $check_order = new Order($orderId);
        $number = $check_order->invoice_number;
        if (empty($number)) {
            return;
        }

        $newFileName = $this->constructFileName($orderId, $check_order->reference);
        $year = date('Y', strtotime($check_order->date_upd));
        $oldFileName = 'FA000000-' . $year . '.pdf';

        if (file_exists($oldFileName)) {
            // Qui viene fatta la rename del pdf
            $folder = _PS_DOWNLOAD_DIR_ . 'Fattura24/';
            rename($folder . $oldFileName, $folder . $newFileName);
        }
    }

    // Costruisce il nome del file PDF
    public function constructFileName($orderId, $orderReference)
    {
        return 'FA' . sprintf('%06d', $orderId) . '-' . $orderReference . '.pdf';
    }

    /**
     * Crea l'xml per l'azione SaveCustomer partendo dall'ordine PS
     */
    public function createCustomerXml($check_order)
    {
        $this->trace('Create Customer Xml');
        $domtree = new DOMDocument('1.0', 'UTF-8');
        $domtree->preserveWhiteSpace = false;
        $domtree->formatOutput = true;
        $xmlRoot = $domtree->createElement('Fattura24');
        $xmlRoot = $domtree->appendChild($xmlRoot);
        $xmlCustomer = $domtree->createElement('Document');
        $xmlCustomer = $xmlRoot->appendChild($xmlCustomer);
        $invoice_address = $check_order->id_address_invoice;
        $billing_data = new Address($invoice_address);
        $customer = new Customer($billing_data->id_customer);

        /**
         * Recupera i campi SDI e PEC per fatturazione elettronica
         * Davide Iandoli 08.03.2019.
         */
        $sql = 'select id_fattura24,fattura24_codice_destinatario,fattura24_pec from `' . _DB_PREFIX_;
        $sql .= 'fattura24` where id_customer = ' . (int) $billing_data->id_customer . '';
        $result = Db::getInstance()->getRow($sql);
        $pec = isset($result['fattura24_pec']) ? $result['fattura24_pec'] : '';
        $sdi_code = isset($result['fattura24_codice_destinatario']) ? $result['fattura24_codice_destinatario'] : '';

        /* controllo su codice destinatario */
        $isoCountry = Country::getIsoById($billing_data->id_country);

        if ('IT' != $isoCountry) {
            $sdi_code = 'XXXXXXX';
        } elseif (empty($result['fattura24_codice_destinatario'])) {
            $sdi_code = '0000000';
        } else {
            $sdi_code = $result['fattura24_codice_destinatario'];
        }

        $customer_fePec = $domtree->createElement('FeCustomerPec');
        $customer_fePec->appendChild($domtree->createCDataSection($pec));
        $xmlCustomer->appendChild($customer_fePec);

        $customer_feCodiceDestinatario = $domtree->createElement('FeDestinationCode');
        $customer_feCodiceDestinatario->appendChild($domtree->createCDataSection($sdi_code));
        $xmlCustomer->appendChild($customer_feCodiceDestinatario);

        $company = trim($billing_data->company);

        if (!empty($company)) {
            $customername = $company;
        } else {
            $customername = $billing_data->firstname . ' ' . $billing_data->lastname;
        }

        $customer_name = $domtree->createElement('CustomerName');
        $customer_name->appendChild($domtree->createCDataSection($customername));
        $xmlCustomer->appendChild($customer_name);

        $customer_address = $domtree->createElement('CustomerAddress');
        $customer_address2 = null == $billing_data->address2 ? '' : ' ' . $billing_data->address2;
        $customer_address->appendChild($domtree->createCDataSection($billing_data->address1 . $customer_address2));

        $xmlCustomer->appendChild($customer_address);
        $xmlCustomer->appendChild($domtree->createElement('CustomerPostcode', (string) $billing_data->postcode));
        $xmlCustomer->appendChild($domtree->createElement('CustomerCity', (string) $billing_data->city));
        if (!empty($billing_data->id_state)) {
            $state = new State($billing_data->id_state);
            $xmlCustomer->appendChild($domtree->createElement('CustomerProvince', (string) $state->iso_code));
        }

        $isoCountry = Country::getIsoById($billing_data->id_country);

        // if ($isoCountry != null) {
        $xmlCustomer->appendChild($domtree->createElement('CustomerCountry', (string) $isoCountry));
        // }

        if (!empty($billing_data->dni)) {
            $customer_fiscalcode = $domtree->createElement('CustomerFiscalCode');
            $customer_fiscalcode->appendChild($domtree->createCDataSection(Tools::strtoupper($billing_data->dni)));
            $xmlCustomer->appendChild($customer_fiscalcode);
        }

        if (!empty($billing_data->vat_number)) {
            $customerVatNumber = $this->cleanVatNumber($isoCountry, $billing_data->vat_number);
            $customer_vatcode = $domtree->CreateElement('CustomerVatCode');
            $customer_vatcode->appendChild($domtree->createCDataSection($customerVatNumber));
            $xmlCustomer->appendChild($customer_vatcode);
        }

        if (!empty($billing_data->phone) || !empty($billing_data->phone_mobile)) {
            $customer_cell_phone = $domtree->createElement('CustomerCellPhone');

            if (!empty($billing_data->phone)) {
                $customer_cell_phone->appendChild($domtree->createCDataSection($billing_data->phone));
            } elseif (!empty($billing_data->phone_mobile)) {
                $customer_cell_phone->appendChild($domtree->createCDataSection($billing_data->phone_mobile));
            }

            $xmlCustomer->appendChild($customer_cell_phone);
        }

        $customer_email = $domtree->createElement('CustomerEmail');
        $customer_email->appendChild($domtree->createCDataSection($customer->email));
        $xmlCustomer->appendChild($customer_email);

        return $domtree->saveXML();
    }

    /*
    * crea l'xml per l'azione SaveDocument
    * @param (string) $order_id
    * @param (bool) $is_Invoice
    */
    public function createInvoiceXml($order_id, $isInvoice)
    {
        $domtree = new DOMDocument('1.0', 'UTF-8');
        $domtree->preserveWhiteSpace = false;
        $domtree->formatOutput = true;
        $xmlRoot = $domtree->createElement('Fattura24');
        $xmlRoot = $domtree->appendChild($xmlRoot);
        $fattura_invoice = $domtree->createElement('Document');
        $fattura_invoice = $xmlRoot->appendChild($fattura_invoice);

        $order = new Order($order_id);
        $this->trace('dettaglio ordine :', $order);
        // mi prendo direttamente il codice ISO della valuta
        $currencyCode = Currency::getCurrency($order->id_currency)['iso_code'];
        $fattura_invoice->appendChild($domtree->createElement('Currency', (string) $currencyCode));
        $invoice_address = $order->id_address_invoice;
        $billing_data = new Address($invoice_address);
        $customer = new Customer($billing_data->id_customer);

        /**
         * Aggiungo i campi per la fattura elettronica
         * Davide Iandoli 8.03.2019.
         */
        $sql = 'select id_fattura24,fattura24_codice_destinatario,fattura24_pec from `' . _DB_PREFIX_;
        $sql .= 'fattura24` where id_customer = ' . (int) $order->id_customer . '';
        $result = Db::getInstance()->getRow($sql);
        $pec = $result ? $result['fattura24_pec'] : '';
        $isoCountry = Country::getIsoById($billing_data->id_country);

        if ('IT' != $isoCountry) {
            $sdi_code = 'XXXXXXX';
        } elseif (empty($result['fattura24_codice_destinatario'])) {
            $sdi_code = '0000000';
        } else {
            $sdi_code = $result['fattura24_codice_destinatario'];
        }

        $fattElChoice = 1 == Configuration::get('PS_CREAZIONE_FATTURA'); // selezione fattura elettronica
        $disReceipt = 1 == Configuration::get('PS_DISABILITA_RICEVUTE'); // selezione casella di controllo
        // creo una fattura elettronica in una di queste due condizioni
        $fattEl = $fattElChoice && !empty($billing_data->vat_number) || $disReceipt ? true : false;

        $customer_fePec = $domtree->createElement('FeCustomerPec');
        $customer_fePec->appendChild($domtree->createCDataSection($pec));
        $fattura_invoice->appendChild($customer_fePec);

        $customer_feCodiceDestinatario = $domtree->createElement('FeDestinationCode');
        $customer_feCodiceDestinatario->appendChild($domtree->createCDataSection($sdi_code));
        $fattura_invoice->appendChild($customer_feCodiceDestinatario);

        $company = empty($customer->company) ? trim($billing_data->company) : trim($customer->company);

        if (!empty($company)) {
            $customername = $company;
        } else {
            $customername = $billing_data->firstname . ' ' . $billing_data->lastname;
        }

        $customer_name = $domtree->createElement('CustomerName');
        $customer_name->appendChild($domtree->createCDataSection($customername));
        $fattura_invoice->appendChild($customer_name);
        $customer_address = $domtree->createElement('CustomerAddress');
        $customer_address2 = null == $billing_data->address2 ? '' : ' ' . $billing_data->address2;
        $customer_address->appendChild($domtree->createCDataSection($billing_data->address1 . $customer_address2));
        $fattura_invoice->appendChild($customer_address);
        $fattura_invoice->appendChild($domtree->createElement('CustomerPostcode', (string) $billing_data->postcode));
        $customer_name = $domtree->createElement('CustomerCity');
        $customer_name->appendChild($domtree->createCDataSection($billing_data->city));
        $fattura_invoice->appendChild($customer_name);
        if (!empty($billing_data->id_state)) {
            $state = new State($billing_data->id_state);
            $fattura_invoice->appendChild($domtree->createElement('CustomerProvince', (string) $state->iso_code));
        } else {
            $fattura_invoice->appendChild($domtree->createElement('CustomerProvince', ''));
        }

        $isoCountry = Country::getIsoById($billing_data->id_country);
        $fattura_invoice->appendChild(
            $domtree->createElement(
                'CustomerCountry',
                Country::getIsoById($billing_data->id_country)
            )
        );

        // codice fiscale
        if (!empty($billing_data->dni)) {
            $customer_fiscalcode = $domtree->createElement('CustomerFiscalCode');
            $customer_fiscalcode->appendChild($domtree->createCDataSection(Tools::strtoupper($billing_data->dni)));
            $fattura_invoice->appendChild($customer_fiscalcode);
        }

        // partita iva
        if (!empty($billing_data->vat_number)) {
            $customerVatNumber = $this->cleanVatNumber($isoCountry, $billing_data->vat_number);
            $customer_vatcode = $domtree->createElement('CustomerVatCode');
            $customer_vatcode->appendChild($domtree->createCDataSection($customerVatNumber));
            $fattura_invoice->appendChild($customer_vatcode);
        } elseif ('IT' !== $isoCountry && empty($billing_data->vat_number) && $fattEl) {
            $customer_vatcode = $domtree->createElement('CustomerVatCode');
            $customer_vatcode->appendChild($domtree->createCDataSection($customername));
            $fattura_invoice->appendChild($customer_vatcode);
        }

        if (!empty($billing_data->phone) || !empty($billing_data->phone_mobile)) {
            $customer_cell_phone = $domtree->createElement('CustomerCellPhone');

            if (!empty($billing_data->phone)) {
                $customer_cell_phone->appendChild($domtree->createCDataSection($billing_data->phone));
            } elseif (!empty($billing_data->phone_mobile)) {
                $customer_cell_phone->appendChild($domtree->createCDataSection($billing_data->phone_mobile));
            }

            $fattura_invoice->appendChild($customer_cell_phone);
        }

        $customer_email = $domtree->createElement('CustomerEmail');
        $customer_email->appendChild($domtree->createCDataSection($customer->email));
        $fattura_invoice->appendChild($customer_email);

        if (!$order->isVirtual() && null != $order->id_address_delivery) {
            $delivery_address = new Address($order->id_address_delivery);
            if (!empty($delivery_address->company)) {
                $customerDeliveryName = $delivery_address->company;
            } else {
                $customerDeliveryName = $delivery_address->firstname . ' ' . $delivery_address->lastname;
            }
            $delivery_name = $domtree->createElement('DeliveryName');
            $delivery_name->appendChild($domtree->createCDataSection($customerDeliveryName));
            $fattura_invoice->appendChild($delivery_name);
            $address = $domtree->createElement('DeliveryAddress');
            $delivery_address2 = null == $delivery_address->address2 ? '' : ' ' . $delivery_address->address2;
            $address->appendChild($domtree->createCDataSection($delivery_address->address1 . $delivery_address2));
            $fattura_invoice->appendChild($address);
            $fattura_invoice->appendChild($domtree->createElement('DeliveryPostcode', (string) $delivery_address->postcode));
            $fattura_invoice->appendChild($domtree->createElement('DeliveryCity', (string) $delivery_address->city));
            if (!empty($delivery_address->id_state)) {
                $state = new State($delivery_address->id_state); // deve cercare l'indirizzo nel delivery_address
                $fattura_invoice->appendChild($domtree->createElement('DeliveryProvince', (string) $state->iso_code));
            }
            $delivery_country = Country::getIsoById($delivery_address->id_country);
            $fattura_invoice->appendChild($domtree->createElement('DeliveryCountry', (string) $delivery_country));
        }

        $TotalWithoutTax = number_format((float) $order->total_paid_tax_excl, 2, '.', '');
        $fattura_invoice->appendChild($domtree->createElement('TotalWithoutTax', (string) $TotalWithoutTax));
        $PS_payment_method = (object) Module::getInstanceByName($order->module);
        $payment_method_name = $domtree->createElement('PaymentMethodName');
        $orderPayment = isset($PS_payment_method->displayName) ? $PS_payment_method->displayName : '';
        $orderTotDiscountTaxExcl = (float) $order->total_discounts_tax_excl;
        $totShippingTaxExcl = $this->getTotShippingTaxExcl($order);
        $shippingRate = (float) number_format((float) $order->carrier_tax_rate, 0, '.', '');
        // $this->trace('tot sconto tasse escluse nell\'ordine :', $orderTotDiscountTaxExcl);

        /*
         * Con $payment_method_needle gestisco i pagamenti elettronici e lo stato Pagato della fattura
         * in conformità con la configurazione del modulo F24 - Davide Iandoli 20.01.2020
         */
        if (!empty($orderPayment) && false !== strpos(Tools::strtolower($orderPayment), 'paypal')) {
            $payment_method_needle = 'paypal';
        } elseif (false !== strpos(Tools::strtolower($orderPayment), 'amzpayments')) {
            $payment_method_needle = 'amazon payments';
        } elseif (false !== strpos(Tools::strtolower($orderPayment), 'braintree')) {
            $payment_method_needle = 'braintree';
        } elseif (false !== strpos(Tools::strtolower($orderPayment), 'payplug')) {
            $payment_method_needle = 'payplug';
        } elseif (false !== strpos(Tools::strtolower($orderPayment), 'maofreepostepay')) {
            $payment_method_needle = 'postepay';
        } elseif (false !== strpos(Tools::strtolower($orderPayment), 'stripe')) {
            $payment_method_needle = 'stripe';
        } elseif (false !== strpos(Tools::strtolower($orderPayment), 'payment')) {
            $payment_method_needle = 'pagamento smart o con carta';
        } elseif (false !== strpos(Tools::strtolower($orderPayment), 'wallet')) {
            $payment_method_needle = 'pagamento smart o con carta';
        } elseif (false !== strpos(Tools::strtolower($orderPayment), 'pay')) {
            $payment_method_needle = 'pagamento smart o con carta';
        } elseif (false !== strpos(Tools::strtolower($orderPayment), 'prestashop checkout')) {
            $payment_method_needle = 'pagamento smart o con carta';
        } else {
            $payment_method_needle = '';
        }

        $modulePayment = isset($PS_payment_method->name) ? $PS_payment_method->name : '';

        // codici MP per fattura elettronica
        if (false !== strpos($modulePayment, 'wire')) { // ps 1.7 => 'ps_wirepayment'; ps 1.6 => 'bankwire';
            $FePaymentCode = 'MP05';
            $bank_name = $PS_payment_method->owner;
            $payment_method_name = $domtree->createElement('PaymentMethodName');
            $payment_method_name->appendChild($domtree->createCDataSection((string) $bank_name));
            $fattura_invoice->appendChild($payment_method_name); // aggiungo il tag xml alla fattura
            $bank_detail = $PS_payment_method->details; // iban
            $payment_method_description = $domtree->createElement('PaymentMethodDescription');
            $payment_method_description->appendChild($domtree->createCDataSection((string) $bank_detail));
            $fattura_invoice->appendChild($payment_method_description); // aggiungo il tag xml alla fattura
        } elseif ('ps_checkpayment' == $modulePayment) { // Aggiunto pagamento con assegno
            $FePaymentCode = 'MP02';
            $payment_method_name->appendChild($domtree->createCDataSection($orderPayment));
            $fattura_invoice->appendChild($payment_method_name);
            $payment_method_description = $domtree->createElement('PaymentMethodDescription');
            $payment_method_description->appendChild($domtree->createCDataSection('Pagamento con assegno'));
            $fattura_invoice->appendChild($payment_method_description);
        } elseif ('ps_cashondelivery' == $modulePayment) {
            $FePaymentCode = 'MP01';
            $payment_method_name->appendChild($domtree->createCDataSection($orderPayment));
            $fattura_invoice->appendChild($payment_method_name);
            $payment_method_description = $domtree->createElement('PaymentMethodDescription');
            $payment_method_description->appendChild($domtree->createCDataSection('Pagamento alla consegna'));
            $fattura_invoice->appendChild($payment_method_description);
        } else {
            $uc_modulePayment = Tools::ucfirst($modulePayment);

            if (empty($payment_method_needle)) {
                $payment_detail = 'Pagamento smart o con carta';
            } else {
                $payment_detail = Tools::ucfirst($payment_method_needle);
            }

            $FePaymentCode = 'MP08';
            $payment_method_name->appendChild($domtree->createCDataSection($uc_modulePayment));
            $fattura_invoice->appendChild($payment_method_name);
            $payment_method_description = $domtree->createElement('PaymentMethodDescription');
            $payment_method_description->appendChild($domtree->createCDataSection($payment_detail));
            $fattura_invoice->appendChild($payment_method_description);
        }

        $fattura_invoice->appendChild($payment_method_description); // aggiungo il tag sempre
        if ($isInvoice) {
            $fattura_invoice->appendChild($domtree->createElement('FePaymentCode', (string) $FePaymentCode));
        }
        // fine blocco per codici MP

        $Total = number_format((float) $order->total_paid, 2, '.', '');
        $vatAmount = $order->total_paid_tax_incl - $order->total_paid_tax_excl;
        $fattura_invoice->appendChild($domtree->createElement('VatAmount', (string) $vatAmount));
        $fattura_invoice->appendChild($domtree->createElement('Total', (string) $Total));

        // se è fattura aggiungo pure il totale pagato, la data di pagamento etc.
        if ($isInvoice) {
            $payments = $fattura_invoice->appendChild($domtree->createElement('Payments'));
            $payment = $payments->appendChild($domtree->createElement('Payment'));
            $payment->appendChild($domtree->createElement('Date', $this->now('Y-m-d')));
            $Amount = number_format((float) $order->total_paid, 2, '.', ''); // arrotondamento a 2 decimali
            $payment->appendChild($domtree->createElement('Amount', (string) $Amount));
            
            if (1 == Configuration::get('PS_STATO_PAGATO') || 0.00 == (float) $order->total_paid) {
                $payment->appendChild($domtree->createElement('Paid', 'true'));
            } elseif (2 == Configuration::get('PS_STATO_PAGATO')) {
                $modulePayment = $PS_payment_method->name;
                $bool_invoice_paid_status = !empty($payment_method_needle) ? 'true' : 'false';
                $payment->appendchild($domtree->createElement('Paid', (string) $bool_invoice_paid_status));
            } else {
                $payment->appendChild($domtree->createElement('Paid', 'false'));
            }
        }

        /**
         * Il carrello mi servirà per controllare le restrizioni
         * nell'applicazione dello sconto.
         */
        $cart = new Cart($order->id_cart);
        $f24CartRules = []; // array di regole carrello
        $giftProductArray = []; // array di prodotti omaggio
        $giftProductList = [];
        $cartRuleName = '';
        $couponArray = $order->getCartRules();
        $nCoupons = count($couponArray);

        /*
         * Qui creo un array personalizzato contenente tutti i codici sconto utilizzati
         * le chiavi reduction_percent e reduction_amount a loro volta contengono un array di info
         * che mi serviranno nelle righe prodotto
         */
        foreach ($couponArray as $cart_rule) {
            // $this->trace(' cart rule :', $cart_rule);
            $cartRuleName = $cart_rule['name'];
            $cartRuleDetails = new CartRule($cart_rule['id_cart_rule']);
            // $this->trace('cartRuleDetails :', $cartRuleDetails);
            $free_shipping = 1 == (int) $cartRuleDetails->free_shipping;
            $giftProductId = (int) $cartRuleDetails->gift_product;
            $productRestrictionsArray = [];
            $productRestrictionsId = '';
            $reductionPercent = 0;
            $reductionAmount = (float) $cartRuleDetails->reduction_amount;
            $isAmountTaxExcl = 0 == (int) $cartRuleDetails->reduction_tax;
            $valueTaxExcl = (float) $cart_rule['value_tax_excl'];
            $reductionProduct = (int) $cartRuleDetails->reduction_product;

            if (0 !== $giftProductId) {
                $giftProductList[] = $giftProductId;
            }

            $type = 'none';
            if (
                (float) $cartRuleDetails->reduction_percent > 0
                && (0 == $reductionProduct || -2 == $reductionProduct)
            ) {
                $type = 'percent';
                $reductionPercent = (float) $cartRuleDetails->reduction_percent;
            } elseif ((float) $cartRuleDetails->reduction_percent > 0 && -1 == $reductionProduct) {
                $type = 'percent_to_cheapest';
                $reductionPercent = (float) $cartRuleDetails->reduction_percent;
            } elseif ((float) $cartRuleDetails->reduction_percent > 0 && $reductionProduct > 0) {
                $type = 'percent_to_product';
                $reductionPercent = (float) $cartRuleDetails->reduction_percent;
            } elseif ((float) $cartRuleDetails->reduction_amount > 0) {
                $type = 'fixed';
            }

            // se lo sconto è fisso devo sottrarre il prezzo del prod omaggio

            // se necessario sottraggo l'importo della spedizione
            if ($free_shipping) {
                $valueTaxExcl -= $totShippingTaxExcl;
            }

            if (is_callable([$cartRuleDetails, 'checkProductRestrictionsFromCart'])) {
                $productRestrictionsArray = $cartRuleDetails->checkProductRestrictionsFromCart($cart, true, true, true);
                // $this->trace('elenco restrizioni :', $productRestrictionsArray);
                /**
                 * Copia dell'array contenente l'id del prodotto, usato per controllare l'elenco dei prodotti
                 * a cui va aggiunto lo sconto.
                 */
                $productRestrictionsId = array_map([$this, 'getParamSubstring'], $productRestrictionsArray);
            }

            $f24CartRules[] = [
                'name' => $cartRuleName,
                'type' => $type,
                'free_shipping' => $free_shipping,
                'gift_product' => $giftProductId,
                'product_ids' => $productRestrictionsId,
                'reduction_product' => $reductionProduct,
                'reduction_amount' => $reductionAmount,
                'is_amount_tax_excl' => $isAmountTaxExcl,
                'reduction_percent' => $reductionPercent,
                'value_tax_excl' => $valueTaxExcl,
            ];
        }
        // $this->trace('f24 cart rules :', $f24CartRules);

        $free_shipping = in_array(true, array_column($f24CartRules, 'free_shipping'));

        // sottraggo il totale spedizione dal totale sconto
        if ($free_shipping) {
            $orderTotDiscountTaxExcl -= $totShippingTaxExcl;
        }

        $products = $fattura_invoice->appendChild($domtree->createElement('Rows'));

        // ciclo prodotti nell'ordine
        $productArray = $order->getProducts();
        $productList = [];
        $itemTax = [];
        $cartDiscount = []; // array di sconti su tutti i prodotti del carrello
        $orderTaxes = $order->getOrderDetailTaxes();

        foreach ($productArray as $product_detail) {
            $this->trace('item details :', $product_detail);

            $idPdc = Configuration::get('PS_F24_PDC');
            if (!empty($idPdc) && 'Nessun Pdc' != $idPdc) {
                $pdcSet = true;
            }

            $productId = (int) $product_detail['product_id'];
            $qty = (int) $product_detail['product_quantity'];
            $productDescription = $product_detail['product_name'];
            $price = (float) $product_detail['unit_price_tax_excl'];
            $discountAmount = 0;
            $isGift = false;
            $giftPrice = 0;
            $productCode = $product_detail['product_reference'];

            foreach ($giftProductList as $item) {
                if ($productId == $item) {
                    --$qty;
                    $orderTotDiscountTaxExcl -= $price;
                    $giftProductArray[] = $product_detail;
                    $giftPrice += $price;
                    $isGift = true;
                }
            }

            $idTaxRule = (int) $product_detail['id_tax_rules_group'];
            $idOrderDetail = (int) $product_detail['id_order_detail'];
            $taxId = $this->getTaxId($orderTaxes, $idTaxRule, $idOrderDetail);
            $taxRateFromDetail = (float) $product_detail['tax_rate'];
            $taxRateFromId = $this->getTaxRateFromId($taxId);
            $taxRate = $taxRateFromDetail == $taxRateFromId ? $taxRateFromDetail : $taxRateFromId;
            $taxNameFromId = $this->getTaxNameFromId($taxId, $taxRate);
            $natura = '';

            /*
             * Controllo solo il rate per agganciare l'aliquota IVA e non crearne una nuova
             * anche nel caso in cui il cliente voglia creare solo un Ordine
             * In questo modo potrà trasformarlo in FE senza un errore di mancata indicazione natura
             */
            if (0 == (float) $taxRate) {
                $natura = $this->getNaturaByIdTax((string) $idTaxRule);
            }

            $taxName = $product_detail['tax_calculator']->getTaxesName();
            // $VatDescription = !empty($taxName) ? $taxName : $this->getDefaultVatDescription($taxRate);
            $VatDescription = !empty($taxName) ? $taxName : $taxNameFromId;
            
            // ciclo i coupon nel carrello
            foreach ($f24CartRules as $rule) {
                $isAmountTaxExcl = $rule['is_amount_tax_excl'];
                $reductionProduct = $rule['reduction_product'];

                if ($free_shipping) {
                    $rule['value_tax_excl'] -= $totShippingTaxExcl;
                }

                /*
                * Verifico se questo prodotto è incluso nel codice sconto,
                * lo è se sia se l'id del prodotto è nella lista degli id prodotto elencati nel codice
                * sia se la lista degli id prodotto è vuota (i.e.: lo sconto si applica a tutti i prodotti nel carrello)
                */
                $productIncluded = in_array((int) $productId, $rule['product_ids'])
                    || empty($rule['product_ids']);
                /*
                 * Se ci sono più sconti in percentuale devono essere
                 * applicati 'a cascata', quindi devo sottrarre al prezzo
                 * del prodotto l'importo dello sconto precedente
                 * si noti che se c'è uno sconto solo discountAmount è
                 * uguale a zero
                 *
                 * Se lo sconto è percentuale non devo tener conto del prezzo del prodotto
                 * omaggio, perché ho già cambiato la quantità
                 */
                if ('percent' == $rule['type'] && $productIncluded) {
                    $newPrice = ($price * $qty) - $discountAmount;
                    $discountAmount += (float) number_format($newPrice / 100
                        * (float) $rule['reduction_percent'], 8, '.', '');
                } elseif ('percent_to_product' == $rule['type'] && $productId == $reductionProduct) {
                    $discountAmount += (float) number_format($price * $qty / 100
                        * (float) $rule['reduction_percent'], 8, '.', '');
                }
                // $this->trace('discount amount 1 :', $discountAmount);
            }

            /**
             * Qui aggiungo gli sconti fissi ristretti al prodotto
             * cui devo sottrarre il prezzo del prodotto omaggio.
             */
            $productReductionAmount = $this->f24getRedAmount($productId, $f24CartRules);
            $discountAmount += $productReductionAmount;
            // $this->trace('discount amount 2 :', $discountAmount);

            $productList[] = [
                'id' => $productId,
                'code' => $productCode,
                'description' => $productDescription,
                'price' => $price,
                'qty' => $qty,
                'is_gift' => $isGift,
                'tax_rate' => $taxRate,
                'tax_description' => $VatDescription,
                'natura' => $natura,
                'disc_amount' => $discountAmount,
            ];

            // raggruppo gli sconti per aliquota IVA
            if (!array_key_exists((int) $taxRate, $itemTax) && $discountAmount > 0) {
                $itemTax[$taxRate] = [
                    'vat_description' => $VatDescription,
                    'natura' => $natura,
                    'total_price_per_vat_rate' => $price * $qty,
                    'total_discount_per_vat_rate' => $discountAmount,
                ];
            } elseif ($discountAmount > 0) {
                $itemTax[$taxRate]['total_price_per_vat_rate'] += $price * $qty;
                $itemTax[$taxRate]['total_discount_per_vat_rate'] += $discountAmount;
            }
            // $this->trace('item tax 1 :', $itemTax);

            // sottraggo gli sconti già calcolati;
            // sconti che si applicano a tutti i prodotti nel carrello
            if ($orderTotDiscountTaxExcl > 0) {
                if (!array_key_exists((int) $taxRate, $cartDiscount)) {
                    $cartDiscount[$taxRate] = [
                        'vat_description' => $VatDescription,
                        'natura' => $natura,
                        'total_price_per_vat_rate' => $price * $qty,
                        'total_discount_per_vat_rate' => $discountAmount,
                    ];
                } else {
                    $cartDiscount[$taxRate]['total_price_per_vat_rate'] += $price * $qty;
                }
            }
            // $this->trace('Cart Discount :', $cartDiscount);
        }

        // aggiungo le righe prodotto all'xml
        $this->addProductRowsToXml($domtree, $products, $productList);

        //aggiungo i prodotti omaggio
        if (!empty($giftProductArray)) {
            $this->addGiftProductToXml($domtree, $products, $giftProductArray, $f24CartRules, $orderTaxes);
        }

        if ($nCoupons > 0) {
            $itemTax = $this->updateItemTax(
                $itemTax,
                $cartDiscount,
                $orderTotDiscountTaxExcl
            );
        }

        // aggiungo una riga prodotto negativa raggruppata per aliquota IVA
        if (!empty($itemTax)) {
            $this->addDiscProduct($domtree, $products, $itemTax, $f24CartRules);
        }
        /*
         * Gestione riga di spedizione, al termine delle righe prodotto
         */
        if ($order->total_shipping_tax_excl >= 0) {
            $cartRuleName = implode("\n", array_column($f24CartRules, 'name'));
            $pdcSet = false;
            $idPdc = Configuration::get('PS_F24_PDC');
            if (!empty($idPdc) && 'Nessun Pdc' != $idPdc) {
                $pdcSet = true;
            }

            $idCarrier = $order->id_carrier == '0' ? '1' : $order->id_carrier;
            $sqlRow = 'SELECT `name` FROM `' . _DB_PREFIX_ . "carrier` 
                       WHERE `id_carrier` = '" . pSQL($idCarrier) . "'";
            $shipping_company = Db::getInstance()->getRow($sqlRow);

            $billing_data = new Address($order->id_address_invoice);
            $carrier = new Carrier($order->id_carrier);
            $VatDescription = $carrier->getTaxCalculator($billing_data)->getTaxesName();

            $shippingLabel = $this->l('Shipping amount :');
            $shippingDescription = !$free_shipping ? $shippingLabel . ' ' . $shipping_company['name'] :
                $shippingLabel . ' ' . $shipping_company['name'] . ' - ' . $cartRuleName;

            $product = $products->appendChild($domtree->createElement('Row'));
            $description = $domtree->createElement('Description');

            $description->appendChild($domtree->createCDataSection($shippingDescription));
            $product->appendChild($description);
            $product->appendChild($domtree->createElement('Qty', '1'));
            $product->appendChild($domtree->createElement('Price', (string) $totShippingTaxExcl));
            if ($free_shipping) {
                $product->appendChild($domtree->createElement('Discounts', '100'));
            }

            $product->appendChild($domtree->createElement('VatCode', (string) $shippingRate));
            $naturaSpedizione = Configuration::get('PS_CONF_NATURA_SPEDIZIONE');
            $lista = getListaNatureNew();
            $needle = Tools::substr($naturaSpedizione, 0, 2);

            /*
             * Con questo blocco gestisco i casi in cui l'aliquota IVA
             * per la spedizione è 0%, inserendo la Natura
             */
            if (0 == (int) $shippingRate) {
                $naturaSpedizione = Configuration::get('PS_CONF_NATURA_SPEDIZIONE');

                $lista = getListaNatureNew(); // uso il nuovo metodo
                $needle = Tools::substr($naturaSpedizione, 0, 2);

                if ('N0' == $naturaSpedizione) {
                    $naturaSpedizione = '';
                    $VatDescription = 'IVA';
                } elseif (is_array($lista[$needle])) {
                    $VatDescription = $lista[$needle][$naturaSpedizione];
                } else {
                    $VatDescription = $lista[$naturaSpedizione];
                }
            }

            $product->appendChild($domtree->createElement('VatDescription', (string) $VatDescription));

            if (0 == $shippingRate) {
                $product->appendChild($domtree->createElement('FeVatNature', (string) $naturaSpedizione));
            }
            if ($pdcSet) {
                $product->appendChild($domtree->createElement('IdPdc', (string) $idPdc));
            }
        }

        // tipo di documento creato nel gestionale Fattura24, Causale, numeratore utilizzato
        if ($isInvoice) {
            $footnotes = $domtree->createElement('FootNotes');
            $footnotes->appendChild($domtree->CreateCDataSection($this->l('Order ref. : ') . $order->reference));
            $fattura_invoice->appendChild($footnotes);
            $f24_inv_object = Configuration::get('PS_INV_OBJECT');
            $f24_search = ['(N)', '(R)'];
            $f24_replace = [$order->id, $order->reference];
            $f24_send_object = str_replace($f24_search, $f24_replace, $f24_inv_object);
            $f24_xml_object = $domtree->createElement('Object');
            $f24_xml_object->appendChild($domtree->createCDataSection($f24_send_object));
            $fattura_invoice->appendChild($f24_xml_object);
            $documentType = '';

            if (empty($billing_data->vat_number)) {
                if (3 == Configuration::get('PS_CREAZIONE_FATTURA')) {
                    $documentType = 'R';
                }
                if (2 == Configuration::get('PS_CREAZIONE_FATTURA')) {
                    $documentType = 1 == Configuration::get('PS_DISABILITA_RICEVUTE') ? 'I-force' : 'R';
                }
                if (1 == Configuration::get('PS_CREAZIONE_FATTURA')) {
                    $documentType = 1 == Configuration::get('PS_DISABILITA_RICEVUTE') ? 'FE' : 'R'; // create fattura
                }
            } else {
                if (3 == Configuration::get('PS_CREAZIONE_FATTURA')) {
                    $documentType = 'R';
                }
                if (2 == Configuration::get('PS_CREAZIONE_FATTURA')) {
                    $documentType = 'I';
                }
                if (1 == Configuration::get('PS_CREAZIONE_FATTURA')) {
                    $documentType = 'FE';
                }
            }
            // se il cliente non ha una partita iva uso il sezionale ricevuta
            if ('R' == $documentType) { // use receipts issue number bugfix 30.01.2020
                $idNumerator = Configuration::get('PS_F24_SEZIONALE_RICEVUTA');
            } else { // uso il sezionale fattura
                $idNumerator = Configuration::get('PS_F24_SEZIONALE_FATTURA');
            }

            if ('Predefinito' != $idNumerator) {
                $fattura_invoice->appendChild($domtree->createElement('IdNumerator', (string) $idNumerator));
            }

            $query = 'SELECT docIdOrderFattura24 FROM ' . _DB_PREFIX_ . 'orders WHERE id_order=\'' .
                (int) $order_id . '\';';
            $docIdOrderFattura24 = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($query)['docIdOrderFattura24'];
            if (null != $docIdOrderFattura24) {
                $fattura_invoice->appendChild($domtree->createElement('F24OrderId', (string) $docIdOrderFattura24));
            }
            $sendEmail = 1 == Configuration::get('PS_EMAIL_FATTURA') ? 'true' : 'false';
            // $updateStorage = Configuration::get('PS_F24_MAGAZZINO_FATTURA');

            if ($order->isVirtual()) {
                $idTemplate = Configuration::get('PS_F24_MODELLO_FATTURA');
            } else {
                $idTemplate = Configuration::get('PS_F24_MODELLO_FATTURA_DEST');
            }
        } else {
            $orderNumberOption = Configuration::get('PS_NUMERO_ORDINE');
            $f24OrderNumber = 0;
            if (0 == $orderNumberOption) {
                $f24OrderNumber = $order->id;
            } elseif (1 == $orderNumberOption) {
                $f24OrderNumber = $order->reference;
            } elseif (2 == $orderNumberOption) {
                $f24OrderNumber = $order->id . ' - ' . $order->reference;
            }

            $fattura_invoice->appendChild($domtree->createElement('Number', (string) $f24OrderNumber));

            $documentType = 'C';
            $sendEmail = 1 == Configuration::get('PS_MAIL_ORDINE') ? 'true' : 'false';
            // $updateStorage = Configuration::get('PS_F24_MAGAZZINO_ORDINE');
            if ($order->isVirtual()) {
                $idTemplate = Configuration::get('PS_F24_MODELLO_ORDINE');
            } else {
                $idTemplate = Configuration::get('PS_F24_MODELLO_ORDINE_DEST');
            }
        }
        $fattura_invoice->appendChild($domtree->createElement('DocumentType', (string) $documentType));

        /**
         * Con questo blocco riporto il bollo virtuale per le FE
         * e scrivo il tag solo se il valore è "V"
         * tre condizioni: opzione selezionata + tipo documento + totale > 77.47
         * Davide Iandoli 16.04.2020.
         */
        $bollo_option = Configuration::get('PS_F24_BOLLO_VIRTUALE_FE');
        if (1 == $bollo_option && 'FE' == $documentType && $Total > 77.47) {
            $bollo_fe = 'V';
            $fattura_invoice->appendChild($domtree->createElement('FeVirtualStamp', (string) $bollo_fe));
        } else {
            $bollo_fe = 'N';
        }

        $fattura_invoice->appendChild($domtree->createElement('SendEmail', (string) $sendEmail));

        if (!empty($idTemplate) && 'Predefinito' !== $idTemplate) {
            $fattura_invoice->appendChild($domtree->createElement('IdTemplate', (string) $idTemplate));
        }

        return $domtree->saveXML();
    }

    // download documento da F24
    public function downloadDocument($docIdFattura24, $orderId, $isInvoice)
    {
        $fattura24_api_url = $this->baseUrl . 'GetFile';
        $command = $this->getApiCommand($fattura24_api_url); // ottengo il comando
        $start_time = $this->startTime();
        $send_data = [];
        $send_data['apiKey'] = Configuration::get('PS_FATTURA24_API');
        $send_data['source'] = $this->sourceInfo;
        $send_data['docId'] = (int) $docIdFattura24;
        $dataReturned = $this->curlDownload($fattura24_api_url, http_build_query($send_data))['output'];
        $finish_time = $this->endTime();
        $file = '';
        $this->getApiTime($command, $start_time, $finish_time);
        libxml_use_internal_errors(true);

        if (!simplexml_load_string(mb_convert_encoding((string) $dataReturned, 'UTF-8', mb_list_encodings()))) {
            if ($isInvoice) {
                $order = new Order((int) $orderId);
                // $this->trace('invoice date :', $order->date_upd);
                // la proprietà date_upd corrisponde all'ultimo aggiornamento dell'ordine
                $fileName = $this->constructFileName($order->id, $order->reference);
            } else {
                $fileName = 'ORD' . sprintf('%06d', (int) $orderId) . '.pdf';
            }
            $file = file_put_contents(_PS_DOWNLOAD_DIR_ . 'Fattura24/' . $fileName, $dataReturned);
        }

        return $file;
    }

    // salva contatto in F24
    public function saveCustomer($check_order)
    {
        $fattura24_api_url = $this->baseUrl . 'SaveCustomer';
        $command = $this->getApiCommand($fattura24_api_url); // ottengo il comando
        $start_time = $this->startTime();
        $send_data = [];
        $send_data['apiKey'] = Configuration::get('PS_FATTURA24_API');
        $send_data['source'] = $this->sourceInfo;
        $send_data['xml'] = $this->createCustomerXml($check_order);
        $dataReturned = $this->curlDownload($fattura24_api_url, http_build_query($send_data))['output'];
        $finish_time = $this->endTime();
        $this->getApiTime($command, $start_time, $finish_time);

        $response = new DOMDocument();
        $response->preserveWhiteSpace = false;
        $response->formatOutput = true;
        $response->loadXML($dataReturned);
        $response_string = $response->saveXML();
        $this->trace($send_data['xml'], $response_string);
    }

    /**
     * Avvio il cronometro per la durata della chiamata
     */
    public function startTime()
    {
        if (Tools::substr($this->phpversion, 0, 3) < '7.3') {
            $start_time = microtime(true);
        } else {
            $start_time = hrtime(true);
        }

        return $start_time;
    }

     /**
     * Fermo il cronometro per la durata della chiamata
     */
    public function endTime()
    {
        if (Tools::substr($this->phpversion, 0, 3) < '7.3') {
            $finish_time = microtime(true);
        } else {
            $finish_time = hrtime(true);
        }

        return $finish_time;
    }

    /**
     * Ottengo il comando inviato alle API di Fattura24
     */
    public function getApiCommand($command)
    {
        $api_command_array = explode('/', $command);

        return $api_command_array[5];
    }

    /**
     * Tempo di risposta della chiamata API
     */
    public function getApiTime($command, $start_time, $finish_time)
    {
        // formule per convertire in millisecondi
        if (Tools::substr($this->phpversion, 0, 3) < '7.3') {
            $duration = ($finish_time - $start_time) * 1000;
        } else {
            $duration = round(($finish_time - $start_time) / 10000000);
        }
        $result = number_format((float) $duration, 0, '.', '');
        $response_time = ['chiamata :' => $command, 'tempo di risposta in millisecondi :' => $result];
        $this->trace('controllo risposte :', $response_time);
    }

    /*
     * Funzione comune da eseguire dopo gli hook dichiarati sopra,
     * il parametro isInvoice serve per distinguere se deve essere creato un ordine o una fattura
     * innesca la chiamata SaveDocument alle API di Fattura24
     */
    public function afterHook($order, $isInvoice)
    {
        $orderId = isset($order->id) ? $order->id : (int) Order::getIdByCartId($order->id_cart);
        $idShop = (int) $order->id_shop;
        $fattura24_api_url = $this->baseUrl . 'SaveDocument';
        $command = $this->getApiCommand($fattura24_api_url); // ottengo il comando
        $start_time = $this->startTime();

        $send_data = [];
        $send_data['apiKey'] = Configuration::get('PS_FATTURA24_API');
        $send_data['source'] = $this->sourceInfo;
        $send_data['xml'] = $this->createInvoiceXml($orderId, $isInvoice);
        
        $debugEnabled = '1' == Configuration::get('PS_F24_DEBUG_MODE');

        if (!$isInvoice) {
            $idRequest = $debugEnabled ? '' : 'C' . $orderId . '_' . $idShop;
        } else {
            $xml = simplexml_load_string($send_data['xml']);
            $docType = '';
            if (is_object($xml)) {
                $docType = $xml->Document->DocumentType;
            }
            $idRequest = $debugEnabled ? '' : $docType . $orderId . '_' . $idShop;
        }

        $send_data['idRequest'] = $idRequest;
        $dataReturned = $this->curlDownload($fattura24_api_url, http_build_query($send_data))['output'];
        $finish_time = $this->endTime();
        $this->getApiTime($command, $start_time, $finish_time);
        $obj_dataReturned = simplexml_load_string(mb_convert_encoding((string) $dataReturned, 'UTF8',
            mb_list_encodings()), 'SimpleXMLElement', LIBXML_NOCDATA);
        $json_dataReturned = json_encode($obj_dataReturned);
        $downloadPdfOrder = 1 == (int) Configuration::get('PS_PDF_ORDINE');
        $downloadPdfInvoice = 1 == (int) Configuration::get('PS_PDF_FATTURA');

        if (!$isInvoice) {
            $column = 'apiResponseOrder';
            if (is_object($obj_dataReturned)) {
                $docIdOrderFattura24 = $obj_dataReturned->docId;
            } else {
                $docIdOrderFattura24 = explode('</docId>', explode('<docId>', $dataReturned)[1])[0];
            }

            $query = 'UPDATE ' . _DB_PREFIX_ . 'orders SET docIdOrderFattura24=\'' . pSQL((int) $docIdOrderFattura24);
            $query .= '\' WHERE id_order=\'' . (int) $orderId . '\';';
            Db::getInstance()->Execute($query);
            if ($downloadPdfOrder) {
                $this->downloadDocument($docIdOrderFattura24, $orderId, false);
            }
        } else {
            $column = 'apiResponseInvoice';
            if (is_object($obj_dataReturned)) {
                $docIdFattura24 = $obj_dataReturned->docId;
            } else {
                // $this->trace('data returned :', $dataReturned);
                $docIdFattura24 = explode('</docId>', explode('<docId>', $dataReturned)[1])[0];
            }

            $query = 'UPDATE ' . _DB_PREFIX_ . 'orders SET docIdFattura24=\'' . pSQL((int) $docIdFattura24);
            $query .= '\' WHERE id_order=\'' . (int) $orderId . '\';';
            $result = Db::getInstance()->Execute($query);
            // $this->trace('doc id fattura24 :', $docIdFattura24);
            if ($downloadPdfInvoice) {
                $this->downloadDocument($docIdFattura24, $orderId, true);
            }
        }

        /**
         *  Con questo blocco gestisco i casi in cui le colonne apiResponseOrder
         *  e apiResponseInvoice NON sono state create
         *  Davide Iandoli 19.08.2020.
         */
        $sql = 'SELECT apiResponseOrder, apiResponseInvoice from ' . _DB_PREFIX_ . 'orders';
        $result = Db::getInstance()->ExecuteS($sql);

        if ($result) {
            if (is_object($obj_dataReturned)) {
                $query = 'UPDATE ' . _DB_PREFIX_ . 'orders SET ' . $column . '=\'' . pSQL((string) $json_dataReturned);
                $query .= '\' WHERE id_order=\'' . (int) $orderId . '\';';
                Db::getInstance()->Execute($query);
            } else {
                $query = 'UPDATE ' . _DB_PREFIX_ . 'orders SET ' . $column . '=\'' .
                    pSQL((string) $dataReturned);
                $query .= '\' WHERE id_order=\'' . (int) $orderId . '\';';
                Db::getInstance()->Execute($query);
            }
        }
        // fine blocco fix

        $response = new DOMDocument();
        $response->preserveWhiteSpace = false;
        $response->formatOutput = true;
        $response->loadXML($dataReturned);
        $response_string = $response->saveXML();
        $this->trace($send_data['xml'], $response_string);
    }

    // opzioni 'Crea documento fiscale'
    public function getInvoiceOptions()
    {
        return [
            $this->l('Disabled'),
            $this->l('Electronic invoice'),
            $this->l('NON electronic Invoice'),
            $this->l('NON fiscal Receipt'),
        ];
    }

    // opzioni stato pagato
    public function f24PaidStatus()
    {
        return [
            $this->l('Never'),
            $this->l('Always'),
            $this->l('E-payments (e.g.: Paypal)'),
        ];
    }

    // opzioni 'bollo virtuale'
    public function f24StampOptions()
    {
        return [
            $this->l('Never'),
            $this->l('VAT exempt fiscal regime and order total > 77,47 Euros'),
        ];
    }

    // opzioni numero ordine
    public function f24OrderNumber()
    {
        return [
            $this->l('Prestashop order id'),
            $this->l('Prestashop order reference'),
            $this->l('Prestashop order id - order reference'),
        ];
    }

    private function convertToUTF8($string)
    {
        $encoding = mb_detect_encoding($string, 'UTF-8, ISO-8859-1, ISO-8859-15, Windows-1252', true);
        if ($encoding === false) {
            $encoding = 'ISO-8859-1';
        }
        return iconv($encoding, 'UTF-8//TRANSLIT', $string);
    }

    // lista modelli
    public function getTemplate($templates, $isOrder)
    {
        $listaNomi = [];
        $listaNomi['Predefinito'] = $this->l('Default');
        $templates_utf8 = $this->convertToUTF8($templates);
        $xml = simplexml_load_string($templates_utf8);
        libxml_use_internal_errors(true);

        //$xml = simplexml_load_string(mb_convert_encoding((string) $templates, 'UTF8', mb_list_encodings()));
        if (is_object($xml) && ($xml !== false)) {
            $listaModelli = $isOrder ? $xml->modelloOrdine : $xml->modelloFattura;
            foreach ($listaModelli as $modello) {
                $listaNomi[(int) $modello->id] = (string) $modello->descrizione;
            }
        } else {
            $errors = libxml_get_errors();
            $this->trace('XML parsing errors templates:', var_export($errors, true));
            libxml_clear_errors();
            /*if (!empty($templates)) {
                $this->trace('error list templates', $templates);
            }*/
        }

        return $listaNomi;
    }

    // lista Pdc
    public function getPdc($f24_pdc)
    {
        $listaNomi = [];
        $listaNomi['Nessun Pdc'] = $this->l('No Pdc');
        $f24_pdc_utf8 = $this->convertToUTF8($f24_pdc);
        $xml = simplexml_load_string($f24_pdc_utf8);
        libxml_use_internal_errors(true);
        //$xml = simplexml_load_string(mb_convert_encoding((string) $f24_pdc, 'UTF8', mb_list_encodings()));
        if (is_object($xml) && ($xml !== false)) {
            foreach ($xml->pdc as $pdc) {
                $list = str_replace('^', '.', (string) $pdc->codice) . ' - ' . (string) $pdc->descrizione;
                $listaNomi[(int) $pdc->id] = $list;
            }
        } else {
            $errors = libxml_get_errors();
            $this->trace('XML parsing errors pdc:', var_export($errors, true));
            libxml_clear_errors();
            /*if (!empty($f24_pdc)) {
                $this->trace('error list pdc', $f24_pdc);
            }*/
        }

        return $listaNomi;
    }

    // lista sezionali
    public function getNumerator($f24_sezionali, $idTipoDocumento)
    {
        $listaNomi = [];
        $listaNomi['Predefinito'] = $this->l('Default');
        $f24_sezionali_utf8 = $this->convertToUTF8($f24_sezionali);
        $xml = simplexml_load_string($f24_sezionali_utf8);
        libxml_use_internal_errors(true);

        //$xml = simplexml_load_string(mb_convert_encoding((string) $f24_sezionali, 'UTF8', mb_list_encodings()));
        if (is_object($xml) && ($xml !== false)) {
            unset($listaNomi['Predefinito']);
            foreach ($xml->sezionale as $sezionale) {
                foreach ($sezionale->doc as $doc) {
                    if ((int) $doc->id == $idTipoDocumento && 1 == (int) $doc->stato) {
                        $listaNomi[(int) $sezionale->id] = (string) $sezionale->anteprima;
                    } elseif ((int) $doc->id == $idTipoDocumento && 2 == (int) $doc->stato) {
                        $listaNomi[(int) $sezionale->id] = (string) $sezionale->anteprima . $this->l(' (Default)');
                    }
                }
            }
        } else {
            $errors = libxml_get_errors();
            $this->trace('XML parsing errors numerators:', var_export($errors, true));
            libxml_clear_errors();
            /*if (!empty($f24_sezionali)) {
                $this->trace('error list sezionale', $f24_sezionali);
            }*/
        }

        return $listaNomi;
    }

    /**
     * Con questo metodo restituisco un solo array di aliquote IVA
     * contenente tutti gli sconti: se sono presenti sconti a carrello
     * il risultato sarà aggiungere ai dati già presenti la quota parte di sconto
     * ripartendola per aliquota.
     */
    public function updateItemTax($itemTax, $cartDiscount, $orderTotDiscountTaxExcl)
    {
        $maxDiscountAmount = $orderTotDiscountTaxExcl;
        $totalDiscounted = array_sum(array_column($itemTax, 'total_discount_per_vat_rate'));
        // formatto il risultato con sei decimali per una corretta comparazione
        $totDiscResult = (float) number_format((float) $totalDiscounted, 6, '.', '');
        // $this->trace('item tax prima dell\'if :', $itemTax);
        /*
         * In caso in cui ci sia un prodotto omaggio
         * faccio questo controllo ulteriore per capire se devo
         * ricalcolare total_discount_per_vat_rate
         * Da notare che questa logica si applica solo a sconti con restrizioni sui prodotti
         */
        if ($totDiscResult > $maxDiscountAmount) {
            foreach ($itemTax as $key => $value) {
                if ($itemTax[$key]['total_discount_per_vat_rate'] > 0) {
                    $itemTax[$key]['total_discount_per_vat_rate'] = $maxDiscountAmount;
                }
            }
        }
        // $this->trace('item tax dopo l\'if :', $itemTax);

        $discountDiff = $orderTotDiscountTaxExcl - $totalDiscounted;

        if ($discountDiff > 0 && $discountDiff > 0.01) {
            // il totale prodotti scontati in questo caso è diverso
            $totPriceProdDisc = (float) array_sum(array_column($cartDiscount, 'total_price_per_vat_rate'));

            foreach ($cartDiscount as $key => $value) {
                $total_price_per_rate = (float) $value['total_price_per_vat_rate'];
                $x = ($total_price_per_rate * 100) / $totPriceProdDisc;
                $price = (float) number_format((float) $discountDiff * $x / 100, 8, '.', '');

                if (!array_key_exists((int) $key, $itemTax)) {
                    $itemTax[$key] = $cartDiscount[$key]; // aggiungo l'array già esistente
                    $itemTax[$key]['total_discount_per_vat_rate'] += $price;
                } else {
                    $itemTax[$key]['total_discount_per_vat_rate'] += $price;
                }
            }
        }

        return $itemTax;
    }

    /**
     * Uso questo metodo per controllare se uno sconto fisso
     * è ristretto al prodotto - aggiunto controllo su tipo sconto
     * cfr ticket n.: 70957 - Davide Iandoli 11.02.2022.
     * edit Davide Iandoli 9.08.2023 : modificato il controllo
     * 
     * Otteggo l'ammontare della riduzione di prezzo su un prodotto
     */
    public function f24getRedAmount($productId, $f24CartRules)
    {
        $amountToAdd = 0;

        foreach ($f24CartRules as $rule) {
            $isFixed = $rule['type'] == 'fixed';

            if ($rule['reduction_product'] == $productId && $isFixed) {
                $amountToAdd += (float) $rule['value_tax_excl'];
            }
        }

        return $amountToAdd;
    }

    /**
     * Solo in caso di sconto a importo fisso chiamo questo metodo con cui intendo
     * creare una riga prodotto per ogni aliquota IVA, secondo la vecchia logica.
     * Riga  prodotto con importo negativo per sconti. $iTemTax è l'array di righe già costruito con 
     * ammontare dello sconto ripartito per aliquota IVA
     */
    public function addDiscProduct($domtree, $products, $itemTax, $f24CartRules)
    {
        $discount_name = 'Sconto ';
        $pdcSet = false;
        $idPdc = Configuration::get('PS_F24_PDC');
        if (!empty($idPdc) && 'Nessun Pdc' != $idPdc) {
            $pdcSet = true;
        }

        foreach ($itemTax as $key => $value) {
            $price = $value['total_discount_per_vat_rate'];

            // non aggiungo righe con importo zero!
            if (0.00 == (float) $price) {
                continue;
            }

            $product = $products->appendChild($domtree->createElement('Row'));
            $code = $domtree->createElement('Code');
            $code->appendChild($domtree->createCDataSection(''));
            $product->appendChild($code);
            $description = $domtree->createElement('Description');
            $description->appendChild($domtree->createCDataSection($discount_name . ' IVA ' . $key . '%'));
            $product->appendChild($description);
            $product->appendChild($domtree->createElement('Qty', '1'));
            $product->appendChild($domtree->createElement('Price', number_format((float) -$price, 8, '.', '')));
            $product->appendChild($domtree->createElement('VatCode', (string) $key));
            $product->appendChild($domtree->createElement('VatDescription', (string) $value['vat_description']));
            if (0 == $key) {
                $product->appendChild($domtree->createElement('FeVatNature', (string) $value['natura']));
            }

            if ($pdcSet) {
                $idPdc = Configuration::get('PS_F24_PDC');
                $product->appendChild($domtree->createElement('IdPdc', (string) $idPdc));
            }
        }
    }

    /**
     * Aggiungo le righe prodotto: gestisco sconti in percentuale e prodotti omaggio
     * in questo ultimo caso devo sottrarre una unità alla quantità acquista se il prodotto
     * è lo stesso che ho messo nel carrello (es.: 2 quadri al prezzo di uno) perché
     * abbiamo deciso di creare una riga prodotto a parte per gli omaggi:
     * così diventa più facile gestire sconti multipli che abbiano sia omaggio
     * sia percentuale sul carrello, e la fattura diventa più trasparente per l'utente.
     * --------------------------------------------------
     * edit: anche per gli sconti in percentuale aggiungiamo una riga prodotto con l'importo
     * per questo in questo blocco di codice non valorizzo il tag <Discounts> nell'xml
     * Davide Iandoli 8.08.2023
     */
    public function addProductRowsToXml($domtree, $products, $productList)
    {
        $pdcSet = false;
        $idPdc = Configuration::get('PS_F24_PDC');
        if (!empty($idPdc) && 'Nessun Pdc' != $idPdc) {
            $pdcSet = true;
        }

        foreach ($productList as $product_detail) {
            $this->trace('product list details', $product_detail);

            $qty = $product_detail['qty'];
            $productCode = $product_detail['code'];
            $productDescription = $product_detail['description'];
            $price = $product_detail['price'];
            $taxRate = (float) $product_detail['tax_rate'];
            $VatDescription = $product_detail['tax_description'];
            $natura = $product_detail['natura'];

            /*
             * se il prodotto è in omaggio la quantità è zero
             * salto al prossimo prodotto
             */
            if ((int) $qty < 1 || 0 == $price) {
                continue;
            }

            $product = $products->appendChild($domtree->createElement('Row'));
            $code = $domtree->createElement('Code');
            $code->appendChild($domtree->createCDataSection($productCode));
            $product->appendChild($code);
            $description = $domtree->createElement('Description');
            $description->appendChild($domtree->createCDataSection($productDescription));

            $product->appendChild($description);
            $product->appendChild($domtree->createElement('Qty', (string) $qty));
            $product->appendChild($domtree->createElement('Price', (string) $price));
            $product->appendChild($domtree->createElement('VatCode', (string) $taxRate));
            $product->appendChild($domtree->createElement('VatDescription', (string) $VatDescription));

            if (0 == (float) $taxRate) {
                $product->appendChild($domtree->createElement('FeVatNature', (string) $natura));
            }

            if ($pdcSet) {
                $product->appendChild($domtree->createElement('IdPdc', (string) $idPdc));
            }
        }
    }

    /**
     *  aggiunta prodotti omaggio all'xml
     */
    public function addGiftProductToXml($domtree, $products, $giftProductArray, $f24CartRules, $orderTaxes)
    {
        $pdcSet = false;
        $idPdc = Configuration::get('PS_F24_PDC');
        if (!empty($idPdc) && 'Nessun Pdc' != $idPdc) {
            $pdcSet = true;
        }

        foreach ($giftProductArray as $product_detail) {
            $cartRuleName = implode("\n", array_column($f24CartRules, 'name'));
            $productDescription = $product_detail['product_name'] . "\n" . $cartRuleName;

            $product = $products->appendChild($domtree->createElement('Row'));
            $code = $domtree->createElement('Code');
            $code->appendChild($domtree->createCDataSection($product_detail['product_reference']));
            $product->appendChild($code);
            $description = $domtree->createElement('Description');
            $description->appendChild($domtree->createCDataSection($productDescription));

            $product->appendChild($description);
            $product->appendChild($domtree->createElement('Qty', '1'));
            $product->appendChild($domtree->createElement('Price', (string) $product_detail['unit_price_tax_excl']));
            $product->appendChild($domtree->createElement('Discounts', '100'));

            $idTaxRule = (int) $product_detail['id_tax_rules_group'];
            $idOrderDetail = (int) $product_detail['id_order_detail'];
            $taxId = $this->getTaxId($orderTaxes, $idTaxRule, $idOrderDetail);
            $taxRateFromDetail = (float) $product_detail['tax_rate'];
            $taxRateFromId = $this->getTaxRateFromId($taxId);
            $taxRate = $taxRateFromDetail == $taxRateFromId ? $taxRateFromDetail : $taxRateFromId;
            $taxNameFromId = $this->getTaxNameFromId($taxId, $taxRate);

            if (1 == Configuration::get('PS_CREAZIONE_FATTURA')) {
                $natura = $this->getNaturaByIdTax((string) $idTaxRule);
                if (0 == (float) $taxRate) {
                    $product->appendChild($domtree->createElement('FeVatNature', (string) $natura));
                }
            }

            $product->appendChild($domtree->createElement('VatCode', (string) $taxRate));
            // $taxName = Product::getTaxesInformations($product_detail)['tax_name'];
            $taxName = $product_detail['tax_calculator']->getTaxesName();
            // $VatDescription = !empty($taxName) ? $taxName : $this->getDefaultVatDescription($taxRate);
            $VatDescription = !empty($taxName) ? $taxName : $taxNameFromId;
            $product->appendChild($domtree->createElement('VatDescription', (string) $VatDescription));

            if ($pdcSet) {
                $product->appendChild($domtree->createElement('IdPdc', (string) $idPdc));
            }
        }
    }

    /**
     * Esegue tutte le chiamate url
     */
    public function curlDownload($url, $data_string)
    {
        static $message_displayed = false;

        if (!function_exists('curl_init')) {
            $this->trace('curl is not installed');
            exit('Sorry, cURL is not installed!');
        }

        $ch = curl_init();
        $config['useragent'] = 'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:17.0) Gecko/20100101 Firefox/17.0';
        $options = [
            CURLOPT_URL => $url,
            CURLOPT_USERAGENT => $config['useragent'],
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_POSTFIELDS => $data_string,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HEADER => 0,
            CURLOPT_TIMEOUT => 60,
        ];

        curl_setopt_array($ch, $options);
        $output = curl_exec($ch);
        $response_code = curl_getinfo($ch, CURLINFO_RESPONSE_CODE);
        $otherError = curl_errno($ch) != 0;

        $error = [];
        if ($response_code !== 200) {
            $message_displayed = true;
            $error = [
                      'code' => $response_code,
                      'description' => $this->l('WARNING: connection to Fattura24 API failed, code ') . $response_code,
                     ];
        } elseif ($otherError) {
            $message_displayed = true;
            $error = [
                        'code' => $response_code,
                        'description' => print_r([curl_getinfo($ch), curl_error($ch), curl_errno($ch)], true),
                     ];
        }

        curl_close($ch);
        if ($message_displayed) {
            return ['error' => $error, 'displayed' => $message_displayed, 'output' => $error];
        } else {
            return ['code' => $response_code, 'output' => $output];
        }
    }

    /**
     *  new function to get Carrier tax rates, for
     *  correct Natura handling.
     */
    public function f24getCarriersTaxRate()
    {
        $context = Context::getContext();
        $id_lang = $context->language->id;
        $f24_carriers = Carrier::getCarriers($id_lang);
        $carrier_tax_rate = [];

        foreach ($f24_carriers as $val) {
            $id_carrier = $val['id_carrier'];
            if (0 == (int) Tax::getCarrierTaxRate($id_carrier)) {
                $carrier_tax_rate[$id_carrier] = Tax::getCarrierTaxRate($id_carrier);
            }
        }

        return $carrier_tax_rate;
    }

    /**
     *  Ottengo la descrizione dell'aliquota
     *  dall'id della tassa
     *  Nel caso peggiore scrivo IVA 22% (esempio)
     * edit 13.02.2023 Davide Iandoli
     */
    public function getTaxNameFromId($idTax, $rate)
    {
        $context = Context::getContext();
        $id_lang = $context->language->id;
        $sql = 'SELECT * from ' . _DB_PREFIX_ .
            'tax_lang where id_tax = ' . (int) $idTax
            . ' AND id_lang =' . (int) $id_lang;
        $result = Db::getInstance()->getRow($sql);
        $resultName = $result ? $result['name'] : $this->l('VAT') . ' ' . $rate . '%';

        return $resultName;
    }

    /**
     * Otteng l'id dell'ordine dalla reference
     */
    public function getOrderIdByReference($reference)
    {
        $sql = 'SELECT * from ' . _DB_PREFIX_ . 'orders where reference="' . $reference . '";';
        $result = Db::getInstance()->getRow($sql);

        return $result['id_order'];
    }

    /**
     *  Ottengo l'aliquota dal'id della tassa.
     */
    public function getTaxRateFromId($idTax)
    {
        $tax_rates = Tax::getTaxes();
        $taxRateResult = '';
        foreach ($tax_rates as $v) {
            if ((int) $v['id_tax'] == (int) $idTax) {
                $taxRateResult = $v['rate'];
            }
        }

        return (float) $taxRateResult;
    }

    /**
     *  Ottiene il taxId dal rate.
     */
    public function getTaxIdFromRate($rate)
    {
        $tax_rates = Tax::getTaxes();
        $idFound = false;
        $idResult = '';

        foreach ($tax_rates as $v) {
            if ((float) number_format((float) $rate, 2, '.', '') == (float) $v['rate']) {
                $idResult = $v['id_tax'];
            }
            $idFound = !empty($idResult);
            if ($idFound) {
                continue;
            }
        }

        return $idResult;
    }

    // new function to get zero rates
    public function getZeroRates()
    {
        $tax_rates = Tax::getTaxes();
        $zero_rate = [];

        foreach ($tax_rates as $v) {
            if (0 == (int) $v['rate']) {
                $id_tax = $v['id_tax'];
                $zero_rate[$id_tax] = $v['rate'];
            }
        }

        return $zero_rate;
    }

    /**
     *  Visualizzo un messaggio se è disponibile un aggiornamento de modulo
     * @return mixed
     */
    public function getUpdateMessage()
    {
        $module_update = isUpdateAvailable($this->version);
        $message = '';
        if ($module_update['update_available'] == 'true') {
            $link = '<a style="text-decoration: underline;" href="' .
            $module_update['filename'] . '" target="_blank">' .
            $module_update['filename'] . '</a>';

            $message = $this->l('Update available! To download click here: ') . $link;
        }

        if ($message) {
            return $this->displayInformation($message);
        }
    }

    /**
     * Visualizzo un messaggio i caso di errore connessione API
     */
    public function getApiError($resultArray, $showMessage)
    {
        if (!$showMessage) {
            return;
        }

        $type = 'success';
        $updateSettings = $this->l('Updated settings');
        $message = $updateSettings;

        if (isset($resultArray['type']) && $resultArray['type'] == 'error') {
            $type = 'error';
            $message = $resultArray['message'];
        } else {
            $message = $resultArray['message'] . ' - ' . $updateSettings;
        }

        if ($type == 'error') {
            return $this->displayError($message);
        } else {
            return $this->displayConfirmation($message);
        }
    }

    /**
     * Visualizzo un messaggio i caso di errore configurazione Natura
     */
    public function getNaturaError()
    {
        $zeroRates = $this->getZeroRates(); // product rates
        $zeroShipping = $this->f24getCarriersTaxRate(); // shipping rates
        $fattEl = 1 == Configuration::get('PS_CREAZIONE_FATTURA'); // solo per fatture elettroniche
        $emptyConfNatura = empty(Configuration::get('PS_CONF_NATURA_IVA'));
        $emptyConfSped = 'N0' == Configuration::get('PS_CONF_NATURA_SPEDIZIONE') && !empty($zeroShipping);
        $enteredConfNatura = preg_split('/-|;/', Configuration::get('PS_CONF_NATURA_IVA'));
        $selectedConfSped = Configuration::get('PS_CONF_NATURA_SPEDIZIONE');
        $oldNaturaArray = ['N2', 'N3', 'N6'];
        $errMsg = '';

        // different messages according to different scenario
        if (!empty($zeroRates) && $fattEl) {
            // primo blocco: campi vuoti
            if ($emptyConfNatura) {
                if ($emptyConfSped) {
                    $errMsg = $this->l('You should set up a Natura in both fields');
                } else {
                    $errMsg = $this->l('You should set up a Natura for each zero rate');
                }
            } elseif ($emptyConfSped) {
                $errMsg = $this->l('You should set up a Natura for each zero shipping rate');
            }

            // secondo blocco: campi compilati con valori non corretti
            if (!$emptyConfNatura) {
                $resultNatura = '';
                foreach ($enteredConfNatura as $naturaValue) {
                    if (false !== array_search($naturaValue, $oldNaturaArray)) {
                        $resultNatura = $naturaValue;
                    }
                }

                if (!empty($resultNatura)) {
                    $errMsg = $this->l('The code ' . $resultNatura . ' is no more allowed 
                    in invoices issued after 01/01/2021');
                }
            }
            if (in_array($selectedConfSped, $oldNaturaArray)) {
                $errMsg .= '<br />' . $this->l('The code ' . $selectedConfSped . ' in Shipping Natura is no more 
                allowed in invoices issued after 01/01/2021');
            }
        }

        if (!empty($errMsg)) {
            return $this->displayError($errMsg); // uso i metodi di prestashop
        } else {
            return '';
        }
    }

    /**
     * estrae l'id del prodotto dall'array ProductRestrictions
     */
    protected function getParamSubstring($param)
    {
        $needlePos = strpos($param, '-');

        return Tools::substr($param, 0, $needlePos);
    }

    /**
     * Descrizione aliquota IVA predefinita
     */
    public function getDefaultVatDescription($rate)
    {
        return 'IVA ' . $rate . '%';
    }

    /**
     * Descrizione Aliquota IVA 0% usata per la Natura
     */
    public function getNaturaVatDescription($idTaxRule)
    {
        if (0 == $idTaxRule) {
            $naturaSpedizione = Configuration::get('PS_CONF_NATURA_SPEDIZIONE');
            $lista = getListaNatureNew();
            $needle = Tools::substr($naturaSpedizione, 0, 2);

            if ('N0' == $naturaSpedizione) {
                $naturaSpedizione = '';
                $VatDescription = 'IVA';
            } elseif (is_array($lista[$needle])) {
                $VatDescription = $lista[$needle][$naturaSpedizione];
            } else {
                $VatDescription = $lista[$naturaSpedizione];
            }

            return $VatDescription;
        }

        return false;
    }

    /**
     * Ottiene il codice Natura dall'id della tassa
     */
    public function getNaturaByIdTax($idTax)
    {
        $output = Configuration::get('PS_CONF_NATURA_SPEDIZIONE');
        if ('0' == $idTax) {
            $output = Configuration::get('PS_CONF_NATURA_SPEDIZIONE');
        } elseif ('N0' == $output) {
            $naturaIvaStringConf = Configuration::get('PS_CONF_NATURA_IVA');
            $naturaIvaArrayConf = explode(';', $naturaIvaStringConf);
            foreach ($naturaIvaArrayConf as $oneNaturaConf) {
                if (false !== strpos($oneNaturaConf, $idTax)) {
                    $oneNaturaConfArray = explode('-', $oneNaturaConf);
                    $output = $oneNaturaConfArray[1];
                } else {
                    continue;
                }
            }
        }

        return $output;
    }

    /**
     * Data e ora in formato CET (Central European Time)
     */
    public function now($fmt = 'Y-m-d H:i:s', $tz = 'Europe/Rome')
    {
        $timestamp = time();
        $dt = new \DateTime('now', new \DateTimeZone($tz));
        $dt->setTimestamp($timestamp);

        return $dt->format($fmt);
    }

    /**
     * converte stringa in oggetto DateTime
     */
    public function convertToDate($param, $format = 'Y-m-d H:i:s')
    {
        if ($param instanceof DateTime) {
            return $param;
        } elseif (!is_string($param)) {
            return;
        }

        return DateTime::createFromFormat($format, $param);
    }
    
    /** Converte oggetto DateTime in stringa */
    public function convertDateToString($param) 
    {
        if ($param instanceof DateTime) {
            return $param->format('d-m-Y');
        } else if (is_string($param)) {
            return $param;
        }
	    
        return '';
    }

    /**
     * Con questo metodo controllo se nel campo partita IVA
     * è stata immessa anche la sigla del paese di fatturazione
     * nei primi due caratteri: in quel caso la tronco.
     */
    public function cleanVatNumber($billingCountry, $vatNumber)
    {
        $needle = Tools::substr(Tools::strtoupper($vatNumber), 0, 2);
        $result = $needle == $billingCountry ? Tools::substr($vatNumber, 2)
            : $vatNumber;

        return $result;
    }

    /**
     * Ottiene Id della tassa
     */
    public function getTaxId($orderTaxes, $idTaxRule, $idOrderDetail)
    {
        $taxId = 0;
        foreach ($orderTaxes as $tax) {
            if (
                $idTaxRule == (int) $tax['id_tax_rules_group']
                && $idOrderDetail == (int) $tax['id_order_detail']
            ) {
                $taxId = (int) $tax['id_tax'];
            }
        }

        return $taxId;
    }

    /**
     * Genera nome file di log
     */
    public function getLogFileName()
    {
        $path = _PS_MODULE_DIR_ . 'fattura24/log/';
        $today = date('d_m_Y');
        $filename = $path . 'f24_trace_' . $today . '.log';
        if (!file_exists($filename)) {
            $this->delOldLogFiles();
        }

        return $filename;
    }

    /**
     * Elimina file di log più vecchi di 30 giorni
     * Innescata al momento in cui viene creato il file di log di giornata
     */
    public function delOldLogFiles()
    {
        $path = _PS_MODULE_DIR_ . 'fattura24/log/';
        $today = DateTime::createFromFormat('d_m_Y', date('d_m_Y'));
        foreach (glob($path . '*.log') as $file) {
            $creationDay = DateTime::createFromFormat('d_m_Y', date('d_m_Y', filectime($file)));
            $result = $creationDay->diff($today);
            $different_days = $result->d;
            if ($different_days > 30) {
                unlink($file);
            }
        }
    }

    /* 
    * Creo il file con cui traccio l'elaborato del modulo
    * solo se abilito la modalità di debug
    */
    public function trace()
    {
        $debugEnabled = '1' == Configuration::get('PS_F24_DEBUG_MODE');
        if (!$debugEnabled) {
            return;
        }

        $logFileName = $this->getLogFileName();
        $fp = fopen($logFileName, 'a+');
        fprintf($fp, "%s: %s\n\n", $this->now(), var_export(func_get_args(), true));
        fclose($fp);
    }

    /**
     * Debug di oggetti, al momento inutilizzato
     */
    public function traceDebug($string)
    {
        $debugEnabled = '1' == Configuration::get('PS_F24_DEBUG_MODE');
        if (!$debugEnabled) {
            return;
        }

        $fp = fopen(_PS_MODULE_DIR_ . 'fattura24/log/traceDebug.log', 'a+');
        fprintf($fp, $string . "\n");
        fclose($fp);
    }

    // restituisce informazioni sull'ambiente (versione PHP, versione modulo etc. e le scrive nel file di log)
    public function getInfo()
    {
        return 'Fattura24 version ' . $this->version . ' | '
            . 'Prestashop version ' . _PS_VERSION_ . ' | '
            . 'PHP version ' . PHP_VERSION . ' | '
            . 'Shop address ' . _PS_BASE_URL_;
    }

    /**
     * Visualizza messaggio di errore configurazione
     */
    public function getConfigError()
    {
        $isFattura24Disabled = !Module::isEnabled('fattura24');
        $disabledOverrides = Configuration::get('PS_DISABLE_OVERRIDES');
        $message = '';

        if ($isFattura24Disabled) {
            $message = $this->l('MODULE DISABLED');
        } elseif ($disabledOverrides) {
            $message = $this->l('WARNING: WRONG CONFIGURATION ');
            $message .= $this->l('To display PDF files, click on Advanced Parameters -> 
            Performance -> Disable all overrides, set the option to No and click on save');
        }

        if (!empty($message)) {
            return $this->displayError($message);
        } else {
            return '';
        }
    }

    /**
     * Prestashop mi passa: tot spedizione iva inclusa
     * tot spedizione iva esclusa
     * aliquota applicata alla spedizione
     * Nel caso in cui nel negozio i prezzi siano
     * IVA inclusa mi calcolo lo scorporo perché Prestashop
     * mi restituisce un dato con arrotondamento al 2 decimale
     * Effettuo l'arrotondamento all'8 decimale
     * cfr. ticket DT n.: 6213
     */
    public function getTotShippingTaxExcl($order)
    {
        $taxCalcMethod = $order->getTaxCalculationMethod();

        // se il prezzo è tasse escluse riporto il valore inserito nel campo
        if ($taxCalcMethod == 1) {
            $result = (float) $order->total_shipping_tax_excl;
        } else {
            $totShippingTaxIncl = (float) $order->total_shipping_tax_incl;
            $carrierTaxRate = (float) $order->carrier_tax_rate;
            $result = number_format($totShippingTaxIncl / (1 + $carrierTaxRate / 100), 8, '.', '');
        }

        return (float) $result;
    }

    /**
     * Nuova opzione per test api da server,
     * innescata dal pulsante
     * casi previsti:
     * 1 - server non raggiungibile => codice 4xx/5xx, esito -2;
     * 2 - chiave Api non valida => codice 200, esito -1;
     * 3 - chiave valida => codice 200, esito 1;
     * 4 - test mai eseguito (prima installazione del modulo) => codice 0, esito 0;
     */
    public function setF24ApiResult($testKey, $now)
    {
        $delimiter = ' | ';
        $code = 0;

        if (isset($testKey['error'])) {
            $code = $testKey['error']['code'];
        } elseif (isset($testKey['code'])) {
            $code = $testKey['code'];
        }

        // $this->trace('testkey result :', $testKey);
        // $this->trace('codice :', $code);
        $response = isset($testKey['output']) && !is_array($testKey['output']) ?
            simplexml_load_string($testKey['output']) : $testKey['output']['description'];
        $esito = '0';
        $result = '';
        $expireDate = '';

        if ((int) $code !== 200) {
            $esito = '-2';
        } elseif ($esito !== '-2' && is_object($response)) {
            $esito = (int) $response->returnCode === 1 ? '1' : '-1';
            $expireDate = isset($response->subscription->expire) ?
                $response->subscription->expire : '';
        }

        $values = [$esito, $code, $now];

        if (!empty($expireDate)) {
            array_push($values, $expireDate);
        }

        $result = implode($delimiter, $values);

        return $result;
    }

    /**
     * Visualizza il messaggio realtivo al test chiave API e i messaggi di errore
     * relativei alle API
     */
    private function showMessage($resultArray)
    {
        $showMessage = false;
        $now = new DateTime((string) $this->now());
        $lastUpdate = $this->convertToDate($resultArray['lastUpdate']);

        if (!is_null($lastUpdate) && $lastUpdate instanceof DateTime && !is_null($now)) {
            $diff = $lastUpdate->diff($now);
            $minutes_elapsed = $diff->i;
            $showMessage = $minutes_elapsed > 5;
        }

        return $showMessage;
    }

    /**
     * Costruzione messagi di esito chiamata API
     */
    private function getTestMsg($res)
    {
        $apiKey = !empty(Tools::getValue('PS_FATTURA24_API')) ? Tools::getValue('PS_FATTURA24_API')
            : Configuration::get('PS_FATTURA24_API');
        $type = is_array($res) ? 'success' : 'error';
        $exitCode = isset($res['exitCode']) ? $res['exitCode'] : '';
        $httpCode = isset($res['httpCode']) ? $res['httpCode'] : '';
        $lastUpdate = isset($res['lastUpdate']) ? DateTime::createFromFormat('Y-m-d', (string) $res['lastUpdate']) : $this->now();
        $expireDate = DateTime::createFromFormat('d-m-Y', date('d-m-Y'));
        if (isset($res['expireDate'])) {
            $expireDate = $res['expireDate'];
        }

        $msg = '';
        if (empty($apiKey)) {
            $type = 'error';
            $msg = $this->l('API Key left empty!');
        } elseif ($exitCode == '-2') {
            $type = 'error';
            $msg = $this->l('API server connection failure, error code ') . $httpCode;
        } elseif ($exitCode == '-1') {
            $type = 'error';
            $msg = $this->l('API key not valid!');
        } elseif ($exitCode == '1') {
            $msg = $this->l('API key verified, subscription expires at: ') . $this->convertDateToString($expireDate);
        }

        return [
            'type' => $type,
            'message' => $msg,
            'lastUpdate' => $lastUpdate,
            'expireDate' => $expireDate,
        ];
    }
}